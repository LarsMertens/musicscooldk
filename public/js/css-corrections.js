$(window).load(function() {

    setTimeout ( function () {

        /**
         * CSS after page load corrections
         */
        $(".circle").each(function(){
            var circle = $(this);
            var width_circle = circle.outerWidth();
            var height_circle = width_circle;
            var line_height = height_circle;
            circle.css("height", width_circle);
            circle.css("line-height", height_circle + "px");
        });

    }, 1);

    /**
     * If agenda-inner-row-item got class unavailable then on hover alert
     */
    var table_cell = $(".agenda-inner-row-item");
    table_cell.hover(function(e) {
        var find_div = $(this).find(".agenda-item");
        if(find_div.length == 1) {
            $(this).css("background-color", "transparent");
            $(".popover").addClass('hide');
        }
    });

});
