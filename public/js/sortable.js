$( function() {
 
    // There's the gallery and the trash
    var $gallery = $( "#gallery" ),
      $trash = $( "#trash" );



 
    // Let the gallery items be draggable
    $( "li", $gallery ).draggable({
      cancel: "a.ui-icon", // clicking an icon won't initiate dragging
      revert: "invalid", // when not dropped, the item will revert back to its initial position
      containment: "document",
      helper: "clone",
      cursor: "move"
    });
        // Let the trash items be draggable
    $( "li", $trash ).draggable({
      cancel: "a.ui-icon", // clicking an icon won't initiate dragging
      revert: "invalid", // when not dropped, the item will revert back to its initial position
      containment: "document",
      helper: "clone",
      cursor: "move"
    });
 
    // Let the trash be droppable, accepting the gallery items
    $trash.droppable({
      accept: "#gallery li",
      classes: {
        "ui-droppable-active": "ui-state-highlight"
      },
      drop: function( event, ui ) {
        deleteImage( ui.draggable );
      }
    });
 
    // Let the gallery be droppable as well, accepting items from the trash
    $gallery.droppable({
      accept: "#trash li",
      classes: {
        "ui-droppable-active": "custom-state-active"
      },
      drop: function( event, ui ) {
        recycleImage( ui.draggable );
      }
    });
 
    // Image deletion function
    var teacherDown = "<a href='link/to/recycle/script/when/we/have/js/off' title='Recycle this image' class='ui-icon ui-icon-circle-arrow-n'>Recycle image</a>";
    function deleteImage( $item ) {

        var $list = $( "ul", $trash ).length ?
          $( "ul", $trash ) :
          $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $trash );
 
        $item.find( "a.ui-icon-circle-arrow-s" ).remove();
        $item.append( teacherDown ).appendTo( $list );
     
    }
 
    // Image recycle function
    var teacherUp = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='ui-icon ui-icon-circle-arrow-s'>Delete image</a>";
    function recycleImage( $item ) {
   
        $item
          .find( "a.ui-icon-circle-arrow-n" )
            .remove()
          .end()
          .css( "width", "96px")
          .append( teacherUp )
          .find( "img" )
            .css( "height", "72px" )
          .end()
          .appendTo( $gallery )
          .fadeIn();
      
    }


        // Image deletion function
    var recycle_icon = "<a href='link/to/recycle/script/when/we/have/js/off' title='Recycle this image' class='ui-icon ui-icon-refresh'>Recycle image</a>";
    function deleteImageB( $item ) {

        var $list = $( "ul", $gallery ).length ?
          $( "ul", $gallery ) :
          $( "<ul class='trash ui-helper-reset'/>" ).appendTo( $gallery );
 
        $item.find( "a.ui-icon-trash" ).remove();
        $item.append( recycle_icon ).appendTo( $list );
     
    }
 
    // Image recycle function
    var trash_icon = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='ui-icon ui-icon-trash'>Delete image</a>";
    function recycleImageB( $item ) {
   
        $item
          .find( "a.ui-icon-refresh" )
            .remove()
          .end()
          .css( "width", "96px")
          .append( trash_icon )
          .find( "img" )
            .css( "height", "72px" )
          .end()
          .appendTo( $trash )
          .fadeIn();
      
    }
 
    // Image preview function, demonstrating the ui.dialog used as a modal window
    function viewLargerImage( $link ) {
      var src = $link.attr( "href" ),
        title = $link.siblings( "img" ).attr( "alt" ),
        $modal = $( "img[src$='" + src + "']" );
 
      if ( $modal.length ) {
        $modal.dialog( "open" );
      } else {
        var img = $( "<img alt='" + title + "' width='384' height='288' style='display: none; padding: 8px;' />" )
          .attr( "src", src ).appendTo( "body" );
        setTimeout(function() {
          img.dialog({
            title: title,
            width: 400,
            modal: true
          });
        }, 1 );
      }
    }



 
    // Resolve the icons behavior with event delegation
    $( "ul.gallery > li" ).on( "click", function( event ) {
      var $item = $( this ),
        $target = $( event.target );
 
      if ( $target.is( "a.ui-icon-circle-arrow-s" ) ) {
        deleteImage( $item );
      } else if ( $target.is( "a.ui-icon-zoomin" ) ) {
        viewLargerImage( $target );
      }  else if ( $target.is( "a.ui-icon-circle-arrow-n" ) ) {
        recycleImage( $item );
      }
 
      return false;
    });
    
    $( "ul.trash > li" ).on( "click", function( event ) {
      var $item = $( this ),
        $target = $( event.target );
 
      if ( $target.is( "a.ui-icon-trash" ) ) {
        deleteImageB( $item );
      } else if ( $target.is( "a.ui-icon-zoomin" ) ) {
        viewLargerImageB( $target );
      }  else if ( $target.is( "a.ui-icon-refresh" ) ) {
        recycleImageB( $item );
      }
 
      return false;
    });
  } );

    // There's the gallery and the trash
    var $gallery = $( "#gallery" ),
      $trash = $( "#trash" );


      



    function save() {
     
    // var ul = document.getElementById("gallery");
    // var items = ul.getElementsByTagName("li");
    // for (var i = 0; i < items.length; ++i) {
    //   // do something with items[i], which is a <li> element
    //         alert(items[i]['id'] + " " +items[i]['getID'])
    // }
 
    var ul = document.getElementById("trash");
    var items = ul.getElementsByTagName("li");
    var sendData=[];
    for (var i = 0; i < items.length; ++i) {
      // do something with items[i], which is a <li> element
            alert(items[i]['id'] + " " + "test")
            //sendData.add(items[i]['id']);
            sendData[sendData.length] =items[i]['id'];
    }    
    // var formData = items;
    var url = '/teacher/mediaStudentsUpdate';
    var send = JSON.stringify(sendData);
  // var data = {
  //       content: $(items).serialize()
  //   };
 
 $.ajax({
        url: url,
        type: "GET",
        data: { id: send}
    });
console.log(send);
    // $.ajax({
    //     type: "POST",
    //     url: url,
    //     data: formData,
    //     dataType: "json",
    //     success: function(data) {
    //         console.log()
    //     },
    //     error: function(){
    //         alert('opps error occured');
    //     }
    // });
        
    };



//   $( function() {

// // $( "#available" ).droppable({
// //       drop: function( event, ui ) {
// //         $( this )
// //           .addClass( "ui-state-highlight" )
// //           .find( "p" )
// //             .html( "Dropped!" );
// //       }
// //     });
//     var $gallery = $( "#student" ),
//       $trash = $( ".teacher" );
// $('.teacher').draggable({
//             accept: "#student",
//     connectToDroppable: "#student",
//             helper: "clone",
//             revert: "invalid",




// });
// $("#origin").droppable({ accept: ".teacher", 
//     drop: function(event, ui) {
//             console.log(ui.draggable);
//             var draggableId = ui.draggable.attr("getID");
//             var draggableUploadedBy = ui.draggable.attr("uploadedBy");
//             var draggablePath = ui.draggable.attr("path");
// //            var droppableId = $(this).attr("id");
//             var dropped = ui.draggable;
//             var droppedOn = $(this);
//             $(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn);
//                     alert(
//           draggableId + " " + draggableUploadedBy+ "      "+draggablePath
//             );    
             
//                 }});

// $('#student').droppable({

//     accept: ".teacher",

//     revert: true,
//     // drop: function(event, ui){
//     //     // var id = $(ui.draggable).attr("id");
//     //     // var available = 'yes';
//     //     alert(
//     //         ui.draggable
//     //         );
//     // }
//            drop: function(event, ui) {
//             console.log(ui.draggable);
//             var draggableId = ui.draggable.attr("getID");
//             var draggableUploadedBy = ui.draggable.attr("uploadedBy");
//             var draggablePath = ui.draggable.attr("path");
// //            var droppableId = $(this).attr("id");
//             var dropped = ui.draggable;
//             var droppedOn = $(this);
//             $(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn);
//                     alert(
//           draggableId + " " + draggableUploadedBy+ "      "+draggablePath
//             );    
//                 },
//             out: function(event, elem) {
                

//                      alert( GetOut + "  " +
//           draggableId + " " + draggableUploadedBy+ "      "+draggablePath
//             );    
//                   }



 

//     // and the rest of your code
// });
  
    

// $("#student").sortable();

//     var trash_icon = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='ui-icon ui-icon-trash'>Delete image</a>";
//     function recycleImage( $item ) {
//       $item.fadeOut(function() {
//         $item
//           .find( "a.ui-icon-refresh" )
//             .remove()
//           .end()
//           .css( "width", "96px")
//           .append( trash_icon )
//           .find( "img" )
//             .css( "height", "72px" )
//           .end()
//           .appendTo( $gallery )
//           .fadeIn();
//       });
//     }
//         var recycle_icon = "<a href='link/to/recycle/script/when/we/have/js/off' title='Recycle this image' class='ui-icon ui-icon-refresh'>Recycle image</a>";
//     function deleteImage( $item ) {
//       $item.fadeOut(function() {
//         var $list = $( "ul", $trash ).length ?
//           $( "ul", $trash ) :
//           $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $trash );
 
//         $item.find( "a.ui-icon-trash" ).remove();
//         $item.append( recycle_icon ).appendTo( $list ).fadeIn(function() {
//           $item
//             .animate({ width: "48px" })
//             .find( "img" )
//               .animate({ height: "36px" });
//         });
//       });
//     }

//     // Resolve the icons behavior with event delegation
//     $( "ul.gallery > li" ).on( "click", function( event ) {
//       var $item = $( this ),
//         $target = $( event.target );
 
//       if ( $target.is( "a.ui-icon-trash" ) ) {
//         deleteImage( $item );
//       } else if ( $target.is( "a.ui-icon-zoomin" ) ) {
//         viewLargerImage( $target );
//       } else if ( $target.is( "a.ui-icon-refresh" ) ) {
//         recycleImage( $item );
//       }
 
//       return false;
//     });

// });

// function save() {
// $.each($('#avalible').find('li'), function() {
//         alert($(this).text());
//     });
    
// };







     // var listItems = $("#avalible li");
     //    listItems.each(function(idx, li){
     //     var product = $(li.item).attr('alt');
     //     alert(product.alt);
     // })