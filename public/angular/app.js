var app = angular.module('app',  ['ui.bootstrap', 'timescheduleCtrl','teacherMediaService', 'teacherMediaCtrl','nonAdminTeacherCtrl', 'nonAdminTeacherService','nonAdminStudentCtrl', 'nonAdminStudentService'], function($interpolateProvider) {

    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

});

var admin_app = angular.module('AdminApp',  ['ui.bootstrap', 'timescheduleCtrl', 'teacherMediaService','teacherMediaCtrl', 'invoiceService', 'invoiceCtrl', 'teacherCtrl', 'teacherService', 'myCtrl', 'studentCtrl', 'studentService','mediaService','mediaCtrl', 'pricingCtrl', 'pricingService'], function($interpolateProvider) {

    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

});


app.filter('range', function() {
    return function(input, total) {
        total = parseInt(total);

        for (var i=0; i<total; i++) {
            input.push(i);
        }

        return input;
    };
});
app.filter('moment', function() {
    return function(dateString, format) {
        return moment(dateString).format(format);
    };
});

admin_app.filter('range', function() {
    return function(input, total) {
        total = parseInt(total);

        for (var i=0; i<total; i++) {
            input.push(i);
        }

        return input;
    };
});
admin_app.filter('moment', function() {
    return function(dateString, format) {
        return moment(dateString).format(format);
    };
});
