angular.module('nonAdminStudentService', [])

    .factory('nonAdminUser', function($http) {

        return {
      
            getAll : function($page) {
                return $http({ method: 'GET',
                               url: '/api/students',
                               params: {page:  $page}
                             });
            },
        
            search : function($search_value) {
                return $http({ method: 'POST',
                               url: '/student/search/' + $search_value
                             });
            },
            
            delete : function($id) {
                return $http({ method: 'POST',
                               url: '/teacher/deleteStudent/' + $id
                });
            }
           
        }

    });