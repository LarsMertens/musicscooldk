angular.module('teacherMediaService', [])

    .factory('teacherMedia', function($http) {

        return {
            getSingleImage : function($id){
                return $http({ method: 'GET',
                               url: '/teacher/media/loadSingleImage/'+$id+''
                });
            },
            getAllImagesWithId : function($id) {
                return $http({ method: 'GET',
                               url: '/teacher/api/media/getAllImagesWithId/'+$id+''
                });
            },
            getAll : function() {
                return $http({ method: 'GET',
                               url: '/teacher/api/media/getAll/'
                });
            },
            update : function($id, $name, $alt, $path) {
                return $http({ method: 'POST',
                               url: '/teacher/api/media/update/'+$id+'/'+$name+'/'+$alt+''
                });
            },
            delete : function($name){
                return $http({ method: 'POST',
                               url: '/teacher/dropzone/delete/'+$name+''
                });
            },
            deleteOnPopup : function($id){
                return $http({ method: 'POST',
                               url: '/teacher/api/media/delete/'+$id+''
                });
            },
            search : function($search_value) {
                return $http({ method: 'POST',
                               url: '/teacher/media/search/' + $search_value
                });
            }
        }

    });