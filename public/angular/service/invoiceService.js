angular.module('invoiceService', [])

    .factory('Invoice', function($http) {

        return {
            // Get request for all Invoices
            getAll : function($page) {
                return $http({ method: 'GET',
                               url: '/admin/api/invoices',
                               params: {page:  $page}
                             });
            },
            // Search request for all Invoices, search on $search_value
            search : function($search_value) {
                return $http({ method: 'POST',
                               url: '/admin/invoice/search/' + $search_value
                             });
            },
            // Delete request for deleting an invoice by id
            delete : function($id) {
                return $http({ method: 'POST',
                               url: '/admin/invoice/deleteInvoice/' + $id
                });
            },
            // Get all choices for editing an Invoice with Invoice Lines dynamically
            getAllChoices : function($invoice_id){
                return $http({ method: 'GET',
                               url: '/admin/api/invoices/choices/' + $invoice_id
                });
            },
            deleteWithRecordChoice : function($invoice_id){
                return $http({ method: 'POST',
                    url: '/admin/api/invoices/delete/choice/' + $invoice_id
                });
            },
            searchStudentID : function($search_value){
                return $http({ method: 'POST',
                    url: '/admin/invoice/search/studentid/' + $search_value
                });
            }
        }

    });