angular.module('nonAdminTeacherService', [])

    .factory('nonAdminTeacher', function($http) {

        return {
      
            getAll : function($page) {
                return $http({ method: 'GET',
                               url: '/teacher/api/teachers',
                               params: {page:  $page}
                             });
            },
        
            search : function($search_value) {
                return $http({ method: 'POST',
                               url: '/teacher/search/' + $search_value
                             });
            },
            
            delete : function($id) {
                return $http({ method: 'POST',
                               url: '/teacher/deleteTeacher/' + $id
                });
            }
           
        }

    });