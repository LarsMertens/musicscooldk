angular.module('pricingService', [])

    .factory('Price', function($http) {

        return {
      
            getAll : function($page) {
                return $http({ method: 'GET',
                               url: '/admin/api/pricing',
                               params: {page:  $page}
                             });
            },
        
            search : function($search_value) {
                return $http({ method: 'POST',
                               url: '/admin/pricing/search/' + $search_value
                             });
            },
            
            delete : function($id) {
                return $http({ method: 'POST',
                               url: '/admin/pricing/deletePrice/' + $id
                });
            }
           
        }

    });