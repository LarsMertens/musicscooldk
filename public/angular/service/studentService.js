angular.module('studentService', [])

    .factory('User', function($http) {

        return {
      
            getAll : function($page) {
                return $http({ method: 'GET',
                               url: '/admin/api/students',
                               params: {page:  $page}
                             });
            },
            getInactive : function($page) {
                return $http({ method: 'GET',
                               url: '/admin/api/studentsInactive',
                               params: {page:  $page}
                             });
            },
        
            search : function($search_value) {
                return $http({ method: 'POST',
                               url: '/admin/student/search/' + $search_value
                             });
            },
            
            delete : function($id) {
                return $http({ method: 'POST',
                               url: '/admin/student/deleteStudent/' + $id
                });
            },
            setActive : function($id) {
                return $http({ method: 'POST',
                               url: '/admin/studentSetActive/' + $id
                });
            },
            setInActive : function($id) {
                return $http({ method: 'POST',
                               url: '/admin/studentSetInactive/' + $id
                });
            }
           
        }

    });