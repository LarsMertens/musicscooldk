angular.module('teacherService', [])

    .factory('Teacher', function($http) {

        return {
      
            getAll : function($page) {
                return $http({ method: 'GET',
                               url: '/admin/api/teachers',
                               params: {page:  $page}
                             });
            },
        
            search : function($search_value) {
                return $http({ method: 'POST',
                               url: '/admin/teacher/search/' + $search_value
                             });
            },
            
            delete : function($id) {
                return $http({ method: 'POST',
                               url: '/admin/teacher/deleteTeacher/' + $id
                });
            },
            getInactive : function($page) {
                return $http({ method: 'GET',
                               url: '/admin/api/teachersInactive',
                               params: {page:  $page}
                             });
            },
            setActive : function($id) {
                return $http({ method: 'POST',
                               url: '/admin/teacherSetActive/' + $id
                });
            },
            setInActive : function($id) {
                return $http({ method: 'POST',
                               url: '/admin/teacherSetInactive/' + $id
                });
            }
           
           
        }

    });