angular.module('mediaService', [])

    .factory('Media', function($http) {

        return {
            getSingleImage : function($id){
                return $http({ method: 'GET',
                               url: '/admin/media/loadSingleImage/'+$id+''
                });
            },
            getAllImagesWithId : function($id) {
                return $http({ method: 'GET',
                               url: '/admin/api/media/getAllImagesWithId/'+$id+''
                });
            },
            getAll : function() {
                return $http({ method: 'GET',
                               url: '/admin/api/media/getAll/'
                });
            },
            update : function($id, $name, $alt, $path) {
                return $http({ method: 'POST',
                               url: '/admin/api/media/update/'+$id+'/'+$name+'/'+$alt+''
                });
            },
            delete : function($name){
                return $http({ method: 'POST',
                               url: '/admin/dropzone/delete/'+$name+''
                });
            },
            deleteOnPopup : function($id){
                return $http({ method: 'POST',
                               url: '/admin/api/media/delete/'+$id+''
                });
            },
            search : function($search_value) {
                return $http({ method: 'POST',
                               url: '/admin/media/search/' + $search_value
                });
            }
        }

    });