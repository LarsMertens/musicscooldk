angular.module('timescheduleCtrl', [])


    .controller('timescheduleController', function($scope, $timeout, $http, $interval) {

        // moment.locale('nl');
        $scope.i = 0;

        // Variables on initialization
        $scope.starting_time = "0930";
        $scope.times = [];
        $scope.loading = true;
        $scope.noLessonPlanned = false;
        $scope.runOnce = true;
        $scope.NrOfLessons = 0;

        $scope.fillTimeSchedule = function($teacher_id, $teacher_name, $booked = false) {

            $scope.theSelectedTeacher = $teacher_name;
            $scope.theSelectedTeacherID = $teacher_id;

            if($booked){
                $scope.booked = true;
                $timeout(function () {
                    $scope.booked = false;
                    }, 3000);
            }

            // @TODO : Try to do all in 1 request

            $scope.loading = true;
            $scope.teacher_id = $teacher_id;

            $http({
                url: 'agenda/getUserSchedule/' + $scope.teacher_id,
                method: "GET"
            }).success(function (data) {
                $scope.student_teacher_id = data;
                $scope.loading = false;
            });

            $http({
                url: 'agenda/getUserSchedule/getUserLessons/' + $scope.teacher_id + '/' + $scope.i,
                method: "GET"
            }).success(function (data) {
                $scope.data = data;
                $scope.loading = false;
            });

            $scope.loadTimes();

        };

        $scope.getNrOfLessons = function()
        {
            $http({
                url: 'agenda/getUserSchedule/getUserLessons/',
                method: "GET"
            }).success(function (data) {
                $scope.NrOfLessons = data.length;
                data.sort(compareStarttime);
                $scope.nextLessonDate = moment(data[0].date).format("DD-MM-YYYY");
                $scope.nextLessonTime = moment(data[0].starttime).format("HH.mm");
                $scope.loading = false;
            });
        };

        function compareStarttime(a,b) {
            if(a.starttime < b.starttime) return -1;
            if(a.starttime > b.starttime) return 1;
            return 0;
        }

        $scope.loadTimes = function()
        {
            $scope.times = [];
            $scope.starting_time = "0930";

            for(i = 0; i < 24; i++) {
                $scope.time = moment($scope.starting_time, "HH.mm");
                $scope.time = $scope.time.add(30, 'minutes').format("HH.mm");
                $scope.starting_time = $scope.time;
                $scope.times.push($scope.time);
            }
        };

        $scope.getDay = function($i) {
            return moment().add($i + $scope.i, 'days').format("dddd");
        };

        $scope.getDate = function($i) {
            return moment().add($i + $scope.i, 'days').format("DD-MM-YYYY");
        };

        $scope.checkSunday = function(day){

            if(day == 'Sunday'){
                return false;
            } else {
                return true;
            }
        };

        $scope.loadPreviousWeek = function(){

            if($scope.i == 0){
                return; // reduce loading times - [refuse request]
            }

            $scope.i = $scope.i - 7;
            if($scope.i < 0){
                $scope.i = 0;
            }

            $scope.fillTimeSchedule($scope.theSelectedTeacherID, $scope.theSelectedTeacher);
        };

        $scope.loadNextWeek = function(){
            $scope.i = $scope.i + 7;
            $scope.fillTimeSchedule($scope.theSelectedTeacherID, $scope.theSelectedTeacher);
        };

        $scope.checkIfMyLesson = function(time, date){
            if ($scope.data !== undefined) {
                for ($i = 0; $i < $scope.data.length; $i++) {

                    if (   moment($scope.data[$i].starttime).format('HH.mm') == time
                        && moment($scope.data[$i].date).format('DD-MM-YYYY') == date
                        ||
                        moment($scope.data[$i].starttime).add(15, 'minutes').format('HH.mm') == time
                        && moment($scope.data[$i].date).format('DD-MM-YYYY') == date
                    ) {
                        if($scope.data[$i].type == 'lesson') {
                            if ($scope.data[$i].student_teacher_id == $scope.student_teacher_id) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        };

        $scope.loadTime = function($time, $minuts){
            var time = moment($time, "HH.mm");
            time = time.add($minuts, 'minutes').format("HH.mm");
            return time;
        };

        $scope.loadMessage = function(time, date){

            if ($scope.data !== undefined) {
                for ($i = 0; $i < $scope.data.length; $i++) {

                    if (   moment($scope.data[$i].starttime).format('HH.mm') == time
                        && moment($scope.data[$i].date).format('DD-MM-YYYY') == date
                        ||
                        moment($scope.data[$i].starttime).add(15, 'minutes').format('HH.mm') == time
                        && moment($scope.data[$i].date).format('DD-MM-YYYY') == date
                        ) {
                            return $scope.data[$i].message;
                    }
                }
            }
            return false;
        };

        $scope.checkScheduledItems = function(time, date){

            if ($scope.data !== undefined) {
                for ($i = 0; $i < $scope.data.length; $i++) {

                    if (   moment($scope.data[$i].starttime).format('HH.mm') == time
                        && moment($scope.data[$i].date).format('DD-MM-YYYY') == date
                    ) {
                            return true;
                    }
                }
            }
            return false;
        };

        $scope.checkScheduledItemsOnQuarter = function(time, date){

            if ($scope.data !== undefined) {
                for ($i = 0; $i < $scope.data.length; $i++) {

                    if (   moment($scope.data[$i].starttime).add(15, 'minutes').format('HH.mm') == time
                        && moment($scope.data[$i].date).format('DD-MM-YYYY') == date)
                    {
                        return true;
                    }
                }
            }
            return false;
        };

        $scope.checkDuration = function(time, date, quarter){

            if ($scope.data !== undefined) {
                for ($i = 0; $i < $scope.data.length; $i++) {
                    if(!quarter) {
                        if (moment($scope.data[$i].starttime).format('HH.mm') == time
                            && moment($scope.data[$i].date).format('DD-MM-YYYY') == date) {
                            if ($scope.data[$i].duration == 30) {
                                return 30;
                            } else if ($scope.data[$i].duration == 45) {
                                return 45;
                            } else if ($scope.data[$i].duration == 690){
                                return 690;
                            }
                        }
                    } else if(quarter){
                        if (   moment($scope.data[$i].starttime).add(15, 'minutes').format('HH.mm') == time
                            && moment($scope.data[$i].date).format('DD-MM-YYYY') == date)
                        {
                            if ($scope.data[$i].duration == 30) {
                                return 30;
                            } else if ($scope.data[$i].duration == 45) {
                                return 45;
                            }
                        }
                    }
                }
            }
            return false;
        };

        /**
         *
         */
        $scope.getTimeUntilNextLesson = function()
        {

            $http({
                url: 'agenda/getNextLesson',
                method: "GET"
            }).success(function (data, status, headers, config) {
                if(data.length > 0){
                    $scope.noLessonPlanned = false;
                    $scope.nextDateGet = moment(data[0].starttime).format("YYYY-MM-DD HH:mm:ss");
                    $scope.nextLessonDate = moment($scope.nextDateGet).format("DD-MM-YYYY");
                    $scope.nextLessonTime = moment($scope.nextDateGet).format("HH.mm");
                } else {
                    $scope.noLessonPlanned = true;
                }
            });

            $interval(function(){

                $scope.nextDate = moment().countdown($scope.nextDateGet,
                    countdown.DAYS|countdown.HOURS|countdown.MINUTES|countdown.SECONDS
                );

                if(moment($scope.nextDate).isSame(moment(), 'day')){
                    $scope.daysCountdown = 0;
                } else {
                    $scope.daysCountdown = moment($scope.nextDate).format('D');
                }

                $scope.hoursCountdown = moment($scope.nextDate).format('HH');
                $scope.minutesCountdown = moment($scope.nextDate).format('mm');
                $scope.secondsCountdown = moment($scope.nextDate).format('ss');

            },1000);
        };

        /**
         *
         */
        $scope.getNrOfLessonsPlanned = function () {

            $http({
                url: 'agenda/getNrOfLessonsPlanned',
                method: "GET"
            }).success(function (data, status, headers, config) {
                $scope.NrOfLessons = data;
            });

        };

    });