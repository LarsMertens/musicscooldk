angular.module('nonAdminStudentCtrl', [])

.controller('nonAdminStudentController', function($scope, $http, $window, nonAdminUser) {

    $scope.data = '';
    $scope.deleteURL = '';
    $scope.loading = true;
    $scope.choices = [{id: 'choice1'}];

    $scope.getPage = function($page) {
        $scope.loading = true;
        nonAdminUser.getAll($page)
            .then(function (success) {
                $scope.data = success.data;
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.nextPage = function($current_page, $last_page) {
        if($current_page != $last_page) {
nonAdminUserscope.loading = true;
            nonAdminUser.getAll($current_page + 1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        }
    };

    $scope.previousPage = function($current_page) {
        if($current_page != 1) {
            $scope.loading = true;
            nonAdminUser.getAll($current_page - 1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        }
    };

    $scope.searchItems = function($search_value) {
        if($search_value != ''){
            $scope.loading = true;
            nonAdminUser.search($search_value)
                .then(function (success) {
                    $scope.data = success;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        } else {
            nonAdminUser.getAll(1)
                .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
        }
    };

    $scope.deleteItem = function($id, $current_page){
        $scope.loading = true;
        nonAdminUser.delete($id)
            .then(function (success) {
                nonAdminUser.getAll($current_page)
                    .then(function (success) {
                        $scope.data = success.data;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.getInfo = function($id){
        nonAdminUser.getAllChoices($id)
            .then(function (success) {
                $scope.choices = success.data;
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };


});


