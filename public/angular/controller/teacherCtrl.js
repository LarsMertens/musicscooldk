angular.module('teacherCtrl', [])

.controller('teacherController', function($scope, $http, $window, Teacher) {

    $scope.data = '';
    $scope.deleteURL = '';
    $scope.loading = true;
    $scope.choices = [{id: 'choice1'}];

    $scope.getPage = function($page) {
        $scope.loading = true;
        Teacher.getAll($page)
            .then(function (success) {
                $scope.data = success.data;
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };
        $scope.getPageInactive = function($page) {
        $scope.loading = true;
        Teacher.getInactive($page)
            .then(function (success) {
                $scope.data = success.data;
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.nextPage = function($current_page, $last_page) {
        if($current_page != $last_page) {
            $scope.loading = true;
            Teacher.getAll($current_page + 1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        }
    };

    $scope.previousPage = function($current_page) {
        if($current_page != 1) {
            $scope.loading = true;
            Teacher.getAll($current_page - 1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        }
    };

    $scope.searchItems = function($search_value) {
        if($search_value != ''){
            $scope.loading = true;
            Teacher.search($search_value)
                .then(function (success) {
                    $scope.data = success;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        } else {
            Teacher.getAll(1)
                .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
        }
    };

    $scope.getInfo = function($id){
        Teacher.getAllChoices($id)
            .then(function (success) {
                $scope.choices = success.data;
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };
 $scope.deleteItem = function($id, $current_page){
        $scope.loading = true;
        Teacher.delete($id)
            .then(function (success) {
                Teacher.getInactive($current_page)
                    .then(function (success) {
                        $scope.data = success.data;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };
    $scope.setInActive = function($id, $current_page){
        $scope.loading = true;
        Teacher.setInActive($id)
            .then(function (success) {
                Teacher.getAll($current_page)
                    .then(function (success) {
                        $scope.data = success.data;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };
    $scope.setActive = function($id, $current_page){
        $scope.loading = true;
        Teacher.setActive($id)
            .then(function (success) {
                Teacher.getInactive($current_page)
                    .then(function (success) {
                        $scope.data = success.data;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };


});


