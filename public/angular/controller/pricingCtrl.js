angular.module('pricingCtrl', [])

.controller('pricingController', function($scope, $http, $window, Price) {

    $scope.data = '';
    $scope.deleteURL = '';
    $scope.loading = true;
    $scope.choices = [{id: 'choice1'}];

    $scope.getPage = function($page) {
        $scope.loading = true;
        Price.getAll($page)
            .then(function (success) {
                $scope.data = success.data;
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.nextPage = function($current_page, $last_page) {
        if($current_page != $last_page) {
            $scope.loading = true;
            Price.getAll($current_page + 1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        }
    };

    $scope.previousPage = function($current_page) {
        if($current_page != 1) {
            $scope.loading = true;
            Price.getAll($current_page - 1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        }
    };

    $scope.searchItems = function($search_value) {
        if($search_value != ''){
            $scope.loading = true;
            Price.search($search_value)
                .then(function (success) {
                    $scope.data = success;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        } else {
            Price.getAll(1)
                .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
        }
    };

    $scope.deleteItem = function($id, $current_page){
        $scope.loading = true;
        Price.delete($id)
            .then(function (success) {
                Price.getAll($current_page)
                    .then(function (success) {
                        $scope.data = success.data;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.getInfo = function($id){
        Price.getAllChoices($id)
            .then(function (success) {
                $scope.choices = success.data;
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.rememberChoice = function($choice_id)
    {
        $scope.singleChoice = false;
        $scope.lastChoice = $choice_id;
    };

    $scope.noChoice = function()
    {
        $scope.singleChoice = true;
    };



});


