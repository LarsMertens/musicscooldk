angular.module('teacherMediaCtrl', [])

    .controller('teacherMediaController', function($scope, $http, $timeout, $window, teacherMedia) {

        $scope.loadSingleImage = function($id)
        {
            teacherMedia.getSingleImage($id)
                .then(function (success) {
                    $scope.single_image = success.data;
                    $( "input[name=id]" ).val($scope.single_image.id);
                    $( "input[name=name]" ).val($scope.single_image.name);
                    $( "input[name=alt]" ).val($scope.single_image.alt);
                    $( "input[name=path]" ).val($scope.single_image.path);
                    $( "#the-highlighted-image" ).attr("src", $scope.single_image.path);
                    $( "input[name=id]" ).trigger("input");
                    $( "input[name=name]" ).trigger("input");
                    $( "input[name=alt]" ).trigger("input");
                    $( "input[name=path]" ).trigger("input");
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.getAllImagesWithId = function($id){
            $scope.loading = true;
            teacherMedia.getAllImagesWithId($id)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });

        };
        $scope.getAllMediaItems = function(){
            $scope.loading = true;
            teacherMedia.getAll()
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });

        };

        $scope.changeImageValues = function(id,name,alt,path){
            $scope.loading = true;
            teacherMedia.update(id, name, alt, path)
                .then(function (success) {
                    $scope.single_image = success;
                    $scope.loading = false;
                    $scope.success = true;
                    $timeout(function () {
                           $scope.success = false;
                        }, 3000
                    );
                }).catch(function (e) {
                    $scope.fail = true;
                    $timeout(function () {
                            $scope.fail = false;
                        }, 3000
                    );
                    console.log("got an error in the process", e);
                });
        };

        /**
         * @param path = path of image
         *
         * Get image url by using ng-model / $scope.path value on input field
         * And close all modals
         */
        $scope.getImageURL = function(path)
        {
            if($scope.singleChoice) {
                $( "input[name=singleImage]").val(path);
                $( "input[name=singleImage]").trigger("input");
            } else if(!$scope.singleChoice){
                $( "#" + $scope.lastChoice ).find("input").val(path);
                $( "#" + $scope.lastChoice ).find("input").trigger("input");
            }

            angular.element(document.querySelector('#myModalHighlight .close')).click();
            angular.element(document.querySelector('#myModal .close')).click();
        };

        $scope.deleteImage = function(name)
        {
            $scope.loading = true;
            teacherMedia.delete(name)
                .then(function (success) {
                    teacherMedia.getAll()
                        .then(function (success) {
                            $scope.data = success.data;
                            $scope.loading = false;
                        }).catch(function (e) {
                            console.log("got an error in the process", e);
                        });
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.deleteImageInMediaPopUp = function(id)
        {
            $scope.loading = true;
            teacherMedia.deleteOnPopup(id)
                .then(function (success) {
                    teacherMedia.getAll()
                        .then(function (success) {
                            $scope.data = success.data;
                            $scope.loading = false;
                        }).catch(function (e) {
                            console.log("got an error in the process", e);
                        });
                    angular.element(document.querySelector('#myModalHighlight .close')).click();
                    angular.element(document.querySelector('#myModal .close')).click();
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.searchItems = function($search_value) {
            if($search_value != ''){
                $scope.loading = true;
                teacherMedia.search($search_value)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            } else {
                teacherMedia.getAll()
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

    });
