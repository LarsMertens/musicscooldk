﻿angular.module('ui.bootstrap')

    .controller('PopoverDemoCtrl', function ($scope, $http, $window, $sce, $filter) {

      $scope.lessonName = 'Guitar - 1 person';
      $scope.teacherID = 4;
      $scope.durationTime = 30;
      $scope.checkPlannedInLesson = false;
      $scope.teacherID = 5;
      $scope.selectedALesson = false;
      $scope.selectedTeachers = [];
      $scope.selectedTeacherID = '';
      $scope.selectedTeacherName = '';

      $scope.selectedLessons = [{
        Id: 1,
        Name: 'Select a lesson'
      }, {
        Id: 2,
        Name: 'Guitar'
      }, {
        Id: 3,
        Name: 'Bas'
      }, {
        Id: 4,
        Name: 'Tromme'
      }, {
        Id: 5,
        Name: 'Klaver'
      }, {
        Id: 6,
        Name: 'Sang'
      }];

      $scope.lessons = [{
        Id: 1,
        Name: 'Guitar - 1 person'
      }, {
        Id: 2,
        Name: 'Guitar - 2 personer'
      }, {
        Id: 3,
        Name: 'Bas - 1 person'
      }, {
        Id: 4,
        Name: 'Bas - 2 personer'
      }, {
        Id: 5,
        Name: 'Tromme - 1 person'
      }, {
        Id: 6,
        Name: 'Klaver - 1 person'
      }, {
        Id: 7,
        Name: 'Klaver - 2 personer'
      }, {
        Id: 8,
        Name: 'Sang - 1 person'
      },{
        Id: 9,
        Name: 'Sang - 2 personer'
      }];

      $scope.durations = [{
        Id: 1,
        Name: '30'
      }, {
        Id: 2,
        Name: '45'
      }];

      $scope.GetValueLessonTypeDropdown = function () {
        var lessonID = this.lesson.Id;
        $scope.lessonName = $.grep($scope.lessons, function (lesson) {
          return lesson.Id == lessonID;
        })[0].Name;
        return $scope.lessonName;
      };

      $scope.GetValueSelectedLessonTypeDropdown = function () {
        var selectedLessonID = this.selectedLesson.Id;
        $scope.selectedLessonName = $.grep($scope.selectedLessons, function (selectedlesson) {
          return selectedlesson.Id == selectedLessonID;
        })[0].Name;

        if(selectedLessonID != 1){
          $scope.selectedALesson = true;

          if($scope.selectedLessonName == 'Guitar'){
            $scope.loadTeacherByLessonType('guitar');
          } else if($scope.selectedLessonName == 'Bas'){
            $scope.loadTeacherByLessonType('bas');
          } else if($scope.selectedLessonName == 'Tromme'){
            $scope.loadTeacherByLessonType('tromme');
          } else if($scope.selectedLessonName == 'Klaver'){
            $scope.loadTeacherByLessonType('klaver');
          } else if($scope.selectedLessonName == 'Sang'){
            $scope.loadTeacherByLessonType('sang');
          }

        } else if(selectedLessonID == 1){
          $scope.selectedALesson = false;
        }

        return $scope.selectedLessonName;
      };

      $scope.GetValueSelectedTeacherDropdown = function () {
        var selectedTeacherID = this.selectedTeacher.Id;
        $scope.selectedTeacher = $.grep($scope.selectedTeachers, function (selectedteacher) {
          return selectedteacher.Id == selectedTeacherID;
        })[0];


        $scope.fillTimeSchedule($scope.selectedTeacher.Id, $scope.selectedTeacher.Name);
        return $scope.selectedTeacher;
      };

      $scope.loadTeacherByLessonType = function($type){
        $scope.selectedTeachers = [];
        $http({
          url: '/student/agenda/getAllTeachers/' + $type,
          method: "GET"
        }).success(function (data) {
          angular.forEach(data, function(value, key) {
            $scope.selectedTeachers.push({ Id: value.id, Name: value.name });
          });
        });
      };

      $scope.GetValueLessonTeachersDropdown = function () {
        var teacherID = this.teacher.Id;
        $scope.teacherID = $.grep($scope.teachers, function (teacher) {
          return teacher.Id == teacherID;
        })[0].Id;

        return $scope.teacherID;
      };

      $scope.GetValueDurationDropdown = function () {
        var durationID = this.duration.Id;
        $scope.durationTime = $.grep($scope.durations, function (duration) {
          return duration.Id == durationID;
        })[0].Name;

        if($scope.durationTime == '30'){
          $scope.time_end = moment($scope.time_end, "HH.mm").add(-15, 'minutes').format("HH.mm");
        }
        if($scope.durationTime == '45'){
          $scope.time_end = moment($scope.time_end, "HH.mm").add(15, 'minutes').format("HH.mm");
        }

        return $scope.durationTime;
      };

      $scope.dynamicPopover = {
        templateUrl: '/popups/inplannen.html',
        title: '⁠⁠⁠Planlæg',
        isOpen: false,

        open: function open() {
          $scope.dynamicPopover.isOpen = true;
        },

        close: function close() {
          $scope.dynamicPopover.isOpen = false;
        }

      };

      $scope.placement = {
        options: [
          'top',
          'top-left',
          'top-right',
          'bottom',
          'bottom-left',
          'bottom-right',
          'left',
          'left-top',
          'left-bottom',
          'right',
          'right-top',
          'right-bottom'
        ],
        selected: 'bottom'
      };



      $scope.fillFieldsOnClick = function($event, time, date) {

        $scope.dynamicPopover.isOpen = true;

        var checkPlannedItem = ($($event.currentTarget).find('.agenda-item'));

        if(checkPlannedItem.length == 0) {
          time = moment(time, "HH.mm");
          $scope.time_start = time.format("HH.mm");
          $scope.time_end = time.add(30, 'minutes').format("HH.mm");
          $scope.date_popup = date;
          $scope.checkPlannedInLesson = false;
        } else if(checkPlannedItem.length == 1) {
          $scope.checkPlannedInLesson = true;
        }

      };

      $scope.bookLesson = function(){

        // @ For debugging
        //console.log($scope.lessonName);
        //console.log($scope.time_start);
        //console.log($scope.time_end);
        //console.log(moment($scope.date_popup, 'DD-MM-YYYY').format("YYYY-MM-DD"));
        //console.log($scope.teacherID);
        //console.log($scope.durationTime);

        $scope.dynamicPopover.isOpen = true;

        $http({
          url: 'agenda/user/scheduleLesson',
          method: "POST",
          cache: false,
          params: {lesson_type: $scope.lessonName,
            starttime:$scope.time_start,
            endtime:$scope.time_end,
            date: moment($scope.date_popup, 'DD-MM-YYYY').format("YYYY-MM-DD"),
            teacher: $scope.theSelectedTeacherID,
            duration: $scope.durationTime}
        }).success(function (data) {
          if(data != 'overlap') {
            $scope.overlap = false;
            $scope.fillTimeSchedule($scope.theSelectedTeacherID, $scope.theSelectedTeacher, true);
            $scope.dynamicPopover.close();
          } else if(data == 'overlap'){
            $scope.overlap = true;
            $scope.booked = false;
          }
        });

      };

    });
