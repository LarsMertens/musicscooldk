angular.module('studentCtrl', [])

.controller('studentController', function($scope, $http, $window, User) {

    $scope.data = '';
    $scope.deleteURL = '';
    $scope.loading = true;
    $scope.choices = [{id: 'choice1'}];

    $scope.getPage = function($page) {
        $scope.loading = true;
        User.getAll($page)
            .then(function (success) {
                $scope.data = success.data;
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.getPageInactive = function($page) {
        $scope.loading = true;
        User.getInactive($page)
            .then(function (success) {
                $scope.data = success.data;
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.nextPage = function($current_page, $last_page) {
        if($current_page != $last_page) {
            $scope.loading = true;
            User.getAll($current_page + 1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        }
    };

    $scope.previousPage = function($current_page) {
        if($current_page != 1) {
            $scope.loading = true;
            User.getAll($current_page - 1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        }
    };

    $scope.searchItems = function($search_value) {
        if($search_value != ''){
            $scope.loading = true;
            User.search($search_value)
                .then(function (success) {
                    $scope.data = success;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        } else {
            User.getAll(1)
                .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
        }
    };

    $scope.deleteItem = function($id, $current_page){
        $scope.loading = true;
        User.delete($id)
            .then(function (success) {
                User.getInactive($current_page)
                    .then(function (success) {
                        $scope.data = success.data;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };
    $scope.setInActive = function($id, $current_page){
        $scope.loading = true;
        User.setInActive($id)
            .then(function (success) {
                User.getAll($current_page)
                    .then(function (success) {
                        $scope.data = success.data;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };
    $scope.setActive = function($id, $current_page){
        $scope.loading = true;
        User.setActive($id)
            .then(function (success) {
                User.getInactive($current_page)
                    .then(function (success) {
                        $scope.data = success.data;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };
    $scope.getInfo = function($id){
        User.getAllChoices($id)
            .then(function (success) {
                $scope.choices = success.data;
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };


});


