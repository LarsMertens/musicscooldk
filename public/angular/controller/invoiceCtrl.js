angular.module('invoiceCtrl', [])

.controller('invoiceController', function($scope, $http, $window, Invoice, $filter) {

    $scope.data = '';
    $scope.deleteURL = '';
    $scope.loading = true;
    $scope.choices = [{id: 'choice1'}];

    $scope.getPage = function($page) {
        $scope.loading = true;
        Invoice.getAll($page)
            .then(function (success) {
                $scope.data = success.data;
                $scope.loading = false;

                console.log($scope.data);
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.nextPage = function($current_page, $last_page) {
        if($current_page != $last_page) {
            $scope.loading = true;
            Invoice.getAll($current_page + 1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        }
    };

    $scope.previousPage = function($current_page) {
        if($current_page != 1) {
            $scope.loading = true;
            Invoice.getAll($current_page - 1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        }
    };

    $scope.searchItems = function($search_value) {
        if($search_value != ''){
            $scope.loading = true;
            Invoice.search($search_value)
                .then(function (success) {
                    $scope.data = success;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        } else {
            Invoice.getAll(1)
                .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
        }
    };

    $scope.deleteItem = function($id, $current_page){
        $scope.loading = true;
        Invoice.delete($id)
            .then(function (success) {
                Invoice.getAll($current_page)
                    .then(function (success) {
                        $scope.data = success.data;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.addNewChoice = function() {
        var newItemNo = $scope.choices.length+1;
        $scope.choices.push({'id':'choice'+newItemNo});
    };

    $scope.removeChoice = function() {
        var lastItem = $scope.choices.length-1;
        $scope.choices.splice(lastItem);
    };

    $scope.removeWithRecordChoice = function($invoice_id) {
        var lastItem = $scope.choices.length-1;
        $scope.choices.splice(lastItem);
        Invoice.deleteWithRecordChoice($invoice_id)
            .then(function (success) {
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.loadChoices = function($invoice_id){
        Invoice.getAllChoices($invoice_id)
            .then(function (success) {
                $scope.choices = success.data;
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.searchStudentID = function($search_value) {
        if($search_value != ''){
            $scope.loading = true;
            Invoice.searchStudentID($search_value)
                .then(function (success) {
                    $scope.students = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        } else {
            // show no students?
        }
    };

    $scope.selectStudentID = function($studentID){
        $( "#mystudentid" ).val($studentID);
        $( "#mystudentid" ).trigger("input");
    };



    //$scope.changeStatus = function($id, $status){
    //    $scope.loading = true;
    //    $http({
    //        method: "POST",
    //        url: 'invoices/changeStatus/' + $status + '/' + $id,
    //        params: {page:  $scope.lastpage}
    //    }).success(function (data, status, headers, config) {
    //        $scope.data = data.data;
    //        $scope.loading = false;
    //    });
    //};
    //
    //$scope.searchItems = function() {
    //    if($scope.search_value != ''){
    //        $scope.loading = true;
    //        $http({
    //            method  : 'POST',
    //            url     : 'invoices/search/' + $scope.search_value
    //        }).success(function(data) {
    //            $scope.data = data;
    //            $scope.loading = false;
    //        });
    //    } else {
    //        $scope.init();
    //    }
    //};

});


