<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $table = 'teacher';

    public function students()
    {
        return $this->belongsToMany('App\Student', 'student_teacher');
    }

    public function media()
    {
        return $this->belongsToMany('App\Media');
    }

}
