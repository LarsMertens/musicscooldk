<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isStudent()
    {
        if($this->type == 'student'){
            return true;
        } else {
            return false;
        }
    }

    public function isTeacher()
    {
        if($this->type == 'teacher'){
            return true;
        } else {
            return false;
        }
    }

    public function isAdmin()
    {
        if($this->type == 'admin'){
            return true;
        } else {
            return false;
        }
    }

    public function naw()
    {
       return $this->hasOne('App\Naw');
    }

    public function student()
    {
        return $this->hasOne('App\Student');
    }

}
