<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoice';

    public function student()
    {
        return $this->hasOne('App\Student','id','student_id');
    }

}
