<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'student';

    public function teachers()
    {
        return $this->belongsToMany('App\Teacher', 'student_teacher')->withPivot('id');
    }

    public function materials()
    {
        return $this->hasMany('App\Material');
    }

    
}
