<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';

    public function extension()
    {
        return $this->hasMany('App\Extension','extension','extension');
    }
}
