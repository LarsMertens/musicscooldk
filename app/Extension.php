<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Extension extends Model
{
    protected $table = 'extension';

    public function media()
    {
        return $this->hasMany('App\media');
    }
}
