<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

use Intervention\Image\ImageManager;
use DB;
use Auth;
use App\Teacher;
use App\Media;
use App\Student_teacher;
use App\Extension;
use App\Material;
use App\Student;
use App\User;


class teacherMediaController extends Controller
{
    public function loadPage()
    {   $id = Auth::user()->id;
        $images = DB::table('media')->where('uploadedBy', $id)->get();;
        return view('teacher.media')->with(['images' => $images]);
    }

    

    public function uploadFiles($id) {
        $imageType = array("png", "jpg", "gif");
        $fileType = array("txt", "pdf","docx");
        $input = Input::all();
        $owner = $id;
        

        $extension = Input::file('file')->getClientOriginalExtension(); // getting file extension

        if(in_array($extension, $imageType)){
            $destinationPath = public_path() . '/img/'; // upload path
            $relativePath = '/img/';
            $fileNameWithExtension = Input::file('file')->getClientOriginalName();
            $fileName = strtok($fileNameWithExtension, '.');
            $upload_success = Input::file('file')->move($destinationPath, $fileNameWithExtension); // uploading file to given path
            $type="Image";
            $altPath = '';
        }elseif (in_array($extension, $fileType)) {
            $destinationPath = public_path() . '/files/'; // upload path
            $relativePath = '/files/';
            $fileNameWithExtension = Input::file('file')->getClientOriginalName();
            $fileName = strtok($fileNameWithExtension, '.');
            $upload_success = Input::file('file')->move($destinationPath, $fileNameWithExtension); // uploading file to given path
            $type="File";

                    switch ($extension) {
            case "pdf":
                    $altPath = "/img/pdflogo.png";
            case "word "||" docx "|| "doc":
                    $altPath = "/img/pdflogo.png";
            case "txt":
                    $altPath = "/img/pdflogo.png";   
                     }
        }
        else{
            return Response::json('Bad file type', 400);
        }

        if ($upload_success) {
           
            $test = DB::table('media')->get();

           DB::table('media')->insert([
                'name' => $fileName,
                'extension' => $extension,
                'alt' => $fileName,
                'path' => $relativePath . $fileNameWithExtension,
                'altPath' => $altPath,
                'type' => $type,
                'uploadedBy' => $owner]);
           
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
    }
    

    public function getAllImages()
    {
        $images = DB::table('media')->get();
        // $extension = DB::table('extensions')->get();
        // return Response::json(array('images'=>$images,'extension'=>$extension));

          // $images = Media::with('extension')->get();
          //$images DB::Media()->with('extension');
        return $images;
    }
  public function getAllImagesWithId($id)
    {
        $images = DB::table('media')->where('uploadedBy', $id)->get();
        // $extension = DB::table('extensions')->get();
        // return Response::json(array('images'=>$images,'extension'=>$extension));

          // $images = Media::with('extension')->get();
          //$images DB::Media()->with('extension');
        return $images;
    }
    public function loadSingleImage($id)
    {
        $selected_image = DB::table('media')
                                ->where('id', $id)
                                ->first();
        return Response::json($selected_image);
    }

    public function updateImageValues($id, $name, $alt)
    {
        $selected_image = DB::table('media')
            ->where('id', $id)
            ->update([
                'name' => $name,
                'alt' => $alt
            ]);
        return Response::json($selected_image);
    }

    public function deleteImage($name)
    {
        $image = DB::table('media')->where('name', $name)->first();
        File::delete(public_path() . $image->path);
        DB::table('media')
            ->where('name', $name)
            ->delete();
    }

    public function deleteHighlightedImage($id)
    {
        $image = DB::table('media')->where('id', $id)->first();
        File::delete(public_path() . $image->path);
        DB::table('media')->where('id', $id)->delete();
    }

    public function searchImages($name)
    {
        $images = DB::table('media')
            ->where('name', 'LIKE', "%$name%")
            ->orderBy('created_at', 'desc')
            ->get();
        return $images;
    }


    public function loadStudentMaterials(){
        $user_id = Auth::user()->id;                                                        //user_id    
        $teacher_id = teacher::where('user_id', $user_id)->select('id')->first();           //get teacher_id
        $media = media::where('uploadedBy', $user_id)->get();                               //all media from teacher   

        $getStudentTeacherID = student_teacher::where('teacher_id', $teacher_id->id)->get();    //Get student_teacher relation

        $getStudentTeacher = student_teacher::where('teacher_id', $teacher_id->id)->first();    //get student

       foreach ($getStudentTeacherID as $s) {


                $student = student::where('id', $s->student_id)->first();

                $student_user_id = user::where('id', $student->user_id)->first();

                $material_student = material::where('student_id', $student->id)->get();


        //    $studentArray = array_add(['student'=> $student_user_id ,  'material' => $material_student]);
            $studentArray = array_add(['student' => $student_user_id], 'material', $material_student);
        //    var_dump($studentArray);
        //$studentArray[] = array('student'=> $student_user_id ,  'material' => $material_student);
        }
    

      return view('teacher.students')->with(['data' => $media, 'students' => $studentArray]);

       //return $studentArray;
}





}
/*   $student_teacher_id = student_teacher::where('teacher_id', $teacher_id->id)->get('id');    //Get student_teacher relation
        $student_teacher_student_id = student_teacher::where('teacher_id', $teacher_id->id)->get('student_id'); //get student_id
        $material_student = material::where('student_id', $student_teacher_student_id)->get();

$student_teacher_id = Student_teacher::where('teacher_id', $teacher_id)->pluck('id')->toArray();

$agenda = DB::table('scheduled')->whereIn('student_teacher_id', $student_teacher_id)->get();