<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use Auth;


class UserController extends Controller
{
    public function logoutUser(){
        \Session::flash('logout', 'U bent succesvol uitgelogd.');
        Auth::logout();
        return Redirect::to('/');
    }
}
