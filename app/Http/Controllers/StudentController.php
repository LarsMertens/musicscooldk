<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;

use App\Http\Requests;
use App\Http\Requests\studentLoginRequest;
use App\Http\Requests\studentRegisterRequest;
use App\Http\Requests\editTeacherRequest;
use Auth;
use DB;
use Hash;
use App\Student;
use Carbon\Carbon;

class StudentController extends Controller
{
    public function loadPage()
    {
        return view('student.login');
    }

    public function loadLoginPage()
    {
        return view('student.login');
    }

    public function loadRegisterPage()
    {
        return view('student.register');
    }

    public function loadResetPasswordPage()
    {
        return view('auth.reset_password');
    }

    public function loginStudent(studentLoginRequest $request)
    {
        $userdata = array(
            'email' => Input::get('email'),
            'password' => Input::get('password'),
        );

        if (Auth::attempt($userdata, Input::get('remember_token'))) {

            return Redirect::to('/student/dashboard');

        } else {
            $errors = new MessageBag(['password' => ['Email or password incorrect...']]);
            return Redirect::to('/')
                ->withErrors($errors)
                ->withInput(Input::except('password'));
        }
    }

    public function registerStudent(studentRegisterRequest $request)
    {
        $gender = Input::get('gender');
        $firstname = Input::get('firstname');
        $insertion = Input::get('insertion');
        $lastname = Input::get('lastname');
        $current = Carbon::now();
        $current = new Carbon();

        if($insertion != ''){
            $fullname = $firstname . ' ' . $insertion . ' ' . $lastname;
        } else if($insertion == ''){
            $fullname = $firstname . ' ' . $lastname;
        }

        $address = Input::get('address');
        $house_number = Input::get('house_number');
        $town = Input::get('town');
        $zipcode = Input::get('zipcode');
        $phonenumber = Input::get('phonenumber');
        $email = Input::get('email');
        $password = Hash::make(Input::get('password'));

        $user_id = DB::table('user')->insertGetId([
                                'name' => $fullname,
                                'email' => $email,
                                'password' => $password,
                                'type' => 'student',
                            ]);

        DB::table('naw')->insert([
            'user_id' => $user_id,
            'gender' => $gender,
            'firstname' => $firstname,
            'insertion' => $insertion,
            'lastname' => $lastname,
            'address' => $address,
            'house_number' => $house_number,
            'town' => $town,
            'zipcode' => $zipcode,
            'phonenumber' => $phonenumber
        ]);
            DB::table('student')->insert([
            'user_id' => $user_id,
            'lessons_followed' => 0,
            'member_since' => $current
        ]);

        \Session::flash('success', 'U bent succesvol geregistreerd.');
    }

    public function logoutStudent()
    {
        \Session::flash('logout', 'U bent succesvol uitgelogd.');
        Auth::logout();
        return Redirect::to('/');
    }

    public function loadDashboard()
    {
        $user_id = Auth::user()->id;

        $student = Student::where('user_id', $user_id)->first();

        return view('student.dashboard')->with(['data' => $student]);
    }

    public function getTeacher($student_id)
    {
        $teacher = DB::table('student_teacher')
                    ->where('student_id', $student_id)
                    ->get();
        $teacher = DB::table('teacher')
                    ->where('id', $teacher[0]->teacher_id)
                    ->get();
        return $teacher;
    }


public function loadNaw()
    {
        $id = Auth::user()->id;
       
        $student = DB::table('user')
            ->where('id', $id)
            ->first();
        $naw = DB::table('naw')->where('user_id',$id)->first();
  
         return view('student.editStudent')->with(['data' => $student, 'naw' => $naw]);
        
      }



public function editStudent(editTeacherRequest $request, $id)
    {
       $values = $request->all();

  
       $email=$values['email'];
       $password=$values['password'];

       $email = $values['email'];
        $password = $values['password'];
        //$password_confirmation = $teacherInfo['password_confirm'];
        $passwordSecret = Hash::make($password);
        $gender = $values['Gender'];
        $firstname = $values['Firstname'];
        $insertion = $values['Insertion'];
        $lastname = $values['Lastname'];
        $address = $values['Address'];
        $house_number = $values['House_number'];
        $town = $values['Town'];
        $zipcode = $values['Zipcode'];
        $phonenumber = $values['Phonenumber'];
       


       if(!$password){
         DB::table('user')
        ->where('id', $id)
        ->update(
            ['email' => $email]    
                );

         DB::table('naw')
        ->where('user_id', $id)
        ->update(
            ['gender' => $gender,
            'firstname' => $firstname,
            'insertion' => $insertion,
            'lastname' => $lastname,
            'address' => $address,
            'house_number' => $house_number,
            'town' => $town,
            'zipcode' => $zipcode,
            'phonenumber' => $phonenumber]
        );
        }else{
            $hashPassword = Hash::make($password);
            DB::table('user')
            ->where('id', $id)
            ->update(
                ['name' => $name,
                'email' => $email,
                'password' => $hashPassword]

        );
            DB::table('naw')
        ->where('user_id', $id)
        ->update(
            ['gender' => $gender,
            'firstname' => $firstname,
            'insertion' => $insertion,
            'lastname' => $lastname,
            'address' => $address,
            'house_number' => $house_number,
            'town' => $town,
            'zipcode' => $zipcode,
            'phonenumber' => $phonenumber]
        );
       }
         \Session::flash('validationMessage', 'Succes!.');
        return Redirect('/student/naw/');
    }

}