<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Student;
use App\Material;
use DB;
use Auth;

class MaterialController extends Controller
{
    public function loadMaterialOverview()
    {
        $user_id = Auth::user()->id;
        $material_from_user = Student::where('user_id', $user_id)->first();
        return view('student.material')->with(['data' => $material_from_user]);
    }

}
