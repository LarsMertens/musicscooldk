<?php

namespace App\Http\Controllers;

use App\Student;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\addInvoiceRequest;
use App\Http\Requests\editInvoiceRequest;
use App\Company;
use App\Invoice;
use DB;
use Illuminate\Support\Facades\Redirect;

class InvoiceController extends Controller
{
    public function loadPage()
    {
        return view('admin.invoice.overview');
    }

    public static function loadTable()
    {
        $invoices_with_company = Invoice::with('student')
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return $invoices_with_company;
    }

    public function viewSingleItem($id)
    {
        $invoice_with_company = Invoice::with('student')
            ->where('id', $id)
            ->first();

        $user_id = $invoice_with_company->student->user_id;

        $invoice_with_naw = User::with('naw')
            ->where('id', $user_id)
            ->first();

        $invoice_lines = DB::table('invoice_line')->where('invoice_id',$invoice_with_company->id)->get();
        return view('admin.invoice.view')->with([
            'data' => $invoice_with_company,
            'user' => $invoice_with_naw,
            'invoice_lines' => $invoice_lines]);
    }

    public function loadAddPage()
    {
        return view('admin.invoice.add');
    }

    public function loadEditPage($id)
    {
        $invoice_with_company = Invoice::with('student')
            ->where('id', $id)
            ->first();

        return view('admin.invoice.edit')->with(['data' => $invoice_with_company]);
    }

    /**
     * Adds an Invoice
     */
    public function addInvoice(addInvoiceRequest $request)
    {
        $invoice = $request->all();

        $invoiceLineNames = $invoice['invoiceLineName'];
        $invoiceLineDescription = $invoice['invoiceLineDescription'];
        $invoiceLinePrices = $invoice['invoiceLinePrice'];

        $student_id = $invoice['student_id'];
        $name = $invoice['name'];
        $price = $invoice['price'];
        $description = $invoice['description'];
        $path = '...';
        $status = 'PENDING';

        $invoice_id = DB::table('invoice')->insertGetId(
            ['student_id' => $student_id,
                'name' => $name,
                'price' => $price,
                'description' => $description,
                'path' => $path,
                'status' => $status]
        );

        for($i=0; $i < count($invoiceLineNames); $i++){
            if($invoiceLineNames[$i] != "" && $invoiceLineDescription[$i] != "" && $invoiceLinePrices[$i] != "") {
                DB::table('invoice_line')->insert(
                    ['invoice_id' => $invoice_id,
                        'name' => $invoiceLineNames[$i],
                        'description' => $invoiceLineDescription[$i],
                        'price' => $invoiceLinePrices[$i]
                    ]
                );
            }
        }

        \Session::flash('validationMessage', 'U heeft succesvol een factuur aangemaakt.');
        return Redirect('/admin/invoice/add');
    }

    /**
     * @param $id = Invoice ID
     *
     * Edit an Invoice by ID
     */
    public function editInvoice(editInvoiceRequest $request, $id)
    {
        $invoice = $request->all();

        $invoiceLineNamesIDS = $invoice['invoiceLineNamesIDS'];
        $invoiceLineNames = $invoice['invoiceLineName'];
        $invoiceLineDescription = $invoice['invoiceLineDescription'];
        $invoiceLinePrices = $invoice['invoiceLinePrice'];

        $student_id = $invoice['student_id'];
        $name = $invoice['name'];
        $price = $invoice['price'];
        $description = $invoice['description'];
        $path = '...';
        $status = $invoice['status'];

        DB::table('invoice')
            ->where('id', $id)
            ->update(
                ['student_id' => $student_id,
                 'name' => $name,
                 'price' => $price,
                 'description' => $description,
                 'path' => $path,
                 'status' => $status]
            );

        for($i=0; $i < count($invoiceLineNames); $i++){
            if($invoiceLineNames[$i] != "" && $invoiceLineDescription[$i] != "" && $invoiceLinePrices[$i] != ""){
                if (DB::table('invoice_line')
                    ->where('id', $invoiceLineNamesIDS[$i])
                    ->where('invoice_id', $id)
                    ->exists()) {
                    DB::table('invoice_line')
                        ->where('id', $invoiceLineNamesIDS[$i])
                        ->where('invoice_id', $id)
                        ->update(
                            ['name' => $invoiceLineNames[$i],
                                'description' => $invoiceLineDescription[$i],
                                'price' => $invoiceLinePrices[$i]
                            ]
                        );
                } else {
                    DB::table('invoice_line')->insert(
                        ['invoice_id' => $id,
                            'name' => $invoiceLineNames[$i],
                            'description' => $invoiceLineDescription[$i],
                            'price' => $invoiceLinePrices[$i]
                        ]
                    );
                }
            }
        }

        \Session::flash('validationMessage', 'U heeft succesvol een factuur gewijzigd.');
        return Redirect('/admin/invoice/edit/' . $id);
    }

    /**
     * @param $id = Invoice ID
     *
     * Delete an Invoice by ID
     */
    public function deleteInvoice($id)
    {
        DB::table('invoice')
            ->where('id', $id)
            ->delete();
    }

    /**
     * @param $id = Invoice ID
     *
     * Pay an Invoice by ID
     */
    public function payInvoice($id)
    {
        // @TODO
    }

    /**
     * @param $id = Invoice ID
     *
     * Open an Invoice by ID
     */
    public function openInvoiceInPDF($id)
    {
        // @TODO
    }

    public function invoiceTemplate($id)
    {
        $invoice = DB::table('invoice')->where('id', $id)->first();
        return view('pdf.invoice')->with(['data' => $invoice]);
    }

    /**
     * @param $id = Invoice ID
     *
     * Print an Invoice by ID
     */
    public function printInvoice($id)
    {
        // @TODO
    }

    /**
     * @param $id = Invoice ID
     *
     * Change an Invoice Status by ID
     */
    public function changeStatus($id)
    {
        // @TODO
    }

    /**
     * @param $id = Invoice ID
     * @return mixed = All Invoices with ID like $id
     *
     * Search All Invoices by invoice ID
     */
    public function searchInvoices($id)
    {
        $invoices = DB::table('invoice')
            ->where('id', 'LIKE', "%$id%")
            ->orderBy('created_at', 'desc')
            ->get();
        return $invoices;
    }

    public function searchStudentIDS($name){
        $students = User::with('student')
            ->where('name', 'LIKE', "%$name%")
            ->where('type','student')
            ->take(3)
            ->get();
        return $students;
    }

    public function loadChoices($invoice_id){
        $invoice_lines = DB::table('invoice_line')
            ->where('invoice_id', $invoice_id)
            ->get();
        return $invoice_lines;
    }

    public function deleteLastChoice($invoice_id){
        DB::table('invoice_line')
            ->where('invoice_id', $invoice_id)
            ->orderBy("id", "DESC")
            ->take(1)
            ->delete();
    }

    public function hasNumber($text) {
        return preg_match("/\d/", $text);
    }

}
