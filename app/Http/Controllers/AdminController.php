<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;

use App\Http\Requests;
use App\Http\Requests\editTeacherRequest;
use App\Http\Requests\addTeacherRequest;
use App\Http\Requests\adminLoginRequest;
use App\Http\Requests\addProductRequest;
use App\Http\Requests\editProductRequest;
use App\Profile;
use Auth;
use App\User;
use App\Product;
use DB;
use Hash;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function loadPage()
    {
        return view('admin.company.overview');
    }

    public function loadLoginPage()
    {
        return view('admin.login');
    }

    public function loadOverviewPage()
    {
        return view('admin.dashboard');
    }

    public function loadResetPasswordPage()
    {
        return view('auth.reset_password');
    }

    public static function loadTable()
    {
        $company = company::paginate(10);
        return $company;
    }

    public function loginAdmin(adminLoginRequest $request)
    {
        $userdata = array(
            'email' => Input::get('email'),
            'password' => Input::get('password'),
        );

        if (Auth::attempt($userdata, Input::get('remember_token'))) {

            return Redirect::to('/admin/overview');

        } else {
            $errors = new MessageBag(['password' => ['Email or password incorrect...']]);
            return Redirect::to('admin')
                ->withErrors($errors)
                ->withInput(Input::except('password'));
        }
    }

    public function logoutAdmin()
    {
        \Session::flash('logout', 'U bent succesvol uitgelogd.');
        Auth::logout();
        return Redirect::to('/');
    }


  public static function setInactiveTeacher($id){

         DB::table('user')
        ->where('id', $id)
        ->update(
            ['status' => 'Inactief']
                );
    }

    public static function setActiveTeacher($id){

         DB::table('user')
        ->where('id', $id)
        ->update(
            ['status' => 'actief']
                );
        
    }

        public static function loadTableTeachersInactive()
    {

        $teacher = user::where('type','teacher')->with('naw')->where('status', 'Inactief')->paginate(15);
        
          return $teacher;
    }

    public function deleteTeacher($id)
    {


       $student = DB::table('user')
            ->where('id', $id)
            ->first();
        $naw = DB::table('naw')->where('user_id',$id)->first();
     

            if(DB::table('student_teacher')->where('teacher_id', $naw->user_id) == null){
                DB::table('user')
                ->where('id', $id)
                ->delete();
            }
            else{
                \Session::flash('validationMessage', 'Error, this Teacher cannot be removed, this student is connected to a teacher/media');
            }
    }



    public static function loadTeacherOverviewInactive()
    {

         return view('admin.overviewInactive');
    }
    

    public static function loadTableTeachers()
    {

        $teachers = user::where('type','teacher')->where('status', 'Actief')->paginate(15);
        
          return $teachers;
    }


    public static function loadteacherOverview()
    {

         return view('admin.teacherOverview');
    }

    public static function searchTeachers($name)
    {

        $searchResult = user::where('name','LIKE',"%$name%")->where('type', 'teacher')->orderBy('created_at', 'desc')->get();
        
          return $searchResult;

    }

     public function loadEditteacherPage($id)
    {
       

       $teacher = DB::table('user')
            ->where('id', $id)
            ->first();
        $naw = DB::table('naw')->where('user_id',$id)->first();
         if($naw){
         return view('admin.teachUpdate')->with(['data' => $teacher, 'naw' => $naw]);
         }
        else{
            Session::flash('validationMessage', 'Error, this teacher has no NAW records/Naw user_Id');
        return Redirect('/admin/teacher/overview');
    }
}


public function editTeacher(editTeacherRequest $request, $id)
    {
        $values = $request->all();
      $email=$values['email'];
       $password=$values['password'];

       $email = $values['email'];
        $password = $values['password'];
        //$password_confirmation = $teacherInfo['password_confirm'];
        $passwordSecret = Hash::make($password);
         $gender = Input::get('gender');
        $firstname = $values['Firstname'];
        $insertion = $values['Insertion'];
        $lastname = $values['Lastname'];

        $fullname = '';
        if($insertion == '') {
            $fullname = $firstname . " " . $lastname;
        } else if($insertion != ''){
            $fullname = $firstname . " " . $insertion . " " . $lastname;
        }

        $address = $values['Address'];
        $house_number = $values['House_number'];
        $town = $values['Town'];
        $zipcode = $values['Zipcode'];
        $phonenumber = $values['Phonenumber'];
       


       if(!$password){
         DB::table('user')
        ->where('id', $id)
        ->update(
            ['name' => $fullname,
             'email' => $email]
                );

         DB::table('naw')
        ->where('user_id', $id)
        ->update(
            ['gender' => $gender,
            'firstname' => $firstname,
            'insertion' => $insertion,
            'lastname' => $lastname,
            'address' => $address,
            'house_number' => $house_number,
            'town' => $town,
            'zipcode' => $zipcode,
            'phonenumber' => $phonenumber]
        );
        }else{
            $hashPassword = Hash::make($password);
            DB::table('user')
            ->where('id', $id)
            ->update(
                ['name' => $fullname,
                'email' => $email,
                'password' => $hashPassword]

        );
            DB::table('naw')
        ->where('user_id', $id)
        ->update(
            ['gender' => $gender,
            'firstname' => $firstname,
            'insertion' => $insertion,
            'lastname' => $lastname,
            'address' => $address,
            'house_number' => $house_number,
            'town' => $town,
            'zipcode' => $zipcode,
            'phonenumber' => $phonenumber]
        );
       }
         \Session::flash('validationMessage', 'Succes!.');
        return Redirect('/admin/teachUpdate/' . $id);
    }


    public function  loadSingleTeacher($id)
    {
         $teacher = DB::table('user')
            ->where('id', $id)
            ->first();
        $naw = DB::table('naw')->where('user_id',$id)->first();
        return view('admin.teacherSingleView')->with(['data' => $teacher,
                                                 'naw' => $naw]);

    }

    public function loadAddPage()
    {
        return view('admin.addTeacher');
    }

    public function addTeacher(addTeacherRequest $request)
    {


        $teacherInfo = $request->all();
        $email = $teacherInfo['email'];
        $password = $teacherInfo['password'];
        $gender = Input::get('gender');
   
       
        $firstname = $teacherInfo['Firstname'];
        $insertion = $teacherInfo['Insertion'];
        $lastname = $teacherInfo['Lastname'];

        $address = $teacherInfo['Address'];
        $house_number = $teacherInfo['House_number'];
        $town = $teacherInfo['Town'];
        $zipcode = $teacherInfo['Zipcode'];
        $phonenumber = $teacherInfo['Phonenumber'];

        $password_confirmation = $teacherInfo['password_confirm'];
        $passwordSecret = Hash::make($password);
        $current = Carbon::now();
        $current = new Carbon();

        $fullname = '';
        if($insertion == '') {
            $fullname = $firstname . " " . $lastname;
        } else if($insertion != ''){
            $fullname = $firstname . " " . $insertion . " " . $lastname;
        }

        $teacher = DB::table('user')->insertGetId(
            ['email' => $email,
             'password' => $passwordSecret,
             'type' => "teacher",
             'name' =>  $fullname]
        );

        $teacherNaw = DB::table('naw')->insert(
            ['user_id' => $teacher,
            'gender' => $gender,
            'firstname' => $firstname,
            'insertion' => $insertion,
            'lastname' => $lastname,
            'address' => $address,
            'house_number' => $house_number,
            'town' => $town,
            'zipcode' => $zipcode,
            'phonenumber' => $phonenumber]
        );

         DB::table('teacher')->insert([
            'user_id' => $teacher,
            'created_at' => $current
        ]);


        \Session::flash('validationMessage', 'Teacher added.');
        return view('admin.teacherOverview');
    
    }


/*******************Student(admin)*******************/




    public static function loadTableStudents()
    {

        $student = user::where('type','student')->with('naw')->where('status', 'Actief')->paginate(15);
        
          return $student;
    }

    public static function setInactive($id){

         DB::table('user')
        ->where('id', $id)
        ->update(
            ['status' => 'Inactief']
                );
    }

    public static function setActive($id){

         DB::table('user')
        ->where('id', $id)
        ->update(
            ['status' => 'actief']
                );
        
    }

        public static function loadTableStudentsInactive()
    {

        $student = user::where('type','student')->with('naw')->where('status', 'Inactief')->paginate(15);
        
          return $student;
    }

    public function deleteStudent($id)
    {


       $student = DB::table('user')
            ->where('id', $id)
            ->first();
        $naw = DB::table('naw')->where('user_id',$id)->first();
     

            if(DB::table('student_teacher')->where('student_id', $naw->user_id) == null){
                DB::table('user')
                ->where('id', $id)
                ->delete();
            }
            else{
                \Session::flash('validationMessage', 'Error, this student canot be removed, this student is connected to a teacher/media');
            }
    }



    public static function loadStudentOverview()
    {

         return view('admin.student/overview');
    }

    public static function loadStudentOverviewInactive()
    {

         return view('admin.student/overviewInactive');
    }

    public static function searchStudent($name)
    {

        $searchResult = user::where('name','LIKE',"%$name%")->where('type', 'student')->orderBy('created_at', 'desc')->get();
        
          return $searchResult;

    }

     public function LoadEditStudent($id)
    {
       $student = DB::table('user')
            ->where('id', $id)
            ->first();
        $naw = DB::table('naw')->where('user_id',$id)->first();
        if($naw){
         return view('admin.student.studentUpdate')->with(['data' => $student,
                                                            'naw' => $naw]);
        }
        else{
        \Session::flash('validationMessage', 'Error, this student has no NAW records/Naw user_Id');
        return Redirect('/admin/student/overview');
        }
    }


public function editStudent(editTeacherRequest $request, $id)
    {
       $values = $request->all();

  
       $email=$values['email'];
       $password=$values['password'];

       $email = $values['email'];
        $password = $values['password'];
        //$password_confirmation = $teacherInfo['password_confirm'];
        $passwordSecret = Hash::make($password);
        $gender = Input::get('gender');
        $firstname = $values['Firstname'];
        $insertion = $values['Insertion'];
        $lastname = $values['Lastname'];

        $fullname = '';
        if($insertion == '') {
            $fullname = $firstname . " " . $lastname;
        } else if($insertion != ''){
            $fullname = $firstname . " " . $insertion . " " . $lastname;
        }

        $address = $values['Address'];
        $house_number = $values['House_number'];
        $town = $values['Town'];
        $zipcode = $values['Zipcode'];
        $phonenumber = $values['Phonenumber'];
       


       if(!$password){
         DB::table('user')
        ->where('id', $id)
        ->update(
            ['name' => $fullname,
             'email' => $email]
                );

         DB::table('naw')
        ->where('user_id', $id)
        ->update(
            ['gender' => $gender,
            'firstname' => $firstname,
            'insertion' => $insertion,
            'lastname' => $lastname,
            'address' => $address,
            'house_number' => $house_number,
            'town' => $town,
            'zipcode' => $zipcode,
            'phonenumber' => $phonenumber]
        );
        }else{
            $hashPassword = Hash::make($password);
            DB::table('user')
            ->where('id', $id)
            ->update(
                ['name' => $fullname,
                'email' => $email,
                'password' => $hashPassword]

        );
            DB::table('naw')
        ->where('user_id', $id)
        ->update(
            ['gender' => $gender,
            'firstname' => $firstname,
            'insertion' => $insertion,
            'lastname' => $lastname,
            'address' => $address,
            'house_number' => $house_number,
            'town' => $town,
            'zipcode' => $zipcode,
            'phonenumber' => $phonenumber]
        );
       }
         \Session::flash('validationMessage', 'Succes!.');
        return Redirect('/admin/studentUpdate/' . $id);
    }


    public function loadSingleStudent($id)
    {
         $student = DB::table('user')
            ->where('id', $id)
            ->first();
        $naw = DB::table('naw')->where('user_id',$id)->first();
        return view('admin.student.singleView')->with(['data' => $student,
                                                 'naw' => $naw]);

    }

    public function loadAddPageStudent()
    {
        return view('admin.student/add');
    }

    public function addStudent(addTeacherRequest $request)
    {
      
      
        $studentInfo = $request->all();
        $email = $studentInfo['email'];
        $password = $studentInfo['password'];
        //$password_confirmation = $teacherInfo['password_confirm'];
        $passwordSecret = Hash::make($password);
        $gender = Input::get('gender');
        $firstname = $studentInfo['Firstname'];
        $insertion = $studentInfo['Insertion'];
        $lastname = $studentInfo['Lastname'];

        $fullname = '';
        if($insertion == '') {
            $fullname = $firstname . " " . $lastname;
        } else if($insertion != ''){
            $fullname = $firstname . " " . $insertion . " " . $lastname;
        }

        $address = $studentInfo['Address'];
        $house_number = $studentInfo['House_number'];
        $town = $studentInfo['Town'];
        $zipcode = $studentInfo['Zipcode'];
        $phonenumber = $studentInfo['Phonenumber'];

        $student = DB::table('user')->insertGetId(
            ['name' => $fullname,
             'email' => $email,
             'password' => $passwordSecret,
             'type' => "student"]
        );

        $studentNaw = DB::table('naw')->insert(
            ['user_id' => $student,
            'gender' => $gender,
            'firstname' => $firstname,
            'insertion' => $insertion,
            'lastname' => $lastname,
            'address' => $address,
            'house_number' => $house_number,
            'town' => $town,
            'zipcode' => $zipcode,
            'phonenumber' => $phonenumber]
        );



        \Session::flash('validationMessage', 'Student added.');
        return view('admin.student.overview');
    
    }





/*******************pricing(admin)*******************/




    public static function loadTablePricing()
    {

         $pricing = DB::table('product')->paginate(15);
        
          return $pricing;
    }


    public static function loadPricingOverview()
    {

         return view('admin.pricing/overview');
    }

    public static function searchPricing($name)
    {

        // $searchResult = product::where('name','LIKE',"%$name%")->orderBy('created_at', 'desc')->get();
        
        // return $searchResult;

       $searchResult  =  DB::table('product')
            ->where('name','LIKE',"%$name%")
            ->orWhere('price','LIKE',"%$name%")
            ->get();
            return $searchResult;
   }
     public function LoadEditPricing($id)
    {
       $pricing = DB::table('product')
            ->where('id', $id)
            ->first();

         return view('/admin/pricing/editPrice')->with(['data' => $pricing]);


    }


public function editPricing(editProductRequest $request, $id)
    {
      
        $pricing = $request->all();

        $name = $pricing['name'];
        $description = $pricing['description'];
        $price = $pricing['price'];
        $path = $pricing['singleImage'];

        $product = DB::table('product')->where('id', $id)->update(
            ['name' => $name,
            'description' => $description,
            'price' => $price,
            'path' => $path
            ]
        );

        \Session::flash('validationMessage', 'Product had been Changed.');
        return view('admin.pricing.overview');
    
    }
       
    


    public function loadSingleProduct($id)
    {
         $pricing = DB::table('product')
            ->where('id', $id)
            ->first();
        return view('admin.pricing.singleView')->with(['data' => $pricing]);

    }

    public function loadAddPagePricing()
    {
        return view('admin.pricing/add');
    }

    public function addPricing(addProductRequest $request)
    {
      
      
        $studentInfo = $request->all();
        $name = $studentInfo['Name'];
        $description = $studentInfo['Description'];
        $price = $studentInfo['Price'];
        $path = $studentInfo['singleImage'];
        

        $product = DB::table('product')->insert(
            ['name' => $name,
            'description' => $description,
            'price' => $price,
            'path' => $path
            ]
        );



        \Session::flash('validationMessage', 'Product had been added.');
        return view('admin.pricing.overview');
    
    }

}
