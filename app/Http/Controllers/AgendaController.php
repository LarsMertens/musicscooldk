<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Student;
use App\Teacher;
use App\Student_teacher;
use App\Scheduled;
use Auth;
use DB;
use DateTime;
use Carbon\Carbon;

class AgendaController extends Controller
{

    public function getAllUserLessons()
    {
        date_default_timezone_set('Europe/Amsterdam');

        $user_id = Auth::user()->id;
        $student = DB::table('student')->where('user_id', $user_id)->select('id')->first();
        $student_teacher_ids = Student_teacher::where('student_id', $student->id)->pluck('id')->toArray();

        $nr_of_lessons = DB::table('scheduled')
            ->whereIn('student_teacher_id', $student_teacher_ids)
            ->where('endtime', '>=', Carbon::now())
            ->where('type', 'lesson')
            ->get();

        return $nr_of_lessons;
    }

    public function loadUserSchedule($teacher_id, $days)
    {
        $student_teacher_id = Student_teacher::where('teacher_id', $teacher_id)->pluck('id')->toArray();

        $agenda = DB::table('scheduled')->whereIn('student_teacher_id', $student_teacher_id)
        ->whereBetween('date',array(Carbon::now()->subDay(1)->addDays($days), Carbon::now()->addDays($days)->addWeek()))
        ->get();

        return $agenda;
    }

    public function loadUserLessons($teacher_id)
    {
        // Get current user...
        $user_id = Auth::user()->id;

        // Get student from user
        $student_id = DB::table('student')->where('user_id', $user_id)->select('id')->first();

        // Get student_teacher id
        $student_teacher_id = Student_teacher::where(['teacher_id' => $teacher_id, 'student_id' => $student_id->id])->select('id')->first();

        return $student_teacher_id['id'];
    }

    /**
     * @TODO : CHECK IF LESSON IS ALREADY IN DATABASE FOR THAT TIME
     * @param $lesson_type
     * @param $starttime
     * @param $endtime
     * @param $date
     * @param $teacher
     */
    public function scheduleLesson(Request $request)
    {
        $user_id = Auth::user()->id;
        $lesson_type = $request->input('lesson_type');
        $date = $request->input('date');
        $starttime = Carbon::parse($request->input('starttime') . ' ' . $date);
        $endtime = Carbon::parse($request->input('endtime') . ' ' . $date);
        $teacherid = $request->input('teacher');
        $duration = $request->input('duration');

        $student = DB::table('student')->where('user_id', $user_id)->select('id')->first();
        $student_teacher = DB::table('student_teacher')->where(['student_id' => $student->id, 'teacher_id' => $teacherid])->first();

        $thereIsALesson = DB::table('scheduled')->where(['student_teacher_id' => $student_teacher->id,
                                                         'date' => $date,
                                                        ])
            ->whereBetween('endtime',array($starttime->addMinute(), $endtime->subMinute()))
            ->orWhereBetween('starttime',array($starttime, $endtime))
            ->get();

        if(!$thereIsALesson) {
            DB::table('scheduled')->insert([
                'student_teacher_id' => $student_teacher->id,
                'type' => 'lesson',
                'lesson_type' => $lesson_type,
                'date' => $date,
                'starttime' => $starttime->subMinute(),
                'endtime' => $endtime->addMinute(),
                'duration' => $duration,
                'message' => 'Les'
            ]);
        } else if($thereIsALesson){
            return 'overlap';
        }
    }

    public function getNextLesson()
    {   
        $user_id = Auth::user()->id;
        $current = Carbon::now();
        $teacher_id = student::where('user_id', $user_id)->select('id')->first();

        if($teacher_id == null){

        $teacher_id = student::where('user_id', $user_id)->select('id')->first();
        $student_teacher_id = Student_teacher::where('teacher_id', $teacher_id->id)->select('id')->first();
        $getNextLesson = scheduled::where('student_teacher_id', $student_teacher_id->id)
                                  ->where('starttime', '>=', $current)->orderby('starttime', 'asc')->get();
       
        return $getNextLesson;
    }
    else{
                return "Next lesson not found, please make sure a next lesson is set.";
        }
    }

    public function getNrOfLessonsPlanned()
    {
        $user_id =  Auth::user()->id;
        $nr_of_lessons = Agenda::orderBy('starttime', 'asc')
            ->where('user_id', $user_id)
            ->where('starttime', '>',  Carbon::now())
            ->get()
            ->count();
        return $nr_of_lessons;
    }

    public function loadAllTeachersByType($type)
    {
        return DB::table('teacher')->where('type', $type)->get();
    }

}

/*   
public function getNextLesson()
    {
        $user_id =  Auth::user()->id;
        $student = Student::find($user_id);
        $student = $student->teachers()->get();
        $student_teacher_id = $student[0]->pivot->id;

        $next_lesson = DB::table('scheduled')
            ->select('starttime')
            ->orderBy('starttime', 'asc')
            ->where('student_teacher_id', $student_teacher_id)
            ->where('starttime', '>',  Carbon::now())
            ->get();
        return $next_lesson;
    }