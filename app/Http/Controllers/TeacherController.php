<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use Log;
use App\Http\Requests;
use App\Http\Requests\teacherLoginRequest;
use App\Http\Requests\editTeacherRequest ;
use Auth;

use App\Teacher;
use App\Student;
use App\Material;
use App\Media;
use App\Student_teacher;
use App\Scheduled;
use Carbon\Carbon;
use DB;
use Hash;

class TeacherController extends Controller
{
    public function loadPage()
    {
        return view('teacher.login');
    }

    public function loadLoginPage()
    {
        return view('teacher.login');
    }

    public function loadStudentsTable()
    {
        return view('teacher.students');
    }

    public function loadStudentMaterials($stID,$teachID)
    {
        $student = student::where('id',$stID)->first();
        $studentMaterial = material::where('student_id',$stID)->where('teacher_id',$teachID)->get(); 
        $media = media::where('uploadedBy', $teachID)->whereNotIn('id', function($q) use($stID){
          $q->select('media_id')
            ->from('material')->where('student_id',$stID);
            // more where conditions
      })->get();

        return view('teacher.studentMaterials')->with([ 'studentMaterial' => $studentMaterial,'media' => $media]);

    }
    public function updateStudentMedia()
    {

        $Data = Input::all();


        if(count($Data) > 0){
                    $test = DB::table('material')->insert(
            ['student_id' => 5,
            'teacher_id' => 4,
            'media_id' => $Data['id']['0']]
        );
        }
 

    }

    public function loadOverviewPage()
    {
        $user_id = Auth::user()->id;

        $teacher = teacher::where('user_id', $user_id)->first();
        $media = media::where('uploadedBy', $user_id)->count();

        return view('teacher.overview')->with(['data' => $teacher, 'count' => $media]);
    }

    public function loadResetPasswordPage()
    {
        return view('auth.reset_password');
    }

    public function getAllTeachers()
    {
        return DB::table('teacher')->get();
    }

    public function loginTeacher(teacherLoginRequest $request)
    {
        $userdata = array(
            'email' => Input::get('email'),
            'password' => Input::get('password'),
        );

        if (Auth::attempt($userdata, Input::get('remember_token'))) {

            return Redirect::to('/teacher/dashboard');

        } else {
            $errors = new MessageBag(['password' => ['Email or password incorrect...']]);
            return Redirect::to('teacher')
                ->withErrors($errors)
                ->withInput(Input::except('password'));
        }
    }

    public function logoutTeacher()
    {
        \Session::flash('logout', 'U bent succesvol uitgelogd.');
        Auth::logout();
        return Redirect::to('/');
    }

    public function getNextLesson()
    {
        $user_id = Auth::user()->id;
        $current = Carbon::now();
        $current = new Carbon();

        $teacher_id = teacher::where('user_id', $user_id)->select('id')->first();
        $student_teacher_id = student_teacher::where('teacher_id', $teacher_id->id)->select('id')->first();
        $getNextLesson = scheduled::where('student_teacher_id', $student_teacher_id->id)->whereDate('starttime', '>=', $current)->orderby('starttime', 'asc')->get();
       

        return $getNextLesson;
    }

 public function loadNaw()
    {
        $id = Auth::user()->id;
       
        $teacher = DB::table('user')
            ->where('id', $id)
            ->first();
        $naw = DB::table('naw')->where('user_id',$id)->first();
  
         return view('teacher.editTeacher')->with(['data' => $teacher, 'naw' => $naw]);
        
      }



public function editTeacher(editTeacherRequest $request, $id)
    {
        $values = $request->all();

        $email=$values['email'];
        $password=$values['password'];

        $email = $values['email'];
        $password = $values['password'];
        //$password_confirmation = $teacherInfo['password_confirm'];
        $passwordSecret = Hash::make($password);
        $gender = $values['Gender'];
        $firstname = $values['Firstname'];
        $insertion = $values['Insertion'];
        $lastname = $values['Lastname'];
        $fullname = '';

        if($insertion != ''){
            $fullname = $firstname . ' ' . $insertion . ' ' . $lastname;
        } else if($insertion == ''){
            $fullname = $firstname . ' ' . $lastname;
        }

        $address = $values['Address'];
        $house_number = $values['House_number'];
        $town = $values['Town'];
        $zipcode = $values['Zipcode'];
        $phonenumber = $values['Phonenumber'];
       

       if(!$password){
         DB::table('user')
        ->where('id', $id)
        ->update(
            ['name' => $fullname,
             'email' => $email]
                );

         DB::table('naw')
        ->where('user_id', $id)
        ->update(
            ['gender' => $gender,
            'firstname' => $firstname,
            'insertion' => $insertion,
            'lastname' => $lastname,
            'address' => $address,
            'house_number' => $house_number,
            'town' => $town,
            'zipcode' => $zipcode,
            'phonenumber' => $phonenumber]
        );
        }else{
            $hashPassword = Hash::make($password);
            DB::table('user')
            ->where('id', $id)
            ->update(
                ['name' => $fullname,
                'email' => $email,
                'password' => $hashPassword]

        );
            DB::table('naw')
        ->where('user_id', $id)
        ->update(
            ['gender' => $gender,
            'firstname' => $firstname,
            'insertion' => $insertion,
            'lastname' => $lastname,
            'address' => $address,
            'house_number' => $house_number,
            'town' => $town,
            'zipcode' => $zipcode,
            'phonenumber' => $phonenumber]
        );
       }
         \Session::flash('validationMessage', 'Succes!.');
        return Redirect('/teacher/naw/');
    }

}
