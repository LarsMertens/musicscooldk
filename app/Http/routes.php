<?php

/**
 * Student User Routes
 */
Route::get('/', 'StudentController@loadLoginPage');
Route::get('/student/register', 'StudentController@loadRegisterPage');
Route::get('/student/reset-password', 'StudentController@loadResetPasswordPage');

Route::post('/student/login', 'StudentController@loginStudent');
Route::post('/student/registerStudent', 'StudentController@registerStudent');
Route::post('/student/password/email', ['as' => 'auth.password.email', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
Route::post('/student/password/reset', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@reset']);

Route::group(['middleware' => ['student']], function () {

    Route::get('/student/dashboard', 'StudentController@loadDashboard');
    Route::get('/student/agenda', function () { return view('student.agenda'); });
    Route::get('/student/mijn-lessen', function () { return view('mijn_lessen'); });
    Route::get('/student/material', 'MaterialController@loadMaterialOverview');
    Route::get('/student/mijn-betalingen', function () { return view('mijn_betalingen'); });
    Route::get('/student/shop', function () { return view('shop'); });

    Route::get('/student/agenda/getUserSchedule/getUserLessons/', 'AgendaController@getAllUserLessons');
    Route::get('/student/agenda/getUserSchedule/getUserLessons/{teacher_id}/{days}', 'AgendaController@loadUserSchedule');
    Route::get('/student/agenda/getUserSchedule/{teacher_id}', 'AgendaController@loadUserLessons');
    Route::get('/student/agenda/getAllTeachers/{type}', 'AgendaController@loadAllTeachersByType');

    Route::get('/student/api/teachers', 'TeacherController@getAllTeachers');

    // Route::get('/student/agenda/getNextLesson', 'AgendaController@getNextLesson');
    // Route::get('/student/agenda/getNrOfLessonsPlanned', 'AgendaController@getNrOfLessonsPlanned');

    Route::get('/student/agenda/getNextLesson', 'AgendaController@getNextLesson');
    Route::get('/student/agenda/getNrOfLessonsPlanned', 'AgendaController@getNrOfLessonsPlanned');

    Route::post('/student/agenda/user/scheduleLesson', 'AgendaController@scheduleLesson');

    Route::get('/student/naw', 'StudentController@loadNaw');
    Route::Post('/student/edit/{id}','StudentController@editStudent');

    Route::post('/student/uitloggen', 'StudentController@logoutStudent');
});


/***
 * Admin User Routes
 */
Route::get('/admin', 'AdminController@loadLoginPage');
Route::get('/admin/reset-password', 'AdminController@loadResetPasswordPage');

Route::post('/admin/login', 'AdminController@loginAdmin');

Route::group(['middleware' => ['admin']], function () {
    Route::get('/admin/overview', 'AdminController@loadOverviewPage');

    /**
     * Admin Invoice Routes
     */
    Route::get('/admin/invoice', 'InvoiceController@loadPage');
    Route::get('/admin/invoice/view/{id}', 'InvoiceController@viewSingleItem');
    Route::get('/admin/invoice/view/pdf/{id}', 'InvoiceController@openInvoiceInPDF');
    Route::get('/admin/invoice/add', 'InvoiceController@loadAddPage');
    Route::get('/admin/invoice/edit/{id}', 'InvoiceController@loadEditPage');
    Route::get('/admin/api/invoices', 'InvoiceController@loadTable');
    Route::get('/admin/api/invoices/choices/{id}', 'InvoiceController@loadChoices');

    Route::post('/admin/invoice/addInvoice', 'InvoiceController@addInvoice');
    Route::post('/admin/invoice/editInvoice/{id}', 'InvoiceController@editInvoice');
    Route::post('/admin/invoice/deleteInvoice/{id}', 'InvoiceController@deleteInvoice');
    Route::post('/admin/invoice/search/{id}', 'InvoiceController@searchInvoices');
    Route::post('/admin/invoice/search/studentid/{name}', 'InvoiceController@searchStudentIDS');
    Route::post('/admin/api/invoices/delete/choice/{id}', 'InvoiceController@deleteLastChoice');


    /**
     * teachersOverview -> admin
     */
    Route::get('/admin/teacherOverview', 'AdminController@loadteacherOverview');
    Route::Post('/admin/teacher/search/{id}','AdminController@searchTeachers'); 
    Route::Get('/admin/teachUpdate/{id}','AdminController@loadEditteacherPage');
    Route::Post('/admin/teachers/edit/{id}','AdminController@editTeacher');
    Route::get('/admin/teacherSingleView/{id}', 'AdminController@loadSingleTeacher'); 
    Route::get('/admin/teacher/add', 'AdminController@loadAddPage');
    Route::post('/admin/addTeach', 'AdminController@addTeacher');

    Route::get('/admin/api/teachers', 'AdminController@loadTableteachers');

    Route::get('/admin/api/teachersInactive', 'AdminController@loadTableTeachersInactive');
    Route::Post('/admin/teacherSetInactive/{id}', 'AdminController@setInactiveTeacher');
    Route::Post('/admin/teacherSetActive/{id}', 'AdminController@setActiveTeacher');
    Route::Post('/admin/teacher/deleteStudent/{id}', 'AdminController@deleteteachert');
    Route::get('/admin/overviewInactive', 'AdminController@loadTeacherOverviewInactive');

    /**
     * studentsOverview -> admin
     */
    Route::get('/admin/student/add', 'AdminController@loadAddPageStudent');
    Route::get('/admin/student/overview', 'AdminController@loadStudentOverview');
    Route::get('/admin/student/overviewInactive', 'AdminController@loadStudentOverviewInactive');
    Route::get('/admin/student/singleView/{id}', 'AdminController@loadSingleStudent'); 

    Route::Post('/admin/student/search/{id}','AdminController@searchStudent'); 
    Route::Get('/admin/studentUpdate/{id}','AdminController@LoadEditStudent');
    Route::Post('/admin/student/edit/{id}','AdminController@editStudent');
    Route::post('/admin/addStudent', 'AdminController@addStudent');
    Route::get('/admin/api/students', 'AdminController@loadTableStudents');
    Route::get('/admin/api/studentsInactive', 'AdminController@loadTableStudentsInactive');
    Route::Post('/admin/studentSetInactive/{id}', 'AdminController@setInactive');
    Route::Post('/admin/studentSetActive/{id}', 'AdminController@setActive');
    Route::Post('/admin/student/deleteStudent/{id}', 'AdminController@deleteStudent');

    /**
     * Media 
     */
    Route::get('/admin/media/overview', 'MediaController@loadPage');
    Route::get('/admin/api/media/getAll/', 'MediaController@getAllImages');
    Route::get('/admin/api/media/getAllImagesWithId/{id}', 'MediaController@getAllImagesWithId');
    Route::get('/admin/api/media/getExtentions/', 'MediaController@getAllExtentions');
    Route::get('/admin/media/loadImages', 'MediaController@loadImagesForMedia');
    Route::get('/admin/media/loadImagesPopUpMedia', 'MediaController@loadImagesPopUpMedia');
    Route::get('/admin/media/loadSingleImage/{id}', 'MediaController@loadSingleImage');

    Route::get('/admin/media/overviewSingle/{id}', 'MediaController@loadPageSingle');

    Route::post('/admin/dropzone/uploadFiles/{id}', 'MediaController@uploadFiles');
    Route::post('/admin/dropzone/uploadFiles', 'MediaController@uploadFiles');
    Route::post('/admin/api/media/update/{id}/{name}/{alt}', 'MediaController@updateImageValues');
    Route::post('/admin/dropzone/delete/{name}', 'MediaController@deleteImage');
    Route::post('/admin/api/media/delete/{id}', 'MediaController@deleteHighlightedImage');
    Route::post('/admin/media/search/{name}', 'MediaController@searchImages');


    /**
     * Pricing 
     */
    Route::get('/admin/pricing/add', 'AdminController@loadAddPagePricing');
    Route::get('/admin/pricing/overview', 'AdminController@loadPricingOverview');

    Route::Post('/admin/pricing/search/{id}','AdminController@searchPricing'); 
    Route::Get('/admin/pricing/editPricing/{id}','AdminController@LoadEditPricing');
    Route::Post('/admin/pricing/edit/{id}','AdminController@editPricing');
    Route::post('/admin/pricing/add', 'AdminController@addPricing');
    Route::get('/admin/api/pricing', 'AdminController@loadTablePricing');
    Route::get('/admin/pricing/singleView/{id}', 'AdminController@loadSingleProduct');

    Route::post('/admin/uitloggen', 'AdminController@logoutAdmin');
});

/**
 * Teacher User Routes
 */
Route::get('/teacher', 'TeacherController@loadLoginPage');
Route::get('/teacher/reset-password', 'TeacherController@loadResetPasswordPage');

Route::post('/teacher/login', 'TeacherController@loginTeacher');
Route::post('/teacher/password/email', ['as' => 'auth.password.email', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
Route::post('/teacher/password/reset', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@reset']);

Route::group(['middleware' => ['teacher']], function () {
    Route::get('/teacher/dashboard', 'TeacherController@loadOverviewPage');

    Route::get('/teacher/agenda/getNextLesson', 'TeacherController@getNextLesson');


    Route::get('/teacher/material', 'teacherMediaController@loadPage');
    Route::get('/teacher/naw', 'TeacherController@loadNaw');
    Route::Post('/teacher/edit/{id}','TeacherController@editTeacher');




    Route::get('/teacher/students', 'TeacherController@loadStudentsTable');
    Route::get('/teacher/studentMaterials/{stID}/{teachID}', 'TeacherController@loadStudentMaterials');
    Route::get('/teacher/mediaStudentsUpdate','TeacherController@updateStudentMedia');

    Route::post('/teacher/uitloggen', 'TeacherController@logoutTeacher');


    /**
     * Media Library
     */
    Route::get('/teacher/media/overview', 'MediaController@loadPage');
    Route::get('/teacher/api/media/getAll/', 'MediaController@getAllImages');
    Route::get('/teacher/api/media/getAllImagesWithId/{id}', 'teacherMediaController@getAllImagesWithId');
    Route::get('/teacher/api/media/getExtentions/', 'MediaController@getAllExtentions');
    Route::get('/teacher/media/loadImages', 'MediaController@loadImagesForMedia');
    Route::get('/teacher/media/loadImagesPopUpMedia', 'MediaController@loadImagesPopUpMedia');
    Route::get('/teacher/media/loadSingleImage/{id}', 'MediaController@loadSingleImage');

    Route::get('/teacher/media/overviewSingle/{id}', 'MediaController@loadPageSingle');
    Route::get('/teacher/media/overviewAllMedia', 'MediaController@loadPageAllMedia');

    Route::post('/teacher/dropzone/uploadFiles/{id}', 'MediaController@uploadFiles');
     Route::post('/teacher/dropzone/uploadFiles', 'MediaController@uploadFiles');
    Route::post('/teacher/api/media/update/{id}/{name}/{alt}', 'MediaController@updateImageValues');
    Route::post('/teacher/dropzone/delete/{name}', 'MediaController@deleteImage');
    Route::post('/teacher/api/media/delete/{id}', 'MediaController@deleteHighlightedImage');
    Route::post('/teacher/media/search/{name}', 'MediaController@searchImages');
});

/**
 * Auth Routes
 */
Route::get('/password/reset/{token?}', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@showResetForm']);
Route::post('/password/email', ['as' => 'auth.password.email', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
Route::post('/password/reset', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@reset']);
Route::post('/user/logout', 'UserController@logoutUser');