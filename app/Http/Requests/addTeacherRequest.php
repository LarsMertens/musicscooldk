<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class addTeacherRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           
            'email' => 'required|unique:user,email|email',
            'password' => 'required|min:3|max:200',
            'password_confirm' => 'required|min:3|max:200|same:password'
        ];
    }
}
