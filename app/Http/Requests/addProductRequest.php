<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class addProductRequest  extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
            'Name' => 'required|string|max:255|unique:product',
            'Price' => 'required|numeric',
            'singleImage' => 'required',
            'Description' => 'required|string|max:255'
            
        ];
    }
}
