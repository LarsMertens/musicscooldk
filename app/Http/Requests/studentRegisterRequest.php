<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class studentRegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gender' => 'required|min:4|string',
            'firstname' => 'required|min:1|max:50|string',
            'insertion' => 'min:2|max:10|string',
            'lastname' => 'required|min:2|max:50|string',
            'town' => 'required|min:2|max:100|string',
            'address' => 'required|min:2|max:100|string',
            'house_number' => 'required|min:1|max:16|string',
            'zipcode' => 'required|min:6|max:7|string',
            'phonenumber' => 'required|min:8|max:16|string',
            'email' => 'required|unique:user,email|email',
            'password' => 'required|min:3|max:200',
            'password_confirmation' => 'required|min:3|max:200|same:password'
        ];
    }
}
