<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduledTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduled', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_teacher_id')->unsigned();
            $table->foreign('student_teacher_id')->references('id')->on('student_teacher')->onDelete('cascade');
            $table->enum('type', array('unavailable','lesson'));
            $table->string('lesson_type');
            $table->date('date');
            $table->dateTime('starttime');
            $table->dateTime('endtime');
            $table->integer('duration');
            $table->string('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scheduled');
    }
}
