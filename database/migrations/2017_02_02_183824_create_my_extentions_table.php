
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyExtentionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
         Schema::create('extension', function (Blueprint $table) {
            $table->increments('id');
            $table->string('extension',255);
            $table->string('path', 255);
            $table->enum('type', array('File','Image'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('extension');
    }
}