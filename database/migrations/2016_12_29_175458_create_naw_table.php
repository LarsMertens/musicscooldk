<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNawTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('naw', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
            $table->enum('gender', array('Mr.', 'Mrs.'));
            $table->string('firstname');
            $table->string('insertion');
            $table->string('lastname');
            $table->string('address');
            $table->string('house_number',10);
            $table->string('town');
            $table->string('zipcode', 20);
            $table->string('phonenumber', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('naw');
    }
}
