<?php


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uploadedBy')->unsigned();
            $table->foreign('uploadedBy')->references('id')->on('user')->onDelete('cascade');
            $table->string('name',255);
            $table->text('alt');
            $table->string('extension',255);
            $table->enum('type', array('File','Image'));
            $table->string('path', 255);
            $table->string('altPath', 255);
            $table->string('alternativePath', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('media');
    }
}
