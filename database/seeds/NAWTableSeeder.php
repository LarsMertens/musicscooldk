<?php

use Illuminate\Database\Seeder;

class NAWTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('naw')->insert([
            'user_id' => 10,
            'gender' => 'Mr.',
            'firstname' => 'Pietje',
            'insertion' => '',
            'lastname' => 'Test',
            'address' => 'De straat van pietjetest',
            'house_number' => '12',
            'town' => 'De Pietjetest Plaats',
            'zipcode' => 'PI 2343',
            'phonenumber' => '06-12345678',
        ]);
        DB::table('naw')->insert([
            'user_id' => 11,
            'gender' => 'Mr.',
            'firstname' => 'Pietje',
            'insertion' => '',
            'lastname' => 'Test 2',
            'address' => 'De straat van pietjetest 2',
            'house_number' => '12',
            'town' => 'De Pietjetest 2 Plaats',
            'zipcode' => 'PI 2344',
            'phonenumber' => '06-12345678',
        ]);
        DB::table('naw')->insert([
            'user_id' => 12,
            'gender' => 'Mr.',
            'firstname' => 'Pietje',
            'insertion' => '',
            'lastname' => 'Test 3',
            'address' => 'De straat van pietjetest 3',
            'house_number' => '12',
            'town' => 'De Pietjetest 3 Plaats',
            'zipcode' => 'PI 2345',
            'phonenumber' => '06-12345678',
        ]);
        DB::table('naw')->insert([
            'user_id' => 13,
            'gender' => 'Mr.',
            'firstname' => 'Docent',
            'insertion' => '',
            'lastname' => 'Achternaam',
            'address' => 'Docentlaan',
            'house_number' => '5',
            'town' => 'Docentenplaats',
            'zipcode' => 'DO 1234',
            'phonenumber' => '06-12345678',
        ]);
        DB::table('naw')->insert([
            'user_id' => 14,
            'gender' => 'Mr.',
            'firstname' => 'Roan',
            'insertion' => '',
            'lastname' => 'Segers',
            'address' => 'Ergens in Roskilde',
            'house_number' => '123',
            'town' => 'Roskilde',
            'zipcode' => 'RO 1234',
            'phonenumber' => '06-12345678',
        ]);
    }
}
