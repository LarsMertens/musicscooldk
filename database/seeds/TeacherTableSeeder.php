<?php

use Illuminate\Database\Seeder;

class TeacherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teacher')->insert([
            'user_id' => '13',
            'name' => 'Docent',
        ]);
        DB::table('teacher')->insert([
            'user_id' => '14',
            'name' => 'Roan Segers',
        ]);
    }
}
