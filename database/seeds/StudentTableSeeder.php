<?php

use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('student')->insert([
            'user_id' => '10',
            'lessons_followed' => '5',
            'favorite_genres' => 'Rock, Metal',
            'member_since' => \Carbon\Carbon::create('2017','01', '01'),
            'subscription' => 'Local Hero',
            'lesson_items' => '20'
        ]);
        DB::table('student')->insert([
            'user_id' => '11',
            'lessons_followed' => '12',
            'favorite_genres' => 'Country',
            'member_since' => \Carbon\Carbon::create('2017','03', '03'),
            'subscription' => 'Rock Stars',
            'lesson_items' => '10'
        ]);
        DB::table('student')->insert([
            'user_id' => '12',
            'lessons_followed' => '4',
            'favorite_genres' => 'Soul, Jazz',
            'member_since' => \Carbon\Carbon::create('2017','02', '02'),
            'subscription' => 'Music Legends',
            'lesson_items' => '5'
        ]);
    }
}
