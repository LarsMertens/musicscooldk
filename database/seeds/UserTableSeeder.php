<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'name' => 'Pietje Test',
            'email' => 'pietjetest@test.nl',
            'password' => bcrypt('test'),
            'type' => 'student',
        ]);
        DB::table('user')->insert([
            'name' => 'Pietje Test 2',
            'email' => 'pietjetest2@test.nl',
            'password' => bcrypt('test'),
            'type' => 'student',
        ]);
        DB::table('user')->insert([
            'name' => 'Pietje Test 3',
            'email' => 'pietjetest3@test.nl',
            'password' => bcrypt('test'),
            'type' => 'student',
        ]);
        DB::table('user')->insert([
            'name' => 'Docent',
            'email' => 'docent@docent.nl',
            'password' => bcrypt('test'),
            'type' => 'teacher',
        ]);
        DB::table('user')->insert([
            'name' => 'Roan Segers',
            'email' => 'roansegers@test.nl',
            'password' => bcrypt('test'),
            'type' => 'teacher',
        ]);
        DB::table('user')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.nl',
            'password' => bcrypt('test'),
            'type' => 'admin',
        ]);
        DB::table('user')->insert([
            'name' => 'Admin Developer',
            'email' => 'admindeveloper@admin.nl',
            'password' => bcrypt('test'),
            'type' => 'admin',
        ]);
    }
}
