<?php

use Illuminate\Database\Seeder;

class MaterialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('material')->insert([
            'student_id' => 2,
            'name' => 'Metallica - One',
            'path' => '../path_to_file',
            'path' => '../path_to_file',
        ]);
        DB::table('material')->insert([
            'student_id' => 2,
            'name' => 'Indiana Jones - Jonge',
            'path' => '../path_to_file',
        ]);
        DB::table('material')->insert([
            'student_id' => 2,
            'name' => 'Thunderstruck - AC/DC',
            'path' => '../path_to_file',
        ]);
        DB::table('material')->insert([
            'student_id' => 2,
            'name' => 'What is this file? Its Magic!',
            'path' => '../path_to_file',
        ]);
    }
}
