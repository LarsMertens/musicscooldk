<?php

use Illuminate\Database\Seeder;

class ScheduledTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('scheduled')->insert([
            'student_teacher_id' => 6,
            'type' => 'lesson',
            'lesson_type' => 'drumles',
            'date' => \Carbon\Carbon::create('2017','03','08'),
            'starttime' => \Carbon\Carbon::create('2017','03','08','10','00','00'),
            'endtime' => \Carbon\Carbon::create('2017','03','08','10','30','00'),
            'duration' => '30',
            'message' => 'les',
        ]);
        DB::table('scheduled')->insert([
            'student_teacher_id' => 6,
            'type' => 'lesson',
            'lesson_type' => 'drumles',
            'date' => \Carbon\Carbon::create('2017','03','08'),
            'starttime' => \Carbon\Carbon::create('2017','03','08','11','00','00'),
            'endtime' => \Carbon\Carbon::create('2017','03','08','11','45','00'),
            'duration' => '45',
            'message' => 'les',
        ]);
        DB::table('scheduled')->insert([
            'student_teacher_id' => 6,
            'type' => 'lesson',
            'lesson_type' => 'drumles',
            'date' => \Carbon\Carbon::create('2017','03','08'),
            'starttime' => \Carbon\Carbon::create('2017','03','08','11','45','00'),
            'endtime' => \Carbon\Carbon::create('2017','03','08','12','30','00'),
            'duration' => '30',
            'message' => 'les',
        ]);
        DB::table('scheduled')->insert([
            'student_teacher_id' => 6,
            'type' => 'unavailable',
            'lesson_type' => 'pauze',
            'date' => \Carbon\Carbon::create('2017','03','08'),
            'starttime' => \Carbon\Carbon::create('2017','03','08','14','00','00'),
            'endtime' => \Carbon\Carbon::create('2017','03','08','14','30','00'),
            'duration' => '30',
            'message' => 'pauze',
        ]);
        DB::table('scheduled')->insert([
            'student_teacher_id' => 6,
            'type' => 'unavailable',
            'lesson_type' => 'none',
            'date' => \Carbon\Carbon::create('2017','03','09'),
            'starttime' => \Carbon\Carbon::create('2017','03','09','10','00','00'),
            'endtime' => \Carbon\Carbon::create('2017','03','09','21','30','00'),
            'duration' => '690',
            'message' => 'vrije dag',
        ]);
        DB::table('scheduled')->insert([
            'student_teacher_id' => 6,
            'type' => 'unavailable',
            'lesson_type' => 'none',
            'date' => \Carbon\Carbon::create('2017','03','15'),
            'starttime' => \Carbon\Carbon::create('2017','03','15','10','00','00'),
            'endtime' => \Carbon\Carbon::create('2017','03','15','21','30','00'),
            'duration' => '690',
            'message' => 'vrije dag',
        ]);
    }
}
