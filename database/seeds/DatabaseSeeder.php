<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(StudentTableSeeder::class);
        $this->call(TeacherTableSeeder::class);
        $this->call(StudentTeacherTableSeeder::class);
        $this->call(NAWTableSeeder::class);
        $this->call(ScheduledTableSeeder::class);
        $this->call(MaterialTableSeeder::class);
    }
}
