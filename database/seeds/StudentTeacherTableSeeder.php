<?php

use Illuminate\Database\Seeder;

class StudentTeacherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('student_teacher')->insert([
            'student_id' => '7',
            'teacher_id' => '4',
        ]);
        DB::table('student_teacher')->insert([
            'student_id' => '7',
            'teacher_id' => '5',
        ]);
        DB::table('student_teacher')->insert([
            'student_id' => '8',
            'teacher_id' => '4',
        ]);
        DB::table('student_teacher')->insert([
            'student_id' => '9',
            'teacher_id' => '5',
        ]);
    }
}
