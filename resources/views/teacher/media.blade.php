@extends('teacher_app')

@include('teacher.nav')

@section('header')
    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        $(document).ready(function(){

            $("#search-box").keyup(function(event){
                if(event.keyCode == 13){
                    $("#search-button").click();
                }
            });

            var baseUrl = "{{ url('/') }}";
            var token = "{{ Session::getToken() }}";
            var id =" {{ Auth::user()->id }}";
            angular.element('#teacherMediaController').scope().getAllImagesWithId(id);

            $("#my-awesome-dropzone").dropzone({
                paramName: 'file',
                url: baseUrl+"/teacher/dropzone/uploadFiles/" + id,
                params: {
                    _token: token
                },
                dictDefaultMessage: "Drop or click to upload images",
                clickable: true,
                maxFilesize: 2,
                addRemoveLinks: true,
                removedfile: function(file) {
                    var name = file.name;
                    name = name.substr(0, name.indexOf('.'));
                    angular.element('#teacherMediaController').scope().deleteImage(name);
                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                },
                queuecomplete: function() {
                    angular.element('#teacherMediaController').scope().getAllImagesWithId(id);
                }
            });
        });
    
    </script>

    <div id="teacherMediaController" ng-controller="teacherMediaController">
    
@endsection



@section('content')

    <div id="dashboard" class="dashboard-wrapper col-xs-12 no-padding">

        <div class="dashboard-content">
            <div class="container">

    <div class="col-xs-8 right-content no-padding">

    <div class="clearfix"></div>
    <div class="gap"></div>

    <div class="col-xs-12 default-wrapper">
        <form id="my-awesome-dropzone" action="/target" class="dropzone"></form>
    </div>

    <div class="clearfix"></div>
    <div class="gap"></div>

    <div class="col-xs-4 col-xs-push-8 no-padding search-wrapper">
        <div class="input-group stylish-input-group">
            <input id="search-box" ng-model="search_text" type="text" class="form-control"  placeholder="Search by Image Name..." >
            <span class="input-group-addon">
                <button id="search-button" type="submit" ng-click="searchItems(search_text)">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </span>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="gap"></div>

    <div id="media-gallery" class="col-xs-12 default-wrapper">
        <div class="col-md-5ths" ng-repeat="image in data" >

            <figure ng-if="image.type =='Image'" class="item col-xs-12 no-padding">
                <div style="background-image: url('<% image.path %>');"
                     ng-click="loadSingleImage(image.id)"
                     class="media-gallery-image media-gallery-image-popup"
                     id="<% image.id %>"
                     data-toggle="modal"
                     data-target="#myModalHighlight">
                </div>
                <figcaption class="my-caption"><%image.name%></figcaption>
            </figure>

            {{--  <div ng-if="image.type == 'File'">
                  <img data-toggle="modal" data-target="#myModal"  class="media-gallery-image" ng-src="<%image.altPath%>" alt="<% image.alt %>"/>
                   </div> --}}
            <div ng-if="image.type =='File'">
                <figure class="item">
                    <img ng-click="loadSingleImage(image.id)" id="<% image.id %>" data-toggle="modal" data-target="#myModalHighlight" class="media-gallery-image media-gallery-image-popup"
                         ng-src="<% image.altPath %>" alt="<% image.alt %>" />
                    <figcaption class="caption"><%image.name%></figcaption>
                </figure>
            </div>
        </div>
    </div>  
</div>
</div>  
</div>


@endsection

@section('modal')
    <div class="modal custom-modal fade" id="myModalHighlight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog col-xs-10 col-xs-push-1" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Afbeelding</h4>
                </div>

                <div class="clearfix"></div>
                <div class="gap"></div>

                <div class="modal-body">
                    <img id="the-highlighted-image" class="col-xs-9 no-padding-right media-highlighted-image"
                         src=""
                         alt=""/>
                    <div class="col-xs-3 no-padding media-right-bar">
                        <div class="col-xs-12">
                            <h6>Afbeelding Informatie</h6>

                            <div ng-if="fail || success">
                                <div class="clearfix"></div>
                                <div class="gap"></div>
                            </div>

                            <div ng-if="fail" class="col-xs-12 alert-danger cstm-alert">
                                Got an error in the process.
                            </div>
                            <div ng-if="success" class="col-xs-12 alert-success cstm-alert">
                                Succesvol gewijzigd.
                            </div>

                            <div class="clearfix"></div>
                            <div class="gap"></div>

                            {{ Form::open() }}

                            <label for="id">ID</label>
                            {{ Form::text("id", null, array("ng-model" => "id", "class" => "form-control col-xs-12", "readonly")) }}

                            <div class="clearfix"></div>
                            <div class="gap"></div>

                            <label for="name">Naam</label>
                            {{ Form::text("name", null, array("ng-model" => "name", "class" => "form-control col-xs-12")) }}

                            <div class="clearfix"></div>
                            <div class="gap"></div>

                            <label for="alt">Alt text</label>
                            {{ Form::text("alt", null, array("ng-model" => "alt", "class" => "form-control col-xs-12")) }}

                            <div class="clearfix"></div>
                            <div class="gap"></div>

                            <label for="path">Url</label>
                            {{ Form::text("path", null, array("ng-model" => "path", "readonly", "class" => "form-control col-xs-12")) }}

                            <div class="clearfix"></div>
                            <div class="gap"></div>

                            <div class="col-xs-12 no-padding">
                                <button ng-click="changeImageValues(id,name,alt,path)" class="col-xs-12 cta-button" type="button">
                                    Afbeelding Wijzigen
                                </button>
                            </div>

                            <div class="col-xs-12 no-padding btn-wrap-media">
                                <div class="col-xs-6 no-padding">
                                    <button ng-click="getImageURL(path)" class="col-xs-10 col-xs-push-1 btn btn-icon-only btn-labeled cta-button" type="button">
                                        <i class='glyphicon glyphicon-paperclip'></i>
                                    </button>
                                </div>
                                <div class="col-xs-6 no-padding">
                                    <button ng-click="deleteImageInMediaPopUp(id)" class="col-xs-10 col-xs-push-1 btn btn-icon-only btn-labeled cta-button" type="button">
                                        <i class='glyphicon glyphicon-remove'></i>
                                    </button>
                                </div>
                            </div>

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="gap"></div>

                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    </div>
@endsection