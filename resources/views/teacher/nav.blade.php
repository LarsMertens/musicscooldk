<div class="nav-wrapper">

    <div class="col-xs-12 the-artist" style="background-image: url('/img/pietjetest.jpg')"></div>
    <div class="artist-name col-xs-12">
        {{ Auth::user()->name }}
    </div>

    <div class="col-xs-12 nav-menu-artist no-padding">
        <a @if(Route::getCurrentRoute()->uri() == 'teacher/dashboard') class="active" @endif href="{{ url('/teacher/dashboard') }}">
            <li>
                <span class="glyphicon glyphicon-dashboard"></span>
                <span class="nav-item">Dashboard</span>
            </li>
        </a>

        <a @if(Route::getCurrentRoute()->uri() == 'teacher/material') class="active" @endif class="" href="{{ url('/teacher/material') }}">
            <li>
                <span class="glyphicon glyphicon-picture"></span>
                <span class="nav-item">Mijn Media</span>
            </li>
        </a>

        <a @if(Route::getCurrentRoute()->uri() == 'teacher/studentMaterials') class="active" @endif class="" href="{{ url('/teacher/studentMaterials') }}">
            <li>
                <span class="glyphicon glyphicon-music"></span>
                <span class="nav-item">Student Materials</span>
            </li>
        </a>

        <a @if(Route::getCurrentRoute()->uri() == 'teacher/naw') class="active" @endif class="" href="{{ url('/teacher/naw') }}">
            <li>
                <span class="glyphicon glyphicon-user"></span>
                <span class="nav-item">Account gegevens</span>
            </li>
        </a>

        {!! Form::open(array('url' => 'user/logout'))  !!}
            {!! Form::button('<li class="logout-wrapper">
                                <span class="fa fa-sign-out"></span>
                                <span class="nav-item">Uitloggen</span>
                              </li>', array('type' => 'submit', 'class' => 'remove-default-btn logout-btn'))
            !!}
        {!! Form::close()  !!}

    </div>

</div>