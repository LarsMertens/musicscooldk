@extends('teacher_app')

@include('teacher.nav')

@section('content')

    <div id="dashboard" class="dashboard-wrapper col-xs-12">

        <div class="dashboard-content">
            <div class="col-xs-8 right-content no-padding" ng-controller="timescheduleController">

                <div class="col-xs-12 no-padding next-lessons-box">
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 time-until-next-lesson-box">
                            Volgende Lektioner over
                        </div>
                    </div>

                    <div ng-if="!noLessonPlanned" class="col-xs-12 shadow-for-time no-padding">
                        <div class="col-xs-3 time-until-next-lesson-time">
                            <div class="responsive-circle time col-xs-10 col-xs-push-1">
                                <% daysCountdown %>
                            </div>
                            <div class="time-text col-xs-12">dagen</div>
                        </div>
                        <div class="col-xs-3 time-until-next-lesson-time">
                            <div class="responsive-circle time col-xs-10 col-xs-push-1">
                                <% hoursCountdown %>
                            </div>
                            <div class="time-text col-xs-12">uur</div>
                        </div>
                        <div class="col-xs-3 time-until-next-lesson-time">
                            <div class="responsive-circle time col-xs-10 col-xs-push-1">
                                <% minutesCountdown %>
                            </div>
                            <div class="time-text col-xs-12">minuten</div>
                        </div>
                        <div class="col-xs-3 time-until-next-lesson-time">
                            <div class="responsive-circle time col-xs-10 col-xs-push-1">
                                <% secondsCountdown %>
                            </div>
                            <div class="time-text col-xs-12">seconden</div>
                        </div>
                    </div>

                    <div ng-if="noLessonPlanned" class="col-xs-12 col-xs-push-1 shadow-for-time no-padding">
                        <div class="col-xs-12 time-until-next-lesson-time">
                            <div class="time-text col-xs-12">Nog geen les ingeplanned</div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="clear"></div>
            <div class="gap"></div>

            <div class="col-xs-12 no-padding statistics-box next-lessons-box">


                <div class="statistics-row col-xs-12 no-padding">
                    <div class="col-xs-4 no-padding-left">
                        <div class="col-xs-12 no-padding statistics-wrapper">
                            <div class="col-xs-12 statistics favorite-genres">
                                Lid Sinds
                            </div>
                            <div class="col-xs-12 favorite-genres-text time-until-next-lesson-time">
                                {{ Carbon\Carbon::parse($data->created_at)->format('d.m.Y') }}
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-4 no-padding-right">
                        <div class="statistics-wrapper col-xs-12 no-padding">
                            <div class="col-xs-12 statistics lessons-followed">
                                Materiaal uploaded
                            </div>
                            <div class="col-xs-12 time-until-next-lesson-time">
                                <div class="responsive-circle-5 time col-xs-5 col-xs-push-3">
                                    {{ $count }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div><!-- end dashboard-wrapper -->

@endsection