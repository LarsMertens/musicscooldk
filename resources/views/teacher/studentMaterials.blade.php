@extends('teacher_app')

@include('teacher.nav')

@section('content')
  <script src="{{ url('js/sortable.js') }}"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <div id="dashboard" class="dashboard-wrapper col-xs-12 no-padding">

        <div class="dashboard-content">
            <div class="container">


                <div class="col-xs-8 right-content no-padding">
                   <div class="col-xs-12 no-padding material-box">

                      <div class="col-xs-12 no-padding">
                            <div class="col-xs-12 time-until-next-lesson-box mijn-materiaal">
                                Lektioner Materiaal Overzicht
                            </div>
                      </div>

                      <div class="clear"></div>
                      <div class="gap"></div>

                      <div class="col-xs-12" style="background-color:blue; height:auto; min-height:150px;">

                        <ul id="gallery" class="gallery ui-helper-reset ui-helper-clearfix">
                        @foreach ($media as $m)
                          <li id="{{$m->id}}" name="{{$m->name}}" class="ui-widget-content ui-corner-tr">
                            <p class="ui-widget-header">{{$m->name}}</p>
                            <img src="{{$m->path}}" alt="The peaks of High Tatras" width="96" height="72">
                            <a href="{{$m->path}}" title="View larger image" class="ui-icon ui-icon-zoomin">View larger</a>
                            <a href="" title="Delete this image" class="ui-icon ui-icon-circle-arrow-s">Delete image</a>
                          </li>
                        @endforeach
                        </ul>

                      </div>

                      <div class="clear"></div>
                      <div class="gap"></div>

                      <div class="col-xs-12" style="background-color:red; height:auto; min-height:150px;">

                        <ul id="trash" class="trash ui-helper-reset ui-helper-clearfix">
                        @foreach ($studentMaterial as $st)
                          <li id="{{$st->media_id}}" class="ui-widget-content ui-corner-tr">
                            <p class="ui-widget-header">{{$st->id}}</p>
                            <img src="{{$st->path}}" alt="The peaks of High Tatras" width="96" height="72">
                            <a href="{{$st->path}}" title="View larger image" class="ui-icon ui-icon-zoomin">View larger</a>
                            <a href="" title="Delete this image" class="ui-icon ui-icon-circle-arrow-n">Delete image</a>
                          </li>
                        @endforeach
                        </ul>

                      </div>
                   

                      <div class="clear"></div>
                      <div class="gap"></div>

                      <button onclick="save()">Click me</button>

                      <div class="clear"></div>
                      <div class="gap"></div>


                   </div><!-- end material-box -->





                </div><!-- end reight-content -->
            </div><!-- end container -->
        </div><!-- end dashboard-content -->
    </div><!-- end dashboard-wrapper -->

@endsection



{{-- 
  Debug

@if (count($media) === 1)
    I have one record!
@elseif (count($media) > 1)
    I have multiple records!
@else
    I don't have any records!
@endif --}}


{{--                     <div class="col-xs-6 no-padding" style="background-color:red; height:80px;">
                      <ul id="student" class="col-xs-12 no-padding" style="background-color:blue; height:auto; min-height:150px;" >
                          @if(count($studentMaterial) > 0)
                            @foreach ($studentMaterial as $st)
                            <li> {{$st}} </li>
                            @endforeach
                          @else
                          Add materials to this student
                          <br><br>
                          @endif
                      </ul>
                    </div>

                   <ul id="origin" class="col-xs-6" style="background-color:red; min-height:150px;">
                         @foreach ($media as $m)
                          <li class="teacher col-xs-4" getID="{{$m->id}}" path="{{$m->path}}" uploadedBy="{{$m->uploadedBy}}" style="height:150px;width:150px; background-image:url('{{$m->path}}'); background-position:center; margin-bottom:10px; margin-left:10px;">    <a href="" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a></li>
                          
                        @endforeach
                     
                    </ul>
                  </div> --}}