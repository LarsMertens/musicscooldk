<div class="col-xs-3 no-padding nav-wrapper">

    <div class="col-xs-12 the-artist" style="background-image: url('../img/pietjetest.jpg')"></div>
    <div class="artist-name col-xs-12">
        {{ Auth::user()->name }}
    </div>

    <div class="col-xs-12 nav-menu-artist no-padding">
        <a @if(Route::getCurrentRoute()->uri() == 'student/dashboard') class="active" @endif href="{{ url('student/dashboard') }}">
            <li>
                <span class="glyphicon glyphicon-dashboard"></span>
                <span class="nav-item">Dashboard</span>
            </li>
        </a>

        <a @if(Route::getCurrentRoute()->uri() == 'student/agenda') class="active" @endif href="{{ url('student/agenda') }}">
            <li>
                <span class="glyphicon glyphicon-calendar"></span>
                <span class="nav-item">Agenda</span>
            </li>
        </a>

        <a @if(Route::getCurrentRoute()->uri() == 'student/mijn-lessen') class="active" @endif href="{{ url('student/mijn-lessen') }}">
            <li>
                <span class="glyphicon glyphicon-user"></span>
                <span class="nav-item">Mijn Lektioner</span>
            </li>
        </a>

        <a @if(Route::getCurrentRoute()->uri() == 'student/material') class="active" @endif href="{{ url('student/material') }}">
            <li>
                <span class="glyphicon glyphicon-list"></span>
                <span class="nav-item">Mijn materiaal</span>
            </li>
        </a>

        <a @if(Route::getCurrentRoute()->uri() == 'student/naw') class="active" @endif href="{{ url('student/naw') }}">
            <li>
                <span class="glyphicon glyphicon-list-alt"></span>
                <span class="nav-item">Account gegevens</span>
            </li>
        </a>

        <a @if(Route::getCurrentRoute()->uri() == 'student/mijn-betalingen') class="active" @endif href="{{ url('student/mijn-betalingen') }}">
            <li>
                <span class="glyphicon glyphicon-list-alt"></span>
                <span class="nav-item">Mijn betalingen</span>
            </li>
        </a>

        <a @if(Route::getCurrentRoute()->uri() == 'student/shop') class="active" @endif href="{{ url('student/shop') }}">
            <li>
                <span class="fa fa-money"></span>
                <span class="nav-item">Shop</span>
            </li>
        </a>

        {!! Form::open(array('url' => 'user/logout'))  !!}
            {!! Form::button('<li>
                                <span class="fa fa-sign-out"></span>
                                <span class="nav-item">Uitloggen</span>
                              </li>', array('type' => 'submit', 'class' => 'remove-default-btn logout-btn'))
            !!}
        {!! Form::close()  !!}

    </div>

</div>