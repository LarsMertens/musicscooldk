@extends('company_app')

@section('title') Login Company @endsection
@section('description') Dit is de login voor de Company. @endsection
@section('login_page')login-page @endsection

@section('content')
    <div class="col-xs-4 the-login-wrapper">

        @if ($errors->has())
            <div class="error-box col-xs-12 alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }}<br>
                @endforeach
            </div>
            <div class="clear"></div>
        @endif

        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Herstel Wachtwoord</h3>
            </div>
            <div class="panel-body">
                {{ Form::open(["url" => "/password/reset"]) }}
                <fieldset>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-xs-12 no-padding">

                            <div class="form-group">
                                {!! Form::text('email',
                                            null,
                                            ['placeholder' => 'Vul hier uw E-mailadres in...',
                                              'id' => 'e-mail',
                                              'class' => 'form-control']
                                             )
                                !!}
                            </div>

                            <div class="form-group">
                                {!! Form::password('password',
                                            ['placeholder' => 'Vul hier uw nieuw wachtwoord in...',
                                              'id' => 'password',
                                              'class' => 'form-control']
                                             )
                                !!}
                            </div>

                            <div class="form-group">
                                {!! Form::password('password_confirmation',
                                            ['placeholder' => 'Vul hier uw wachtwoord nogmaals in...',
                                              'id' => 'password_confirmation',
                                              'class' => 'form-control']
                                             )
                                !!}
                            </div>

                        </div>
                    </div>

                    {!! Form::button('Nieuw wachtwoord instellen', ['type' => 'submit', 'class' => 'col-xs-12 cta-button']) !!}

                </fieldset>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
