@extends('admin_app')

@section('title') Reset Password Page @endsection
@section('description') Dit is een pagina om jouw wachtwoord te herstellen @endsection
@section('login_page')login-page @endsection

@section('content')
    <div class="col-xs-4 the-login-wrapper">

        @if ($errors->has())
            <div class="error-box col-xs-12 alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }}<br>
                @endforeach
            </div>
            <div class="clear"></div>
        @endif

        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Vraag nieuw wachtwoord aan</h3>
            </div>
            <div class="panel-body">
                {{ Form::open(["url" => "/password/email"]) }}
                <fieldset>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-xs-12 no-padding">

                            <div class="form-group">
                                {!! Form::text('email',
                                            null,
                                            ['placeholder' => 'Vul hier uw E-mailadres in...',
                                              'id' => 'e-mail',
                                              'class' => 'form-control']
                                             )
                                !!}
                            </div>

                            <div class="gap-10"></div>

                        </div>
                    </div>

                    {!! Form::button('Nieuw wachtwoord opsturen', ['type' => 'submit', 'class' => 'col-xs-12 cta-button']) !!}

                </fieldset>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    @endsection



