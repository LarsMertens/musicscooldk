@extends('admin_app')

@section('title') Admin Login @endsection
@section('description') This is the login page for the admin. @endsection

@section('content')
    <div class="background-login-wrapper">
        <div class="col-xs-4 the-login-wrapper">

            @if ($errors->has())
                <div class="error-box col-xs-12 alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
                <div class="clear"></div>
            @endif

            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Admin Login</h3>
                </div>
                <div class="panel-body">
                    {{ Form::open(["url" => "/admin/login"]) }}
                    <fieldset>
                        <div class="form-group">
                            {!! Form::text('email',
                                        null,
                                        ['placeholder' => 'Vul hier uw E-mailadres in...',
                                          'id' => 'e-mail',
                                          'class' => 'form-control']
                                         )
                            !!}
                        </div>
                        <div class="form-group">
                            {!! Form::password('password',
                                        ['placeholder' => 'Vul hier uw Wachtwoord in...',
                                          'id' => 'e-mail',
                                          'class' => 'form-control']
                                         )
                            !!}
                        </div>

                        <div class="col-xs-12 no-padding">
                            <div class="col-xs-6 no-padding">
                                <div class="checkbox">
                                    <label>
                                        {{ Form::checkbox('remember_token', (1 or true), null) }}
                                        Remember Me
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <a id="reset-password" href="{{ url('/admin/reset-password') }}">Wachtwoord vergeten?</a>
                            </div>
                        </div>

                        {!! Form::button('Inloggen', ['type' => 'submit', 'class' => 'col-xs-12 cta-button']) !!}

                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection


