@extends('admin_app')

@extends('admin.nav')

@section('header')
        <h1 class="col-xs-12">Dashboard</h1>
@endsection

@section('content')


        <div id="aanbod-content" class="col-xs-12">
            {{--Welkom: {{ Auth::user()->name }} <br/>--}}
            <div class="clearfix"></div>
            <div class="gap-30"></div>
            <div id="aanbod-items-wrapper">
                <a href="{{ url('/admin/teacherOverview') }}" class="col-xs-6 col-md-4 zoom">
                    <div class="col-xs-12 single-aanbod-item" style="background-image: url({{ url('img/lessons.jpg')}})"></div>
                    <h4 class="col-xs-12">Teachers</h4>
                </a>
                <a href="{{ url('/admin/student/overview') }}" class="col-xs-6 col-md-4 aanbod-item zoom">
                    <div class="col-xs-12 single-aanbod-item" style="background-image: url({{ url('img/pietjetest.jpg')}})"></div>
                    <h4 class="col-xs-12">Students</h4>
                </a>
                <a href="{{ url('/admin/media/overview') }}" class="col-xs-6 col-md-4 zoom">
                    <div class="col-xs-12 single-aanbod-item" style="background-image: url({{ url('img/mijn-lessen.jpg') }})"></div>
                    <h4 class="col-xs-12">My own Media</h4>
                </a>
                <a href="{{ url('/admin/media/overview') }}" class="col-xs-6 col-md-4 zoom">
                    <div class="col-xs-12 single-aanbod-item" style="background-image: url({{ url('img/genres.jpg') }})"></div>
                    <h4 class="col-xs-12">All Media</h4>
                </a>
                <a href="{{ url('/admin/pricing/overview') }}" class="col-xs-6 col-md-4 zoom">
                    <div class="col-xs-12 single-aanbod-item" style="background-image: url({{ url('img/gC.jpg') }})"></div>
                    <h4 class="col-xs-12">Pricing</h4>
                </a>
            </div>

        </div>

        {{ Form::open(["url" => "/admin/uitloggen"]) }}
        {!! Form::button('Uitloggen', ['type' => 'submit', 'class' => 'col-xs-3 cta-button']) !!}
        <div class="clearfix"></div>
        <div class="gap"></div>
        {{ Form::close() }}

@endsection

@section('footer')
    </div>
@endsection