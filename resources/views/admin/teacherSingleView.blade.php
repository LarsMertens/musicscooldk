@extends('admin_app')

@extends('admin.nav')

@section('header')
       <h1 class="col-xs-12">Teacher Single View</h1>
@endsection

@section('content')

     <div class="clearfix"></div>
     <div class="gap"></div>

     <a href="{{ url('/admin/teacherOverview') }}" type="button" class="cta-button-icon col-xs-push-9 col-xs-3">
                <i class="glyphicon glyphicon-arrow-left"></i>
                Back
     </a>

    <div class="clearfix"></div>
    <div class="gap"></div>

    <div class="col-xs-12 default-wrapper highlight-wrapper">
        <div class="highlight"></div>
           User ID: {{ $data->id }} 
        </div>

        <div class="clearfix"></div>
        <div class="gap"></div>

            <div class="gap"></div>

     @if(isset($naw))
            <div class="col-xs-12 ">
                <div class="gap"></div>
                <h2>Profiel:  {{ $naw->firstname }} {{ $naw->lastname }}   --   {{ $data->type }}</h2>
                <div class="gap"></div>
                <div class="teacher-info-wrapper col-xs-6 no-padding">
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding">{{ $naw->firstname }}</div>
                    </div>
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding">{{ $naw->insertion }}</div>
                    </div>
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding">{{ $naw->lastname }}</div>
                    </div>

                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding">{{ $naw->address }}</div>
                    </div>
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding">{{ $naw->zipcode }}</div>
                    </div>
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding">{{ $naw->town }}</div>
                    </div>
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding">{{ $naw->phonenumber }}</div>
                    </div>
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding">{{ $data->email }}</div>
                    </div>
                </div>
            </div>
    @else
            
    @endif


        <div class="clearfix"></div>
        <div class="gap"></div>
</div>
@endsection