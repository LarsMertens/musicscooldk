@extends('admin_app')
@extends('admin.nav')

@section('header')
    <div ng-controller="studentController" ng-init="getPageInactive(1)">
        <h1 class="col-xs-12">Inactive Students</h1>
@endsection

@section('content')

    <div class="col-xs-12">
            <div class="clearfix"></div>
            <div class="gap"></div>

                <a href="{{ url('admin/overview') }}" type="button" class="cta-button-icon col-xs-push-2 col-xs-3">
                    <i class="fa fa-mouse-pointer"></i>
                    Terug naar dashboard
                </a>

                 <a href="{{ url('/admin/student/add') }}" type="button" class="cta-button-icon col-xs-push-4 col-xs-3">
                     <i class="fa fa-plus"></i>
                     Add a new Student
                </a>

     </div>
        <div class="col-xs-12 default-wrapper no-border-top no-padding">

        <div class="clearfix"></div>
        <div class="gap"></div>




        <script>
            $( document ).ready(function() {
                $("#search-box").keyup(function(event){
                    if(event.keyCode == 13){
                        $("#search-button").click();
                    }
                });
            });
        </script>

            @if(Session::has('validationMessage'))
                <div class="col-xs-12 default-wrapper highlight-wrapper flash-message">
                    <div class="highlight highlight-green"></div>
                    {{ Session::get('validationMessage') }}
                </div>

                <div class="clearfix"></div>
                <div class="gap"></div>
            @endif

        <div class="col-xs-4 col-xs-push-8 no-padding search-wrapper">
            <div class="input-group stylish-input-group">
                <input id="search-box" ng-model="search_text" type="text" class="form-control"  placeholder="Search by name" >
                    <span class="input-group-addon">
                        <button id="search-button" type="submit" ng-click="searchItems(search_text)">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
            </div>
        </div>

        <table class="table table-striped">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th></th>
            </tr>
            <p class="text-center" ng-show="loading"><span class="fa fa-spinner fa-pulse fa-3x fa-fw"></span>
            <tr ng-hide="loading" ng-repeat="student in data.data track by student.id">
                <td><% student.id %></td>
                <td><% student.name %></td>
                <td>
                    <a href="/admin/student/singleView/<% student.id %>" type="button" class="btn btn-icon-only btn-labeled btn-primary">
                        <i class="glyphicon glyphicon-eye-open"></i>
                    </a>
                    <a href="/admin/studentUpdate/<% student.id %>" type="button" class="btn btn-icon-only btn-labeled btn-info">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    <a ng-click="setActive(student.id, data.current_page)" class="btn btn-icon-only btn-labeled btn-success"  >
                        <i class="glyphicon glyphicon-ok"></i>
                    </a>
                    <a ng-click="deleteItem(student.id, data.current_page)" class="btn btn-icon-only btn-labeled btn-danger"  >
                        <i class="glyphicon glyphicon-remove"></i>
                    </a>
                    
                </td>
            </tr>
        </table>

        <div class="center">
            <ul class="pagination">
                <li ng-click="previousPage(data.current_page)"><a href="#">«</a></li>
                    <li ng-repeat="n in [] | range: data.last_page">
                        <a ng-if="n == data.current_page" href="#" class="active" ng-click="getPage(n)"><% n %></a>
                        <a ng-if="n != data.current_page" href="#" ng-click="getPage(n)"><% n %></a>
                    </li>
                <li ng-click="nextPage(data.current_page, data.last_page)"><a href="#">»</a></li>
            </ul>
        </div>

        <div class="total">Results: <% data.from %> - <% data.to %></div>
        <div class="total">Total Records: <% data.total %></div>
    </div>
@endsection


@section('footer')
    </div>
@endsection