<div class="col-xs-3 left-admin-bar">
    <h4>Musicscool DK</h4>
    <nav>
        <ul class="col-xs-10 col-xs-push-1 no-padding">
            <li>
                @if(Route::getCurrentRoute()->uri() == 'admin/overview')<div class="active"></div>@endif
                <a href="{{ url('/admin/overview') }}"><i class="fa fa-dashboard"></i>Dashboard</a>
            </li>
            <li>
                @if(Route::getCurrentRoute()->uri() == 'admin/teacherOverview')<div class="active"></div>@endif
                <a href="{{ url('/admin/teacherOverview') }}"><i class="fa fa-graduation-cap"></i>Teachers</a>
            </li>
            <li>
                @if(Route::getCurrentRoute()->uri() == 'admin/student/overview')<div class="active"></div>@endif
                <a href="{{ url('/admin/student/overview') }}"><i class="fa fa-user"></i>Students</a>
            </li>
            <li>
                @if(Route::getCurrentRoute()->uri() == 'admin/media/overview')<div class="active"></div>@endif
                <a href="{{ url('/admin/media/overview') }}"><i class="fa fa-image"></i>Media</a>
            </li>
            <li>
                @if(Route::getCurrentRoute()->uri() == 'admin/pricing/overview')<div class="active"></div>@endif
                <a href="{{ url('/admin/pricing/overview') }}"><i class="fa fa-money"></i>Pricing</a>
            </li>
            <li>
                @if(Route::getCurrentRoute()->uri() == 'admin/invoice')<div class="active"></div>@endif
                <a href="{{ url('/admin/invoice') }}"><i class="fa fa-barcode"></i>Factuur</a>
            </li>
        </ul>
    </nav>
</div>