@extends('admin_app')

@extends('admin.nav')

@section('header')
    <div ng-controller="invoiceController" ng-init="loadChoices({{ $data->id }})">
        <h1 class="col-xs-12">Factuur #{{ $data->id }} Wijzigen</h1>
        @endsection

        @section('content')

            <script>
                $(document).ready(function(){
                    $('.flash-message').delay(3000).slideUp(300);
                });
            </script>

            @if(Session::has('validationMessage'))
                <div class="col-xs-12 default-wrapper highlight-wrapper flash-message">
                    <div class="highlight highlight-green"></div>
                    {{ Session::get('validationMessage') }}
                </div>

                <div class="clearfix"></div>
                <div class="gap"></div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger col-xs-12">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="col-xs-12 default-wrapper highlight-wrapper">
                <div class="highlight"></div>
                Wijzig hieronder Factuur #{{ $data->id }}
                </div>


            <div class="clearfix"></div>
            <div class="gap"></div>

            <div class="col-xs-12 default-wrapper default-styles">

                {!! Form::open(['url' => '/admin/invoice/editInvoice/' . $data->id]) !!}

                <div class="form-group">
                    <div class="col-xs-4 no-padding">
                        <div class="col-xs-12">
                            <label for="name">Student ID</label>
                            {!! Form::text('student_id',
                                            $data->student->id,
                                            ['placeholder' => 'Voer factuurnaam in...',
                                              'id' => 'mystudentid',
                                              'class' => 'form-control',
                                              'readonly']
                                             )
                            !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-6 no-padding">
                        <div class="col-xs-12">
                            <table class="table table-striped">
                                <tr>
                                    <th>
                                        <label for="name">Zoek Student ID</label>
                                        {!! Form::text('student_name',
                                                        null,
                                                        ['placeholder' => 'Zoek op naam',
                                                          'id' => 'mystudentid',
                                                          'class' => 'form-control',
                                                          'ng-model' => 'query',
                                                          'ng-change' => 'searchStudentID(query)'
                                                          ]
                                                         )
                                        !!}
                                    </th>
                                    <th>Student ID</th>
                                    <th></th>
                                </tr>
                                <tr ng-repeat="student in students">
                                    <td>
                                        <% student.name %>
                                    </td>
                                    <td>
                                        <% student.student.id %>
                                    </td>
                                    <td>
                                        <a ng-click="selectStudentID(student.student.id)" type="button" class="btn btn-icon-only btn-labeled btn-primary">
                                            <i class="glyphicon glyphicon-pushpin"></i>
                                        </a>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
                <div class="gap"></div>

                <div class="form-group">
                    <div class="col-xs-4 no-padding">
                        <div class="col-xs-12">
                            <label for="name">Factuurnaam</label>
                            {!! Form::text('name',
                                            $data->name,
                                            ['placeholder' => 'Voer factuurnaam in...',
                                              'id' => 'name',
                                              'class' => 'form-control']
                                             )
                            !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-4 no-padding">
                        <div class="col-xs-12">
                            <label for="price">Factuurprijs</label>
                            {!! Form::text('price',
                                            $data->price,
                                            ['placeholder' => 'Voer factuurprijs in...',
                                              'id' => 'price',
                                              'class' => 'form-control']
                                             )
                            !!}
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="gap"></div>

                <div class="form-group">
                    <div class="col-xs-4 no-padding">
                        <div class="col-xs-12">
                            <label for="status">Factuurstatus</label>
                            {!! Form::select('status',
                                             ['PENDING' => 'PENDING',
                                              'SUCCESS' => 'SUCCESS',
                                              'FAILURE' => 'FAILURE',
                                              'EXPIRED' => 'EXPIRED',
                                              'CANCELLED' => 'CANCELLED'],
                                             $data->status,
                                             ['placeholder' => 'Kies een status...',
                                              'id' => 'status',
                                              'class' => 'form-control']
                                             )
                            !!}
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="gap"></div>

                <div class="form-group">
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12">
                            <label for="description">Omschrijving Factuur</label>
                            {!! Form::textarea('description',
                                            $data->description,
                                            ['placeholder' => 'Voer omschrijving van de factuur in...',
                                              'id' => 'description',
                                              'class' => 'form-control']
                                             )
                            !!}
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="gap"></div>

                <div class="col-xs-12">
                    <h6>Factuur regels</h6>
                </div>

                <div class="clearfix"></div>
                <div class="gap"></div>

                <div ng-repeat="choice in choices">

                    <input type="hidden" name="invoiceLineNamesIDS[]" value="<% choice.id %>" />

                    <div class="form-group">
                        <div class="col-xs-4 no-padding">
                            <div class="col-xs-12">
                                <label for="invoiceLineName">Factuurregel Naam</label>
                                <input value="<% choice.name %>" name="invoiceLineName[]" type="text" placeholder="Voer factuurregel naam in..." id="invoiceLineName" class="form-control" >
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-4 no-padding">
                            <div class="col-xs-12">
                                <label for="invoiceLineDescription">Factuurregel Omschrijving</label>
                            <textarea name="invoiceLineDescription[]"
                                      placeholder="Voer factuurregel omschrijving in..."
                                      id="invoiceLineDescription"
                                      class="form-control custom-height-omschrijving"
                                      cols="10" rows="50" ><% choice.description %></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-4 no-padding">
                            <div class="col-xs-12">
                                <label for="invoiceLinePrice">Factuurregel Prijs</label>
                                <input name="invoiceLinePrice[]"
                                       type="text"
                                       placeholder="Voer factuurregel prijs naam in..."
                                       id="invoiceLinePrice"
                                       class="form-control"
                                       value="<% choice.price %>">
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="gap"></div>
                </div>

                <div class="col-xs-12">
                    <button ng-click="removeWithRecordChoice({{ $data->id }})" type="button" class="col-xs-1 col-xs-push-10 btn-icon-only btn-remove-form">
                        <i class="glyphicon glyphicon-minus"></i>
                    </button>
                    <button ng-click="addNewChoice()" type="button" class="col-xs-1 col-xs-push-10 btn-icon-only btn-add-form">
                        <i class="glyphicon glyphicon-plus"></i>
                    </button>
                </div>

                <div class="col-xs-12">
                    {!! Form::button('Factuur Wijzigen', ['type' => 'submit', 'class' => 'col-xs-4 cta-button']) !!}
                </div>

                {!! Form::close() !!}
            </div>
    </div>
@endsection