@extends('admin_app')

@extends('admin.nav')

@section('header')
    <div ng-controller="invoiceController">
        <h1 class="col-xs-12">Factuur #{{ $data->id }}</h1>
 @endsection

@section('content')

    <div class="col-xs-12 default-wrapper highlight-wrapper">
        <div class="highlight"></div>
            Bekijk hieronder de details van Factuur #{{ $data->id }}
        </div>

        <div class="clearfix"></div>
        <div class="gap"></div>

        <div class="col-xs-12 default-wrapper no-border-top no-padding">
            <a href="{{ url('/admin/invoice/view/pdf/'.$data->id.'') }}" type="button" class="cta-button-icon col-xs-push-9 col-xs-3">
                <i class="fa fa-file-pdf-o"></i>
                Factuur in PDF
            </a>
        </div>

        <div class="clearfix"></div>
        <div class="gap"></div>

        <div class="col-xs-12 factuur-wrapper factuur-wrapper-border">
            <div class="col-xs-12 no-padding">
                <div class="logo-factuur col-xs-3"></div>
                <div class="factuur-my-company col-xs-6 col-xs-push-3">
                    <div class="col-xs-12">
                        <div class="col-xs-4 factuur-highlight">Adres:</div>
                        <div class="col-xs-8">
                            <div class="col-xs-12">Mijn bedrijfsnaam</div>
                            <div class="col-xs-12">Straatnaam + nummer</div>
                            <div class="col-xs-12">Postcode + Vestigingsplaats</div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="factuur-gap"></div>

                    <div class="col-xs-12">
                        <div class="col-xs-4 factuur-highlight">KvK nr:</div>
                        <div class="col-xs-8">
                            <div class="col-xs-12">12345678</div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-4 factuur-highlight">BTW nr:</div>
                        <div class="col-xs-8">
                            <div class="col-xs-12">NL12345678B01</div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="factuur-gap"></div>

                    <div class="col-xs-12">
                        <div class="col-xs-4 factuur-highlight">Bank:</div>
                        <div class="col-xs-8">
                            <div class="col-xs-12">Mijn banknaam</div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-4 factuur-highlight">IBAN:</div>
                        <div class="col-xs-8">
                            <div class="col-xs-12">Mijn rekeningnummer</div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-4 factuur-highlight">BIC</div>
                        <div class="col-xs-8">
                            <div class="col-xs-12">Mijn bank BIC code</div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="factuur-gap"></div>

                    <div class="col-xs-12">
                        <div class="col-xs-4 factuur-highlight">Tel:</div>
                        <div class="col-xs-8">
                            <div class="col-xs-12">06-12345678</div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-4 factuur-highlight">E-mail:</div>
                        <div class="col-xs-8">
                            <div class="col-xs-12">info@bedrijf.dk</div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-4 factuur-highlight">Website:</div>
                        <div class="col-xs-8">
                            <div class="col-xs-12">http://mijnwebsite.nl</div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="factuur-gap"></div>
            </div>

            <div class="clearfix"></div>
            <div class="factuur-gap"></div>
            <div class="factuur-border col-xs-12"></div>

            <div class="gap"></div>

            <div class="col-xs-12 factuur-info-wrapper">
                <div class="gap"></div>
                <h2>Factuur</h2>
                <div class="gap"></div>
                <div class="col-xs-6 no-padding">
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding factuur-highlight">
                            {{ $user->naw->firstname }}
                            @if($user->naw->insertion != '')
                                {{ $user->naw->insertion }}
                            @endif
                            {{ $user->naw->lastname }}
                        </div>
                    </div>
                    <div class="factuur-gap"></div>
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding factuur-highlight">{{ $user->naw->address }}</div>
                    </div>
                    <div class="factuur-gap"></div>
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding factuur-highlight">
                            {{ $user->naw->zipcode }},
                            {{ $user->naw->town }}
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 no-padding">
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-4 no-padding factuur-highlight">Factuurnummer:</div>
                        <div class="col-xs-8">{{ $data->id }}</div>
                    </div>
                    <div class="factuur-gap"></div>
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-4 no-padding factuur-highlight">Factuurdatum:</div>
                        <div class="col-xs-8">{{ Carbon\Carbon::now()->format('d-m-Y') }}</div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="gap"></div>

            <table class="table table-factuur table-striped">
                <tr>
                    <th class="col-xs-5">Naam</th>
                    <th class="col-xs-5">Omschrijving</th>
                    <th class="col-xs-2">Prijs</th>
                </tr>
                @foreach($invoice_lines as $invoice_line)
                    <tr>
                        <td>
                            <b>{{ $invoice_line->name }}</b>
                        </td>
                        <td>
                            {{ $invoice_line->description }}
                        </td>
                        <td>
                            &euro; {{ $invoice_line->price }}
                        </td>
                    </tr>
                @endforeach
            </table>

            <div class="clearfix"></div>
            <div class="gap"></div>

            <div class="col-xs-6 col-xs-push-6 btw-totalprice-wrapper">
                <div class="col-xs-12">
                    <div class="col-xs-8 total-left">Totaal excl.btw</div>
                    <div class="col-xs-4 total-right">&euro; {{ $data->price }}</div>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-8 total-left">Totaal BTW</div>
                    <div class="col-xs-4 total-right">&euro; {{ number_format($data->price * 0.21, 2) }}</div>
                </div>
                <div class="col-xs-8 col-xs-push-4 factuur-line-sep"></div>
                <div class="col-xs-12">
                    <div class="col-xs-8 total-left">Te Betalen</div>
                    <div class="col-xs-4 total-right">&euro; {{ number_format(($data->price * 1.21),2) }}</div>
                </div>
            </div>

            <div class="clear"></div>
            <div class="gap"></div>

            <div class="col-xs-12 factuur-payment">
                Ik verzoek u het totaalbedrag binnen 14 dagen over te maken op rekeningnummer:
               ... T.n.v. Bedrijfsnaam
            </div>

        </div>
    </div>

@endsection