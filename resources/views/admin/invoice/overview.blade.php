@extends('admin_app')

@extends('admin.nav')

@section('header')
    <div ng-controller="invoiceController" ng-init="getPage(1)">
        <h1 class="col-xs-12">Factuur Overzicht</h1>
@endsection

@section('content')

        <div class="col-xs-12 default-wrapper no-border-top no-padding">
            <a href="{{ url('admin/invoice/add') }}" type="button" class="cta-button-icon col-xs-push-9 col-xs-3">
                <i class="fa fa-plus"></i>
                Voeg een factuur toe
            </a>

        <div class="clearfix"></div>
        <div class="gap"></div>

        <div class="col-xs-12 default-wrapper highlight-wrapper">
            <div class="highlight"></div>
                Hieronder is een overzicht te zien van alle gemaakte facturen.
            </div>
        </div>

        <script>
            $( document ).ready(function() {
                $("#search-box").keyup(function(event){
                    if(event.keyCode == 13){
                        $("#search-button").click();
                    }
                });
            });
        </script>

        <div class="col-xs-4 col-xs-push-8 no-padding search-wrapper">
            <div class="input-group stylish-input-group">
                <input id="search-box" ng-model="search_text" type="text" class="form-control"  placeholder="Zoek met Factuur ID" >
                    <span class="input-group-addon">
                        <button id="search-button" type="submit" ng-click="searchItems(search_text)">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
            </div>
        </div>

        <table class="table table-striped">
            <tr>
                <th>#</th>
                <th>Student</th>
                <th>Naam</th>
                <th>Prijs</th>
                <th>Status</th>
                <th></th>
            </tr>
            <p class="text-center" ng-show="loading"><span class="fa fa-spinner fa-pulse fa-3x fa-fw"></span>
            <tr ng-hide="loading" ng-repeat="invoice in data.data track by invoice.id">
                <td><% invoice.id %></td>
                <td><% invoice.student.name %></td>
                <td><% invoice.name %></td>
                <td>&euro; <% invoice.price %></td>
                <td><% invoice.status %></td>
                <td>
                    <a href="/admin/invoice/view/<% invoice.id %>" type="button" class="btn btn-icon-only btn-labeled btn-primary">
                        <i class="glyphicon glyphicon-eye-open"></i>
                    </a>
                    <a href="/admin/invoice/edit/<% invoice.id %>" type="button" class="btn btn-icon-only btn-labeled btn-info">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    <a data-toggle="modal" data-target="#myModal<% invoice.id %>" type="button" class="btn btn-icon-only btn-labeled btn-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                    </a>
                </td>
            </tr>
        </table>

        <div class="center">
            <ul class="pagination">
                <li ng-click="previousPage(data.current_page)"><a href="#">«</a></li>
                    <li ng-repeat="n in [] | range: data.last_page">
                        <a ng-if="n == data.current_page" href="#" class="active" ng-click="getPage(n)"><% n %></a>
                        <a ng-if="n != data.current_page" href="#" ng-click="getPage(n)"><% n %></a>
                    </li>
                <li ng-click="nextPage(data.current_page, data.last_page)"><a href="#">»</a></li>
            </ul>
        </div>

        <div class="total">Results: <% data.from %> - <% data.to %></div>
        <div class="total">Total Records: <% data.total %></div>

@endsection

@section('modal')
    <div ng-repeat="invoice in data.data track by invoice.id">
        <div class="modal fade" id="myModal<% invoice.id %>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Factuur verwijderen</h4>
                    </div>
                    <div class="modal-body">
                        Weet u zeker dat u factuur <b><% invoice.name %></b> wilt verwijderen?
                    </div>
                    <div class="modal-footer">
                        <a ng-click="deleteItem(invoice.id, data.current_page)" class="cta-button" data-dismiss="modal">Verwijder</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    </div>
@endsection