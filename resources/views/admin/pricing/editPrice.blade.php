@extends('admin_app')
@extends('admin.nav')

@section('header')

    <div ng-controller="pricingController">
    <div id="mediaController" ng-controller="mediaController">
 <script>
        $(document).ready(function(){
            $('.flash-message').delay(3000).slideUp(300);

            angular.element('#mediaController').scope().getAllMediaItems();
        });
    </script>

        <h1 class="col-xs-12">Edit Product</h1>
@endsection

@section('content')

        <div class="clearfix"></div>
        <div class="gap"></div>

        <a href="{{ url('/admin/pricing/overview') }}" type="button" class="cta-button-icon col-xs-push-9 col-xs-3">
                <i class="glyphicon glyphicon-arrow-left"></i>
                Back
            </a>

        <div class="clearfix"></div>
        <div class="gap"></div>

            <script>
                $(document).ready(function(){
                    $('.flash-message').delay(3000).slideUp(300);
                });
            </script>

            @if(Session::has('validationMessage'))
                <div class="col-xs-12 default-wrapper highlight-wrapper flash-message">
                    <div class="highlight highlight-green"></div>
                    {{ Session::get('validationMessage') }}
                </div>

                <div class="clearfix"></div>
                <div class="gap"></div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger col-xs-12">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="col-xs-12 default-wrapper highlight-wrapper">
                <div class="highlight"></div>
                Wijzig product: <b> {{ $data->name }}</b>
                </div>


            <div class="clearfix"></div>
            <div class="gap"></div>

                <div class="col-xs-12 default-wrapper default-styles">

        {!! Form::open(['url' => '/admin/pricing/edit/'.$data->id]) !!}

            <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="name">Name</label>
                        {!! Form::text('name',
                                        $data->name,
                                        ['placeholder' => 'Enter name',
                                          'id' => 'name',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="Price">Price</label>
                        {!! Form::text('price',
                                       $data->price,
                                        ['placeholder' => 'Enter Price',
                                          'id' => 'price',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>

    <div class="clearfix"></div>
    <div class="gap"></div>




            <div class="form-group">
                <div class="col-xs-8 no-padding">
                    <div class="col-xs-12">
                        <label for="Description">Description</label>
                        {!! Form::textarea('description',
                                        $data->description,
                                        ['placeholder' => 'Enter Description',
                                          'id' => 'description',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="gap"></div>

            <div class="form-group">
                <div class="col-xs-12">
                    <label for="name">Afbeelding</label>
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-5 no-padding-left">
                            <input type="text" value="{{ $data->path }}" name="singleImage" class="form-control col-xs-12" placeholder="Select img url..." readonly />
                        </div>
                        <button type="button"
                                data-toggle="modal"
                                data-target="#myModal"
                                class="cta-button col-xs-2"
                                ng-click="noChoice();"
                                >
                            Select Image
                        </button>
                    </div>

                </div>
            </div>



            <div class="col-xs-12">
                {!! Form::button('Edit Product', ['type' => 'submit', 'class' => 'col-xs-4 cta-button']) !!}
            </div>
            <div class="clearfix"></div>
            <div class="gap"></div>

        {!! Form::close() !!}
    </div>
 
@endsection


@section('modal')
    <div class="modal custom-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog col-xs-10 col-xs-push-1" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Mediatheek</h4>
                </div>
                <div class="modal-body">
                    <div id="media-gallery-modal" class="col-xs-12 default-wrapper">
                        <div ng-repeat="image in data">
                            <img ng-click="loadSingleImage(image.id)" id="<% image.id %>" data-toggle="modal" data-target="#myModalHighlight" class="media-gallery-image media-gallery-image-popup" ng-src="<% image.path %>" alt="<% image.alt %>" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    <div class="modal custom-modal fade" id="myModalHighlight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog col-xs-10 col-xs-push-1" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Afbeelding</h4>
                </div>

                <div class="clearfix"></div>
                <div class="gap"></div>

                <div class="modal-body">
                    <img id="the-highlighted-image" class="col-xs-9 no-padding-right media-highlighted-image"
                         src=""
                         alt=""/>
                    <div class="col-xs-3 no-padding media-right-bar">
                        <div class="col-xs-12">
                            <h6>Afbeelding Informatie</h6>

                            <div ng-if="fail || success">
                                <div class="clearfix"></div>
                                <div class="gap"></div>
                            </div>

                            <div ng-if="fail" class="col-xs-12 alert-danger cstm-alert">
                                Got an error in the process.
                            </div>
                            <div ng-if="success" class="col-xs-12 alert-success cstm-alert">
                                Succesvol gewijzigd.
                            </div>

                            <div class="clearfix"></div>
                            <div class="gap"></div>

                            {{ Form::open() }}

                            <label for="id">ID</label>
                            {{ Form::text("id", null, array("ng-model" => "id", "class" => "form-control col-xs-12", "readonly")) }}

                            <div class="clearfix"></div>
                            <div class="gap"></div>

                            <label for="name">Naam</label>
                            {{ Form::text("name", null, array("ng-model" => "name", "class" => "form-control col-xs-12")) }}

                            <div class="clearfix"></div>
                            <div class="gap"></div>

                            <label for="alt">Alt text</label>
                            {{ Form::text("alt", null, array("ng-model" => "alt", "class" => "form-control col-xs-12")) }}

                            <div class="clearfix"></div>
                            <div class="gap"></div>

                            <label for="path">Url</label>
                            {{ Form::text("path", null, array("ng-model" => "path", "readonly", "class" => "form-control col-xs-12")) }}

                            <div class="clearfix"></div>
                            <div class="gap"></div>

                            <div class="col-xs-12 no-padding">
                                <button ng-click="changeImageValues(id,name,alt,path)" class="col-xs-12 cta-button" type="button">
                                    Afbeelding Wijzigen
                                </button>
                            </div>

                            <div class="col-xs-12 no-padding btn-wrap-media">
                                <div class="col-xs-6 no-padding">
                                    <button ng-click="getImageURL(path)" class="col-xs-10 col-xs-push-1 btn btn-icon-only btn-labeled cta-button" type="button">
                                        <i class='glyphicon glyphicon-paperclip'></i>
                                    </button>
                                </div>
                            </div>

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="gap"></div>

                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

@endsection