@extends('admin_app')
@extends('admin.nav')

@section('header')
        <h1 class="col-xs-12">Pricing Single View</h1>
@endsection

@section('content')

    <div class="clearfix"></div>
     <div class="gap"></div>

     <a href="{{ url('/admin/pricing/overview') }}" type="button" class="cta-button-icon col-xs-push-9 col-xs-3">
                <i class="glyphicon glyphicon-arrow-left"></i>
                Back
            </a>

    <div class="clearfix"></div>
    <div class="gap"></div>

    <div class="col-xs-12 default-wrapper highlight-wrapper">
        <div class="highlight"></div>
           Product ID: {{ $data->id }}
        </div>

        <div class="clearfix"></div>
        <div class="gap"></div>

            <div class="gap"></div>

   
            <div class="col-xs-12 ">
                <div class="gap"></div>
                <h2>Product: {{ $data->name }}</h2>
                <div class="gap"></div>
                <div class="col-xs-6 no-padding">
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding"><p>Name:  {{ $data->name }}</p></div>
                    </div>
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding"><p>Price:{{ $data->price }}</p></div>
                    </div>
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding"><p>Description:{{ $data->description }}</p></div>
                    </div>

                  
                </div>
            </div>
 


        <div class="clearfix"></div>
        <div class="gap"></div>
</div>
@endsection