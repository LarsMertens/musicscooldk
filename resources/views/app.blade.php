<!DOCTYPE html>
<html ng-app="app">
<head>

	<!-- Google -->
	<meta name="robots" content="noindex">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans|Oswald" rel="stylesheet">

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('css/font-awesome.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('css/jquery.fancybox.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('css/jquery.fancybox-buttons.css?v=1.0.5') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('css/style.css') }}">
   <link rel="stylesheet" href="{{url('css/dropzone.min.css')}}">
	<!-- JQuery -->
	<script src="{{ url('js/jquery.min.js') }}"></script>
	<script src="{{ url('js/bootstrap.min.js') }}"></script>
	<script src="{{ url('js/masonry.min.js') }}"></script>
	<script src="{{ url('js/jquery.fancybox.js') }}"></script>
	<script src="{{ url('js/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
	<script src="{{ url('js/css-corrections.js') }}"></script>
	<script src="{{ url('js/moment.js') }}"></script>
	<script src="{{ url('js/countdown.min.js') }}"></script>
	<script src="{{ url('js/moment-countdown.min.js') }}"></script>
	<script type="text/javascript" src="{{url('js/dropzone.min.js')}}"></script>
	<!-- Angular  -->
	<script src="{{ url('angular/angular.min.js') }}"></script>
	<script src="{{ url('angular/ui-bootstrap-tpls-1.2.5.min.js') }}"></script>
	<script src="{{ url('angular/app.js') }}"></script>
	<script src="{{ url('angular/controller/timescheduleCtrl.js') }}"></script>
	<script src="{{ url('angular/controller/ui.bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{url('angular/service/teacherMediaService.js')}}"></script>
    <script type="text/javascript" src="{{url('angular/controller/teacherMediaCtrl.js')}}"></script>
    <script type="text/javascript" src="{{url('angular/controller/nonAdminTeacherCtrl.js')}}"></script>
	<script type="text/javascript" src="{{url('angular/service/nonAdminTeacherService.js')}}"></script>
 	<script type="text/javascript" src="{{url('angular/controller/nonAdminStudentCtrl.js')}}"></script>
    <script type="text/javascript" src="{{url('angular/service/nonAdminStudentService.js')}}"></script>



</head>
<body class="background-student-image">
	<div class="user-content-wrapper">
		@yield('header')

		@yield('content')

		@yield('footer')

		@yield('modal')
	</div>
</body>   

</html>