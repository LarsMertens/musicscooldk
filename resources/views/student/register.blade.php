@extends('app')

@section('title') Login Company @endsection
@section('description') Dit is de login voor de Company. @endsection
@section('login_page')login-page @endsection

@section('content')
    <div class="background-login-wrapper">
        <div class="col-xs-5 col-xs-push-7 register-wrapper">

            @if ($errors->has())
                <div class="error-box col-xs-12 alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
                <div class="clear"></div>
            @endif

            <h3>Registreren</h3>

            {{ Form::open(['url' => '/student/registerStudent']) }}

                {!! Form::radio('gender', 'Dhr.', true) !!}
                Dhr.
                {!! Form::radio('gender', 'Mevr.') !!}
                Mevr.

                <div class="clear"></div>
                <div class="gap-10"></div>

                <div class="col-xs-5 no-padding-left">
                    {!! Form::text('firstname', '', array('class' => 'form-control',
                                                         'placeholder' => 'Firstname'))
                    !!}
                </div>
                <div class="col-xs-2 no-padding-left">
                    {!! Form::text('insertion', '', array('class' => 'form-control',
                                                              'placeholder' => 'Insertion'))

                    !!}
                </div>
                <div class="col-xs-5 no-padding">
                    {!! Form::text('lastname', '', array('class' => 'form-control',
                                                         'placeholder' => 'Lastname'))
                    !!}
                </div>

                <div class="clear"></div>
                <div class="gap-10"></div>

                <div class="col-xs-9 no-padding-left">
                    {!! Form::text('address', '', array('class' => 'form-control',
                                                        'placeholder' => 'Street Address',
                                                        'id' => 'straatnaaminput'))
                    !!}
                </div>
                <div class="col-xs-3 no-padding">
                    {!! Form::text('house_number', '', array('class' => 'form-control',
                                                             'placeholder' => 'House Number',
                                                             'id' => 'huisnummerinput'))
                    !!}
                </div>

                <div class="clear"></div>
                <div class="gap-10"></div>

                <div class="col-xs-3 no-padding">
                    {!! Form::text('zipcode', '', array('class' => 'col-xs-3 form-control',
                                                         'placeholder' => 'Zipcode',
                                                         'id' => 'postcodeinput'))
                    !!}
                </div>

                <div class="col-xs-9 no-padding-right">
                    {!! Form::text('town','', array('class' => 'form-control',
                                                          'placeholder' => 'Town',
                                                          'id' => 'woonplaatsinput'))
                    !!}
                </div>

                <div class="clear"></div>
                <div class="gap-10"></div>

                {!! Form::text('phonenumber', '', array('class' => 'form-control',
                                                                   'placeholder' => 'Phonenumber',))

                !!}

                <div class="clear"></div>
                <div class="gap-10"></div>

                {!! Form::text('email', '', array('class' => 'col-xs-9 form-control', 'placeholder' => 'E-mail')) !!}

                <div class="clear"></div>
                <div class="gap-10"></div>

                <div class="col-xs-6 no-padding">
                    {!! Form::password('password', array('class' => 'form-control',
                                                         'placeholder' => 'Password'))

                    !!}
                </div>

                <div class="col-xs-6 no-padding-right">
                    {!! Form::password('password_confirmation', array('class' => 'form-control',
                                                                      'placeholder' => 'Repeat Password'))

                    !!}
                </div>

                <div class="clear"></div>
                <div class="gap-20"></div>

                {!! Form::button('Register Account', array('id' => 'bestelling-afronden',
                                                           'class' => 'col-xs-12 cta-button cta-btn-keuze cta-btn-keuze-custom-pad',
                                                           'type' => 'submit'))
                !!}

            {{ Form::close() }}

        </div>
    </div>
@endsection


