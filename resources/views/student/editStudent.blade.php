@extends('app')

@include('partials/navigation')

@section('content')

    <div id="dashboard" class="dashboard-wrapper col-xs-12 no-padding">

        <div class="dashboard-content">
            <div class="container">

    <div class="col-xs-8 right-content no-padding">

        <div class="clearfix"></div>
        <div class="gap"></div>

            <script>
                $(document).ready(function(){
                    $('.flash-message').delay(3000).slideUp(300);
                });
            </script>

            @if(Session::has('validationMessage'))
                <div class="col-xs-12 default-wrapper highlight-wrapper flash-message">
                    <div class="highlight highlight-green"></div>
                   <p> {{ Session::get('validationMessage') }}</p>
                </div>

                <div class="clearfix"></div>
                <div class="gap"></div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger col-xs-12">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="col-xs-12 default-wrapper highlight-wrapper">
                <div class="highlight"></div>
               <p> Wijzig hieronder het profiel van: <b> {{ $data->name }}</b><p>
                </div>


            <div class="clearfix"></div>
            <div class="gap"></div>

            <div class="col-xs-12 default-wrapper default-styles">

        {!! Form::open(['url' => '/student/edit/' . $data->id]) !!}

            <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="Firstname">Firstname</label>
                        {!! Form::text('Firstname',
                                        $naw->firstname,
                                        ['placeholder' => 'Enter firstname',
                                          'id' => 'Firstname',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="Insertion">Insertion</label>
                        {!! Form::text('Insertion',
                                        $naw->insertion,
                                        ['placeholder' => 'Enter Insertion',
                                          'id' => 'Insertion',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>

                <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="Lastname">Lastname</label>
                        {!! Form::text('Lastname',
                                        $naw->lastname,
                                        ['placeholder' => 'Enter Lastname',
                                          'id' => 'Lastname',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>
    <div class="clearfix"></div>
    <div class="gap"></div>


            <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="Gender">Gender</label>
                        {{ Form::select('Gender', [
                           'Mr.' => 'Mr.',
                           'Mrs.' => 'Mrs.']
                        ) }}
                    </div>
                </div>
            </div>


           

    <div class="clearfix"></div>
    <div class="gap"></div>


     <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="Address">Address</label>
                        {!! Form::text('Address',
                                        $naw->address,
                                        ['placeholder' => 'Enter Address',
                                          'id' => 'Address',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="House_number">Housenumber</label>
                        {!! Form::text('House_number',
                                        $naw->house_number,
                                        ['placeholder' => 'Enter house number',
                                          'id' => 'House_number',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="Town">Town</label>
                        {!! Form::text('Town',
                                        $naw->town,
                                        ['placeholder' => 'Enter Town',
                                          'id' => 'Town',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>

    <div class="clearfix"></div>
    <div class="gap"></div>

                <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="Zipcode">Zipcode</label>
                        {!! Form::text('Zipcode',
                                      $naw->zipcode,
                                        ['placeholder' => 'Enter Zipcode',
                                          'id' => 'Zipcode',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>

    <div class="clearfix"></div>
    <div class="gap"></div>

            <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="email">Email</label>
                        {!! Form::text('email',
                                        $data->email,
                                        ['placeholder' => 'Voer email in...',
                                          'id' => 'email',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="Phonenumber">Phonenumber</label>
                        {!! Form::text('Phonenumber',
                                        $naw->phonenumber,
                                        ['placeholder' => 'Enter Phonenumber',
                                          'id' => 'Phonenumber',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>

    <div class="clearfix"></div>
    <div class="gap"></div>

            <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="password">Wachtwoord</label>
                        {!! Form::password('password',
                                      
                                        ['placeholder' => 'Voer wachtwoord in...',
                                          'id' => 'password',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-4 no-padding">
                    <div class="col-xs-12">
                        <label for="password_confirm">Bevestig wachtwoord</label>
                        {!! Form::password('password_confirm',
                                      
                                        ['placeholder' => 'Voer wachtwoord in...',
                                          'id' => 'password_confirm',
                                          'class' => 'form-control']
                                         )
                        !!}
                    </div>
                </div>
            </div>

                
            <div class="clearfix"></div>
            <div class="gap"></div>


            <div class="col-xs-12">
                {!! Form::button('Edit Student', ['type' => 'submit', 'class' => 'col-xs-4 cta-button']) !!}
            </div>
            <div class="clearfix"></div>
            <div class="gap"></div>

        {!! Form::close() !!}
            </div>
    </div>
</div>
@endsection


