@extends('app')

@include('partials.navigation')

@section('content')

    <div id="dashboard" class="dashboard-wrapper col-xs-12 no-padding">

        <div class="dashboard-content">
            <div class="container">

                <div class="col-xs-8 right-content no-padding">

                    <div class="col-xs-12 no-padding next-lessons-box">
                        <div class="col-xs-12 col-xs-push-1 no-padding">
                            <div class="col-xs-12 time-until-next-lesson-box mijn-materiaal">
                                Lektioner Materiaal Overzicht
                            </div>
                        </div>
                        <div class="table-content col-xs-12 col-xs-push-1 shadow-for-time no-padding">

                            <div class="col-xs-12 time-until-next-lesson-time no-padding">
                                <div class="head-item col-xs-4">Toegevoegd op</div>
                                <div class="head-item col-xs-5">Name</div>
                                <div class="head-item col-xs-3">Controls</div>
                            </div>

                            @foreach($data->materials as $material)
                                <div class="table-row col-xs-12 time-until-next-lesson-time no-padding">
                                    <div class="sub-item col-xs-4">{{ $material->created_at }}</div>
                                    <div class="sub-item col-xs-5">{{ $material->name }}</div>
                                    <div class="sub-item col-xs-3">
                                        <a href="{{ url('/student/material/'.$material->id) }}" class="verzet-les-btn col-xs-10 col-xs-push-1">
                                            Bekijk materiaal
                                        </a>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

                <div class="clear"></div>

                <div class="col-xs-12 no-padding statistics-box next-lessons-box">

                </div>

            </div><!-- end dashboard-wrapper -->

@endsection