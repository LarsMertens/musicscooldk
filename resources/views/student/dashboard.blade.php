@extends('app')

@include('partials/navigation')

@section('content')

    <div id="dashboard" class="dashboard-wrapper col-xs-12">

        <div class="dashboard-content">


                <div class="col-xs-8 right-content" ng-controller="timescheduleController" ng-init="getTimeUntilNextLesson()">

                    <div class="col-xs-12 no-padding next-lessons-box">
                        <div class="col-xs-12 no-padding">
                            <div class="col-xs-12 time-until-next-lesson-box">
                                Volgende Lektioner over
                            </div>
                        </div>

                        <div ng-if="!noLessonPlanned" class="col-xs-12 shadow-for-time no-padding">
                            <div class="col-xs-3 time-until-next-lesson-time">
                                <div class="responsive-circle time col-xs-10 col-xs-push-1">
                                    <% daysCountdown %>
                                </div>
                                <div class="time-text col-xs-12">dagen</div>
                            </div>
                            <div class="col-xs-3 time-until-next-lesson-time">
                                <div class="responsive-circle time col-xs-10 col-xs-push-1">
                                    <% hoursCountdown %>
                                </div>
                                <div class="time-text col-xs-12">uur</div>
                            </div>
                            <div class="col-xs-3 time-until-next-lesson-time">
                                <div class="responsive-circle time col-xs-10 col-xs-push-1">
                                    <% minutesCountdown %>
                                </div>
                                <div class="time-text col-xs-12">minuten</div>
                            </div>
                            <div class="col-xs-3 time-until-next-lesson-time">
                                <div class="responsive-circle time col-xs-10 col-xs-push-1">
                                    <% secondsCountdown %>
                                </div>
                                <div class="time-text col-xs-12">seconden</div>
                            </div>
                        </div>

                        <div ng-if="noLessonPlanned" class="col-xs-12 col-xs-push-1 shadow-for-time no-padding">
                            <div class="col-xs-12 time-until-next-lesson-time">
                                <div class="time-text col-xs-12">Nog geen les ingeplanned</div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="clear"></div>

                <div class="col-xs-12 no-padding statistics-box next-lessons-box">

                    <div class="statistics-row col-xs-12 no-padding">
                        <div class="col-xs-4">
                            <div class="statistics-wrapper col-xs-12 no-padding">
                                <div class="col-xs-12 statistics lessons-followed">
                                    Lektioner followed
                                </div>
                                <div class="col-xs-12 with-circle time-until-next-lesson-time">
                                    <div class="responsive-circle-5 time col-xs-5 col-xs-push-3">
                                       @if($data)
                                         {{ $data->lessons_followed }}
                                       @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="col-xs-12 no-padding statistics-wrapper">
                                <div class="col-xs-12 statistics favorite-genres">
                                    Favorite Genre(s)
                                </div>
                                <div class="col-xs-12 favorite-genres-text time-until-next-lesson-time">
                                    @if($data)
                                        {{ $data->favorite_genres }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                                <div class="col-xs-12 no-padding statistics-wrapper">
                                    <div class="col-xs-12 statistics favorite-genres">
                                        Docent(en)
                                    </div>
                                    <div id="all-teachers" class="col-xs-12 favorite-genres-text time-until-next-lesson-time">
                                        @if($data->teachers )
                                            @foreach($data->teachers as $teacher)
                                                {{ $teacher->name }} <br/>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="statistics-row col-xs-12 no-padding">
                        <div class="col-xs-4">
                            <div class="col-xs-12 no-padding statistics-wrapper">
                                <div class="col-xs-12 statistics favorite-genres">
                                    Lid Sinds
                                </div>
                                <div class="col-xs-12 favorite-genres-text time-until-next-lesson-time">
                                    @if($data)
                                    {{ Carbon\Carbon::parse($data->member_since)->format('d.m.Y') }}
                                
                                     @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="col-xs-12 no-padding statistics-wrapper">
                                <div class="col-xs-12 statistics favorite-genres">
                                    Subscription
                                </div>
                                <div class="col-xs-12 favorite-genres-text time-until-next-lesson-time">
                                    @if($data)
                                        {{ $data->subscription }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="statistics-wrapper col-xs-12 no-padding">
                                <div class="col-xs-12 statistics lessons-followed">
                                    Lektioner Items
                                </div>
                                <div class="col-xs-12 with-circle time-until-next-lesson-time">
                                    <div class="responsive-circle-5 time col-xs-5 col-xs-push-3">
                                         @if($data)
                                            {{ $data->lesson_items }}
                                         @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

    </div><!-- end dashboard-wrapper -->

@endsection