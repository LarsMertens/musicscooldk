@extends('app')

@include('partials.navigation')

@section('content')

    <div id="dashboard" class="dashboard-wrapper col-xs-12 no-padding" ng-controller="timescheduleController">

        <div ng-if="booked" class="fixed-highlight col-xs-6 col-xs-push-3 default-wrapper highlight-wrapper flash-message">
            <div class="highlight highlight-green"></div>
            De les is succesvol ingeplanned.
        </div>

        <div class="dashboard-content">
            <div class="container">

                <div class="col-xs-8 right-content">

                    <div class="agenda-info-wrapper col-xs-12 no-padding">

                        <div class="user-agenda-highlight-wrapper col-xs-3 no-padding-left">
                            <div class="user-agenda-highlight col-xs-12">
                                <div class="round-shape round-shape-white-border transparant-bg yellow col-xs-6 col-xs-push-3">
                                    <span class="glyphicon glyphicon-info-sign"></span>
                                </div><!-- end round-shape -->
                                <div class="clear"></div>
                                <p>Plan hieronder jouw lessen in op datum naar keuze.</p>
                            </div>
                        </div><!-- end user-agenda-highlight-wrapper -->

                        <div class="user-agenda-highlight-wrapper col-xs-3 no-padding-left">
                            <div class="user-agenda-highlight col-xs-12">
                                <div class="round-shape round-shape-white-border transparant-bg yellow col-xs-12 no-padding">
                                    <span ng-init="getNrOfLessons()"><% NrOfLessons %></span>
                                </div><!-- end round-shape -->
                                <div class="user-agenda-highlight-description text-center col-xs-12">
                                    <p>les(sen) geplanned</p>
                                </div>
                            </div><!-- end user-agenda-highlight -->
                        </div><!-- end user-agenda-highlight-wrapper -->

                        <div class="user-agenda-highlight-wrapper col-xs-3 no-padding-left">
                            <div class="user-agenda-highlight col-xs-12">
                                <div class="round-shape round-shape-white-border transparant-bg yellow col-xs-6 col-xs-push-3">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div><!-- end round-shape -->
                                <div class="user-agenda-highlight-description text-center col-xs-12 no-padding">
                                    <p class="col-xs-12"> Volgende les: </p>
                                    <p class="col-xs-12">
                                        Date: <% nextLessonDate %>
                                        <br/>
                                        Time: <% nextLessonTime %>
                                    </p>
                                </div>
                            </div><!-- end user-agenda-highlight -->
                        </div><!-- end user-agenda-highlight-wrapper -->

                        <div class="user-agenda-highlight-wrapper col-xs-3 no-padding-left no-padding-right">
                            <div class="user-agenda-highlight col-xs-12">
                                <h4 class="col-xs-12">Legenda</h4>

                                <div class="legenda-item col-xs-12">
                                    <span class="legenda-text no-padding col-xs-6">Niet beschikbaar</span>
                                    <img src="{{ url('img/unavailable.png') }}" alt="Unavailable" class="col-xs-6">
                                </div>

                                <div class="legenda-item col-xs-12">
                                    <span class="legenda-text no-padding col-xs-6">Mijn les</span>
                                    <img src="{{ url('img/my-lesson.png') }}" alt="My lesson" class="col-xs-6">
                                </div>
                            </div>
                        </div><!-- end user-agenda-highlight-wrapper -->
                    </div>

                </div>

                <div class="clear"></div>

                <div class="col-xs-12 no-padding next-lessons-box agenda-wrapper">
                    <div class="col-xs-12">
                        <div class="col-xs-12 time-until-next-lesson-box mijn-lessen">
                            <span class="agenda-header-heading">
                                Agenda
                            </span>

                            <div ng-controller="PopoverDemoCtrl">
                                <select ng-init="selectedLesson = selectedLessons[0]"
                                        ng-model="selectedLesson"
                                        ng-options="selectedLesson as selectedLesson.Name for selectedLesson in selectedLessons track by selectedLesson.Id"
                                        ng-change="GetValueSelectedLessonTypeDropdown()"
                                        style="color: #000; font-size: 2rem;">
                                </select>

                                <select ng-if="selectedALesson"
                                        ng-init="selectedTeacher = selectedTeachers[0]"
                                        ng-model="selectedTeacher"
                                        ng-options="selectedTeacher as selectedTeacher.Name for selectedTeacher in selectedTeachers track by selectedTeacher.Id"
                                        ng-change="GetValueSelectedTeacherDropdown()"
                                        style="color: #000; font-size: 2rem;">
                                        <option value="">Select a teacher</option>
                                </select>
                            </div>

                            {{--<span class="button agenda-btn-select-teacher">--}}
                                {{--Docent: Roan Segers--}}
                                {{--<span class="glyphicon glyphicon-chevron-down"></span>--}}
                            {{--</span>--}}
                            {{--<span class="button agenda-btn-select-teacher agenda-btn-select-teacher-second">--}}
                                {{--Les: Drumles--}}
                                {{--<span class="glyphicon glyphicon-chevron-down"></span>--}}
                            {{--</span>--}}
                        </div>
                    </div>

                    <div class="text-center loading-for-agenda" ng-show="loading">
                        <span class="fa fa-spinner fa-pulse fa-3x fa-fw"></span>
                        <div class="clear"></div>
                        <span class="text">loading agenda...</span>
                    </div>

                    <div class="clear"></div>
                    <div class="gap"></div>
                    <div class="col-xs-12 the-agenda-buttons">
                        <div style="margin: 0 auto; width: 100px;">
                        <div class="cta-button" ng-click="loadPreviousWeek()">
                            <span class="fa fa-chevron-left"></span>
                        </div>
                        <div class="cta-button" ng-click="loadNextWeek()">
                            <span class="fa fa-chevron-right"></span>
                        </div>
                        </div>
                    </div>

                    <div class="col-xs-12 statistics-box next-lessons-box">

                            <div class="col-xs-12 agenda no-padding-right">

                                <div class="col-xs-12 no-padding">
                                    <div class="col-xs-1"></div>
                                    <div class="col-xs-11 no-padding">

                                        <!-- if day is not Søndag -->

                                       <div data-ng-repeat="n in [] | range:7" ng-if="checkSunday(getDay(n))" class="col-xs-2 agenda-day">
                                            <div class="col-xs-12 no-padding"><% getDay(n) %></div>
                                            <div class="col-xs-12 no-padding"><% getDate(n) %></div>
                                       </div>

                                    </div>
                                </div>

                                <div class="col-xs-12 no-padding">
                                    <div class="agenda-row" ng-repeat="time in times">
                                        <div class="col-xs-1 agenda-time"><% time %></div>
                                        <div class="col-xs-11 agenda-inner-row no-padding">
                                            <div ng-if="n!=23">
                                                <div data-ng-repeat="t in [] | range:7">
                                                    <div ng-if="checkSunday(getDay(t))">
                                                        <div ng-controller="PopoverDemoCtrl"
                                                             uib-popover-template="dynamicPopover.templateUrl"
                                                             popover-title="<% dynamicPopover.title %>"
                                                             popover-trigger="outsideClick"
                                                             popover-placement="bottom"
                                                             popover-is-open="dynamicPopover.isOpen"
                                                             ng-click="fillFieldsOnClick($event,time,getDate(t))"
                                                             class="col-xs-2 agenda-inner-row-item no-padding"
                                                             id="field<% n %><% t %>">

                                                            <!-- 30 minuts duration items -->
                                                            <div ng-if="checkScheduledItems(time, getDate(t))">
                                                                <!-- my lesson -->
                                                                <div ng-if="checkIfMyLesson(time, getDate(t))">
                                                                    <div class="agenda-item my-lesson-in-agenda col-xs-12 no-padding" ng-if="checkDuration(time, getDate(t), false) == '30'">
                                                                        <div class="endtime">
                                                                            <% time %> - <% loadTime(time, 30) %>
                                                                        </div>
                                                                        <div class="message">
                                                                            <% loadMessage(time, getDate(t)) %>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- unavailable -->
                                                                <div ng-if="!checkIfMyLesson(time, getDate(t))">
                                                                    <div class="agenda-item not-available col-xs-12 no-padding" ng-if="checkDuration(time, getDate(t), false) == '30'">
                                                                        <div class="endtime">
                                                                            <% time %> - <% loadTime(time, 30) %>
                                                                        </div>
                                                                        <div class="message">
                                                                            <% loadMessage(time, getDate(t)) %>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- 45 minuts duration items -->
                                                            <div ng-if="checkScheduledItems(time, getDate(t))">
                                                                <!-- my lesson -->
                                                                <div ng-if="checkIfMyLesson(time, getDate(t))">
                                                                    <div class="agenda-item my-lesson-in-agenda fourty-five-minutes col-xs-12 no-padding" ng-if="checkDuration(time, getDate(t), false) == '45'">
                                                                        <div class="endtime">
                                                                            <% time %> - <% loadTime(time, 45) %>
                                                                        </div>
                                                                        <div class="message">
                                                                            <% loadMessage(time, getDate(t)) %>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- unavailable -->
                                                                <div ng-if="!checkIfMyLesson(time, getDate(t))">
                                                                    <div class="agenda-item not-available fourty-five-minutes col-xs-12 no-padding" ng-if="checkDuration(time, getDate(t), false) == '45'">
                                                                        <div class="endtime">
                                                                            <% time %> - <% loadTime(time, 45) %>
                                                                        </div>
                                                                        <div class="message">
                                                                            <% loadMessage(time, getDate(t)) %>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- Starts on half starting time and 30 minuts duration -->
                                                            <div ng-if="checkScheduledItemsOnQuarter(time, getDate(t))">
                                                                <!-- my lesson -->
                                                                <div ng-if="checkIfMyLesson(time, getDate(t))">
                                                                    <div class="agenda-item my-lesson-in-agenda fourty-five-minutes-quartar-30 col-xs-12 no-padding" ng-if="checkDuration(time, getDate(t), true) == '30'">
                                                                        <div class="endtime">
                                                                            <% loadTime(time, -15) %> - <% loadTime(time, 30 - 15) %>
                                                                        </div>
                                                                        <div class="message">
                                                                            <% loadMessage(time, getDate(t)) %>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- unavailable -->
                                                                <div ng-if="!checkIfMyLesson(time, getDate(t))">
                                                                    <div class="agenda-item not-available fourty-five-minutes-quartar-30 col-xs-12 no-padding" ng-if="checkDuration(time, getDate(t), true) == '30'">
                                                                        <div class="endtime">
                                                                            <% loadTime(time, -15) %> - <% loadTime(time, 30 - 15) %>
                                                                        </div>
                                                                        <div class="message">
                                                                            <% loadMessage(time, getDate(t)) %>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- Starts on half starting time and 45 minuts duration -->
                                                            <div ng-if="checkScheduledItemsOnQuarter(time, getDate(t))">
                                                                <!-- my lesson -->
                                                                <div ng-if="checkIfMyLesson(time, getDate(t))">
                                                                    <div class="agenda-item my-lesson-in-agenda fourty-five-minutes-quartar col-xs-12 no-padding" ng-if="checkDuration(time, getDate(t), true) == '45'">
                                                                        <div class="endtime">
                                                                            <% loadTime(time, -15) %> - <% loadTime(time, 45 - 15) %>
                                                                        </div>
                                                                        <div class="message">
                                                                            <% loadMessage(time, getDate(t)) %>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- unavailable -->
                                                                <div ng-if="!checkIfMyLesson(time, getDate(t))">
                                                                    <div class="agenda-item not-available fourty-five-minutes-quartar col-xs-12 no-padding" ng-if="checkDuration(time, getDate(t), true) == '45'">
                                                                        <div class="endtime">
                                                                            <% loadTime(time, -15) %> - <% loadTime(time, 45 - 15) %>
                                                                        </div>
                                                                        <div class="message">
                                                                            <% loadMessage(time, getDate(t)) %>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- Vacation / Or Scheduled all day -->
                                                            <div ng-if="checkScheduledItems(time, getDate(t))">

                                                                <div class="agenda-item not-available vacation col-xs-12 no-padding" ng-if="checkDuration(time, getDate(t), false) == '690'">
                                                                    <div class="message">
                                                                        <% loadMessage(time, getDate(t)) %>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- end agenda -->

                        </div><!-- end user-content-main -->
                    </div><!-- end ANGULAR mainCtrl -->

            </div><!-- end dashboard-wrapper -->



@endsection