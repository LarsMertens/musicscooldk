@extends('app')

@section('content')

    <div id="dashboard" class="dashboard-wrapper col-xs-12 no-padding">

        <div class="dashboard-content">
            <div class="container">

                @include('/partials/navigation')

                <div class="col-xs-8 right-content no-padding">

                    <div class="col-xs-12 no-padding next-lessons-box">
                        <div class="col-xs-12 col-xs-push-1 no-padding">
                            <div class="col-xs-12 time-until-next-lesson-box mijn-betalingen">
                                Betaal Overzicht
                            </div>
                        </div>
                        <div class="table-content col-xs-12 col-xs-push-1 shadow-for-time no-padding">

                            <div class="col-xs-12 time-until-next-lesson-time no-padding">
                                <div class="head-item col-xs-3">Periode</div>
                                <div class="head-item col-xs-4">Uiterste Betaaldatum</div>
                                <div class="head-item col-xs-3">Gekocht</div>
                                <div class="head-item col-xs-2">Controls</div>
                            </div>

                            <div class="table-row col-xs-12 time-until-next-lesson-time no-padding">
                                <div class="sub-item col-xs-3">Kwartaal 1 - 2016</div>
                                <div class="sub-item col-xs-4">31-03-2016</div>
                                <div class="sub-item col-xs-3">Local Hero</div>
                                <div class="sub-item col-xs-2">
                                    <button class="verzet-les-btn col-xs-10 col-xs-push-1">Betalen</button>
                                </div>
                            </div>

                            <div class="table-row col-xs-12 time-until-next-lesson-time no-padding">
                                <div class="sub-item col-xs-3">Kwartaal 4 - 2015</div>
                                <div class="sub-item col-xs-4">31-12-2016</div>
                                <div class="sub-item col-xs-3">Local Hero</div>
                                <div class="sub-item col-xs-2">
                                    <button class="verzet-les-btn col-xs-10 col-xs-push-1">Betalen</button>
                                </div>
                            </div>

                            <div class="table-row col-xs-12 time-until-next-lesson-time no-padding">
                                <div class="sub-item col-xs-3">Kwartaal 3 - 2015</div>
                                <div class="sub-item col-xs-4">31-09-2016</div>
                                <div class="sub-item col-xs-3">Local Hero</div>
                                <div class="sub-item col-xs-2">
                                    <button class="verzet-les-btn col-xs-10 col-xs-push-1">Betalen</button>
                                </div>
                            </div>

                            <div class="table-row col-xs-12 time-until-next-lesson-time no-padding">
                                <div class="sub-item col-xs-3">Kwartaal 2 - 2015</div>
                                <div class="sub-item col-xs-4">31-06-2016</div>
                                <div class="sub-item col-xs-3">Local Hero</div>
                                <div class="sub-item col-xs-2">
                                    <button class="verzet-les-btn col-xs-10 col-xs-push-1">Betalen</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="clear"></div>

                <div class="col-xs-12 no-padding statistics-box next-lessons-box">

                </div>

            </div><!-- end dashboard-wrapper -->

@endsection