@extends('app')

@section('content')

    <div id="dashboard" class="dashboard-wrapper col-xs-12 no-padding">

        <div class="dashboard-content">
            <div class="container">

               @include('/partials/navigation')

                <div class="col-xs-8 right-content no-padding">

                    <div class="col-xs-12 no-padding next-lessons-box">
                        <div class="col-xs-12 col-xs-push-1 no-padding">
                            <div class="col-xs-12 time-until-next-lesson-box mijn-lessen">
                                Lektioner overzicht Juni
                            </div>
                        </div>
                        <div class="table-content col-xs-12 col-xs-push-1 shadow-for-time no-padding">

                            <div class="col-xs-12 time-until-next-lesson-time no-padding">
                                <div class="head-item col-xs-2">Datum</div>
                                <div class="head-item col-xs-2">Tijd</div>
                                <div class="head-item col-xs-2">Les</div>
                                <div class="head-item col-xs-2">Personen</div>
                                <div class="head-item col-xs-2">Docent</div>
                                <div class="head-item col-xs-2">Controls</div>
                            </div>

                            <div class="table-row col-xs-12 time-until-next-lesson-time no-padding">
                                <div class="sub-item col-xs-2">24-06-2016</div>
                                <div class="sub-item col-xs-2">10.30 - 11.00 </div>
                                <div class="sub-item col-xs-2">Gitaarles</div>
                                <div class="sub-item col-xs-2">1 Persoon</div>
                                <div class="sub-item col-xs-2">Roan Segers </div>
                                <div class="sub-item col-xs-2">
                                    <button class="verzet-les-btn col-xs-10 col-xs-push-1">Verzet les</button>
                                </div>
                            </div>

                            <div class="table-row col-xs-12 time-until-next-lesson-time no-padding">
                                <div class="sub-item col-xs-2">22-06-2016</div>
                                <div class="sub-item col-xs-2">10.30 - 11.00 </div>
                                <div class="sub-item col-xs-2">Gitaarles</div>
                                <div class="sub-item col-xs-2">1 Persoon</div>
                                <div class="sub-item col-xs-2">Roan Segers </div>
                                <div class="sub-item col-xs-2">
                                    <button class="verzet-les-btn col-xs-10 col-xs-push-1">Verzet les</button>
                                </div>
                            </div>

                            <div class="table-row col-xs-12 time-until-next-lesson-time no-padding">
                                <div class="sub-item col-xs-2">20-06-2016</div>
                                <div class="sub-item col-xs-2">10.30 - 11.00 </div>
                                <div class="sub-item col-xs-2">Gitaarles</div>
                                <div class="sub-item col-xs-2">1 Persoon</div>
                                <div class="sub-item col-xs-2">Roan Segers </div>
                                <div class="sub-item col-xs-2">
                                    <button class="verzet-les-btn col-xs-10 col-xs-push-1">Verzet les</button>
                                </div>
                            </div>

                            <div class="table-row col-xs-12 time-until-next-lesson-time no-padding">
                                <div class="sub-item col-xs-2">18-06-2016</div>
                                <div class="sub-item col-xs-2">13.30 - 14.00 </div>
                                <div class="sub-item col-xs-2">Gitaarles</div>
                                <div class="sub-item col-xs-2">1 Persoon</div>
                                <div class="sub-item col-xs-2">Roan Segers </div>
                                <div class="sub-item col-xs-2">
                                    <button class="verzet-les-btn col-xs-10 col-xs-push-1">Verzet les</button>
                                </div>
                            </div>

                            <div class="table-row col-xs-12 time-until-next-lesson-time no-padding">
                                <div class="sub-item col-xs-2">16-06-2016</div>
                                <div class="sub-item col-xs-2">15.30 - 17.00 </div>
                                <div class="sub-item col-xs-2">Gitaarles</div>
                                <div class="sub-item col-xs-2">1 Persoon</div>
                                <div class="sub-item col-xs-2">Roan Segers </div>
                                <div class="sub-item col-xs-2">
                                    <button class="verzet-les-btn col-xs-10 col-xs-push-1">Verzet les</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="clear"></div>

                <div class="col-xs-12 no-padding statistics-box next-lessons-box">

                </div>

            </div><!-- end dashboard-wrapper -->

@endsection