@extends('app')

@section('content')

    <div id="dashboard" class="dashboard-wrapper col-xs-12 no-padding">

        <div class="dashboard-content">
            <div class="container">

                @include('/partials/navigation')

                <div class="col-xs-8 right-content no-padding">

                    <div class="col-xs-12 no-padding next-lessons-box">

                        <div class="col-xs-12 col-xs-push-1 no-padding">
                            <div class="col-xs-12 time-until-next-lesson-box shop">
                                Music'scool Shop
                            </div>
                        </div>

                        <div class="shop-wrapper col-xs-12 no-padding">
                            <div class="col-xs-6 col-xs-push-1 no-padding-left">
                                <div class="col-xs-12 time-until-next-lesson-box lessen">
                                    Lessen
                                </div>
                                <button class="buy-btn col-xs-12">Koop Lektioner</button>
                            </div>

                            <div class="col-xs-6 col-xs-push-1 no-padding-right">
                                <div class="col-xs-12 time-until-next-lesson-box abonnementen">
                                    Abonnementen
                                </div>
                                <button class="buy-btn col-xs-12">Koop abonnement</button>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="clear"></div>


            </div><!-- end dashboard-wrapper -->

@endsection