<!DOCTYPE html>
<html>
<head>

    <!-- Mobile Ready -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Charset = UTF8 -->
    <meta charset="UTF-8">

    <!-- Google Content -->
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Oswald" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/jquery.fancybox.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/jquery.fancybox-buttons.css?v=1.0.5') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('css/admin_style.css') }}">
    <link rel="stylesheet" href="{{url('css/dropzone.min.css')}}">

    <!-- JQuery -->
    <script src="{{ url('js/jquery.min.js') }}"></script>
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src="{{ url('js/masonry.min.js') }}"></script>
    <script src="{{ url('js/jquery.fancybox.js') }}"></script>
    <script src="{{ url('js/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ url('js/css-corrections.js') }}"></script>
    <script src="{{ url('js/moment.js') }}"></script>
    <script src="{{ url('js/countdown.min.js') }}"></script>
    <script src="{{ url('js/moment-countdown.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="{{url('js/dropzone.min.js')}}"></script>

    <!-- Angular  -->
    <script src="{{ url('angular/angular.min.js') }}"></script>
    <script src="{{ url('angular/ui-bootstrap-tpls-1.2.5.min.js') }}"></script>
    <script src="{{ url('angular/app.js') }}"></script>
    <script src="{{ url('angular/controller/invoiceCtrl.js') }}"></script>
    <script src="{{ url('angular/service/invoiceService.js') }}"></script>
    <script src="{{ url('angular/controller/ui.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{url('angular/controller/teacherCtrl.js')}}"></script>
    <script type="text/javascript" src="{{url('angular/controller/myController.js')}}"></script>
    <script type="text/javascript" src="{{url('angular/service/teacherService.js')}}"></script>
    <script type="text/javascript" src="{{url('angular/controller/studentCtrl.js')}}"></script>
    <script type="text/javascript" src="{{url('angular/service/studentService.js')}}"></script>
    <script type="text/javascript" src="{{url('angular/service/mediaService.js')}}"></script>
    <script type="text/javascript" src="{{url('angular/controller/mediaCtrl.js')}}"></script>
    <script type="text/javascript" src="{{url('angular/service/pricingService.js')}}"></script>
    <script type="text/javascript" src="{{url('angular/controller/pricingCtrl.js')}}"></script>
    <script type="text/javascript" src="{{ url('angular/controller/timescheduleCtrl.js') }}"></script>
    <script type="text/javascript" src="{{url('angular/service/teacherMediaService.js')}}"></script>
    <script type="text/javascript" src="{{url('angular/controller/teacherMediaCtrl.js')}}"></script>

</head>
<body class="background-image-teacher" ng-app="AdminApp">

    @yield('nav')

<div class="teacher-content-wrapper">
    @yield('header')

    @yield('content')

    @yield('modal')

    @yield('footer')
</div>

</body>

</html>